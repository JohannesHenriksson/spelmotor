#include "stdafx.h"
#include "CppUnitTest.h"

#include <iostream>

#include "../Spelmotor/PoolAllocator.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(PoolAllocatorNotNull)
		{
			// TODO: Your test code here
			PoolAllocator poolAllocator(5, sizeof(int));
			Assert::IsNotNull(&poolAllocator, L"Pool Allocator should not be null", LINE_INFO());
		}
		TEST_METHOD(GetPointerWhenAllocating)
		{
			// TODO: Your test code here
			PoolAllocator poolAllocator(5, sizeof(int));
			auto memPointer = poolAllocator.Allocate<int>();
			Assert::IsNotNull(memPointer, L"Allocating memory should not return a nullptr", LINE_INFO());
		}
		TEST_METHOD(CheckValueAfterAllocation)
		{
			// TODO: Your test code here
			PoolAllocator poolAllocator(5, sizeof(int));
			//Check pool not full
			auto memPointer = poolAllocator.Allocate<int>();
			*memPointer = 5;
			Logger::WriteMessage("Mempointer points to: " + *memPointer);
			//Assert::IsNotNull(memPointer, L"Allocating memory should not return nullptr when pool is not full", LINE_INFO());
			Assert::AreEqual(5, *memPointer, L"Pointer should point to memory with the value we created", LINE_INFO());
		}

	};
}