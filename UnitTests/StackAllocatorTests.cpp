#include "stdafx.h"
#include "CppUnitTest.h"
#include <iostream>
#include "../Spelmotor/stackAllocator.h"
#include "../Spelmotor/stackAllocator.cpp"
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(AllocateZeroMemory)
		{
			StackAllocator stack(0);
			int* test = stack.Alloc<int>();
			*test = 1;
			Assert::AreEqual(1, *test);
		}
		TEST_METHOD(AmountOfMemoryInUse)
		{
			StackAllocator stack(100);
			for (int i = 0; i < 5; i++)
			{
				stack.Alloc<char>();
			}
			Assert::AreEqual(stack.MemoryInUse(), 5);
		}
		TEST_METHOD(AmountOfMemoryFree)
		{
			StackAllocator stack(100);
			for (int i = 0; i < 74; i++)
			{
				stack.Alloc<char>();
			}
			Assert::AreEqual(stack.GetFreeMemory(), 26);
		}
		TEST_METHOD(CorrectValues)
		{
			StackAllocator stack(0);
			std::vector<char> chars;
			std::vector<char> correctl;
			int exper = 0;
			int correct = 0;
			for (int i = 0; i < 100; i++)
			{
				correctl.push_back((char)i);
				char* test = stack.Alloc<char>();
				*test = (char)i;
				chars.push_back(*test);

			}
			for (int i = 0; i < 100; i++)
			{
				exper += chars.at(i);
				correct += correctl.at(i);
			}
			Assert::AreEqual(exper, correct);
		}
		TEST_METHOD(ExpandStack)
		{
			StackAllocator stack(0);
			std::vector<char> chars;
			
			for (int i = 0; i < 100; i++)
			{
				char* test = stack.Alloc<char>();
				*test = (char)i;
				chars.push_back(*test);
			}
			stack.Release();

			//150 since we add the overflow + overflow/2 to the stack
			Assert::AreEqual(stack.GetFreeMemory(), 150);
		}
	};
}
