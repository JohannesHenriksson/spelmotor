#ifndef NEW_OVERRIDE
#define NEW_OVERRIDE

#include <Spelmotor\memoryManager.h>

//Global placement new and delete using PoolAllocator
template<class U, class... Args>
void* operator new(size_t size, PoolAllocator& memMgr, Args&&... args)
{
	void* adress = memMgr.Allocate<U>();
//((U*)adress)->U(std::forward<Args>(args)...);
new (adress) U(std::forward<Args>(args)...);
return adress;
}
template<class U>
void  operator delete(void* pointerToDelete, PoolAllocator& memMgr)
{
	((U*)pointerToDelete)->~U();
	memMgr.Deallocate(pointerToDelete);
}

#ifdef USING_MEMORY_MANAGER
// (datatype, poolAllocator, constructor inparameters...)
#define new(dataType, poolAllocator, ...)	((dataType*)operator new<dataType>(sizeof(int), poolAllocator, __VA_ARGS__))
#define delete(dataType, poolAllocator, pointer)	(operator delete<dataType>(pointer, poolAllocator))
#else
#define new(a, b, ...)		(new a(__VA_ARGS__))
#define delete(a, b, c)	(delete c)
#endif // USING_MEMORY_MANAGER

#endif //NEW_OVERRIDE