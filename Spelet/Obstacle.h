#ifndef Obstacle_h__
#define Obstacle_h__

#include <Spelmotor\PhysicsGameObject.h>

class Sound;
class CollisionBox;

class Obstacle : public PhysicsGameObject
{
public:
	/**
	* Creates a obstacle with the specified model and collisionBox at position.
	* An obstacle mainly consists of two Physics GameObjects, one for the specified model and one for the pedestal the model stands on.
	* 
	* \param model The model that will stand on the pedestal 
	* \param collBox The Collision box for the object standing on the pedestal
	* \param position Position with origin in the bottom of the pedestal
	*
	*/
	Obstacle(Model* model, CollisionBox* collBox, DirectX::XMFLOAT3 position);
	~Obstacle();

	/**
	* Updates the obstacle
	*/
	void Update();
	/**
	* Draws the obstacle
	*/
	void Draw();

	/**
	* Sets sound
	*
	* \param sound the sound to set 
	*/
	void SetSound(Sound* sound);
	/**
	* Sets Position
	*
	* \param newPos New position
	*/
	void SetPosition(XMFLOAT3 newPos);
	/**
	* Sets rotation
	*
	* \param newRotation A quaternion with the new rotation
	*/
	void SetRotation(XMFLOAT4 newRotation);

	/**
	* Function to run when a player collides with this obstacle
	*
	* \param sound the sound to set
	*/
	void PlayerCollision();

private:
	// Pedestal that the object stands on
	PhysicsGameObject* pedestal;

	CollisionBox* collisionBox;
	CollisionBox* pedestalCollisionBox;
	// Sound
	Sound* hitSound;

};

#endif // Obstacle_h__

