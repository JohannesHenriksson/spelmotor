#include "GameCamera.h"



GameCamera::GameCamera(float aspectRatioHbW /*= (1280.f / 720.f)*/, float nearPlane /*= 0.1f*/, float farPlane /*= 300.0f*/, float fieldOfView /*= DirectX::XM_PIDIV4*/)
	: Camera(aspectRatioHbW, nearPlane, farPlane, fieldOfView)
	, m_minSpeed(10.0f)
	, m_speed(20.0f)
	, m_roadPosition({ 0.f, 0.f, 0.f })
	, m_offsetDirection({ 0.f, .3f, -1.f })
	, m_cameraDistance(7.f)
{
	LookAt(m_roadPosition, m_roadPosition + m_offsetDirection * m_cameraDistance);
}

GameCamera::GameCamera(DirectX::XMFLOAT3 roadPosition, DirectX::XMFLOAT3 offsetDirection, float aspectRatioHbW /*= 1280.f / 720.f*/, float nearPlane /*= 0.1f*/, float farPlane /*= 300.0f*/, float fieldOfView /*= DirectX::XM_PIDIV4*/)
	: Camera(aspectRatioHbW, nearPlane, farPlane, fieldOfView)
	, m_minSpeed(10.0f)
	, m_speed(3.0f)
	, m_roadPosition(roadPosition)
	, m_offsetDirection(offsetDirection)
	, m_cameraDistance(35.f)
{
	LookAt(m_roadPosition, m_roadPosition + m_offsetDirection * m_cameraDistance);
}

GameCamera::~GameCamera()
{
}

void GameCamera::Update(float dt)
{
	m_speed = m_minSpeed;
	//for (auto& player : m_trackedPlayers)
	//{
	//	if (player->GetPlayerZVelocity() > m_speed)
	//		m_speed = player->GetPlayerZVelocity();
	//}
	m_roadPosition.z += m_speed * dt;
	LookAt(m_roadPosition, m_roadPosition + m_offsetDirection * m_cameraDistance);
}

void GameCamera::AddPlayer(Player* player)
{
	m_trackedPlayers.push_back(player);
}

void GameCamera::AddPlayers(const std::vector<Player*>& players)
{
	m_trackedPlayers.insert(m_trackedPlayers.end(), players.begin(), players.end());
}

void GameCamera::IncreaseMinSpeed(float speedIncrease)
{
	m_minSpeed += speedIncrease;
}

void GameCamera::SetNewRoadLocation(const DirectX::XMFLOAT3& roadPos)
{
	m_roadPosition = roadPos;
	LookAt(m_roadPosition, m_roadPosition + m_offsetDirection * m_cameraDistance);
}
