#include "Obstacle.h"

#include <Spelmotor\GameEngine.h>
#include <Spelmotor\Model.h>
#include <boost\thread.hpp>
#include <Spelmotor\AssetManager.h>
#include <Spelmotor\DDSTexture.h>
#include <Spelmotor\Sound.h>
#include <Spelmotor\CollisionBox.h>

Obstacle::Obstacle(Model* model, CollisionBox* collBox, DirectX::XMFLOAT3 position)
	:PhysicsGameObject(GameEngine::GetInstance()->GetGraphicsManager(), GameEngine::GetInstance()->GetAssetManager(), SimpleShapes::BOX, collBox->halfExtents, { position.x, position.y + 0.544805527f * 2 + collBox->halfExtents.y, position.z })
{
	this->SetModel(model);
	this->GetPhysicsObject()->m_renderOffset = new DirectX::XMFLOAT3(collBox->middlePoint);

	this->collisionBox = collBox;
	// Create pedestal
	Model* pedestalModel = GameEngine::GetInstance()->GetAssetManager()->LoadAsync<Model>(0x5E72568ECF69F198);
	DDSTexture* pedestalTexture = GameEngine::GetInstance()->GetAssetManager()->LoadAsync<DDSTexture>(0x40CB9AFECF69F198);
	this->pedestalCollisionBox = GameEngine::GetInstance()->GetAssetManager()->Load<CollisionBox>(0x5254CEEECF69F198);


	pedestal = new PhysicsGameObject(GameEngine::GetInstance()->GetGraphicsManager(), GameEngine::GetInstance()->GetAssetManager(), SimpleShapes::BOX, pedestalCollisionBox->halfExtents, { position.x, position.y + 0.544805527f, position.z });
	pedestal->GetPhysicsObject()->m_renderOffset = &pedestalCollisionBox->middlePoint;
	pedestal->SetModel(pedestalModel);
	pedestal->SetTexture(pedestalTexture);
}

Obstacle::~Obstacle()
{ 
	// Pedestal unload
	GameEngine::GetInstance()->GetAssetManager()->Unload<Model>(pedestal->GetModel());
	GameEngine::GetInstance()->GetAssetManager()->Unload<DDSTexture>(pedestal->GetTexture());
	GameEngine::GetInstance()->GetAssetManager()->Unload<CollisionBox>(this->pedestalCollisionBox);


	// Object unload
	GameEngine::GetInstance()->GetAssetManager()->Unload<Model>(m_model);
	GameEngine::GetInstance()->GetAssetManager()->Unload<DDSTexture>(m_texture);
	GameEngine::GetInstance()->GetAssetManager()->Unload<CollisionBox>(this->collisionBox);

	if(hitSound != nullptr)
		GameEngine::GetInstance()->GetAssetManager()->Unload<Audio>(hitSound->GetAudioAsset());


	delete pedestal;
	delete hitSound;
}

void Obstacle::Update()
{
	PhysicsGameObject::Update();
	pedestal->Update();
}

void Obstacle::Draw()
{
	PhysicsGameObject::Draw();
	pedestal->Draw();
}

void Obstacle::SetSound(Sound* sound)
{
	this->hitSound = sound;
}

void Obstacle::SetPosition(XMFLOAT3 newPos)
{
	PhysicsGameObject::SetPosition({ newPos.x, newPos.y + 1.09f, newPos.z });
	pedestal->SetPosition(newPos);
}

void Obstacle::SetRotation(XMFLOAT4 newRotation)
{
	PhysicsGameObject::SetRotation(newRotation);
	pedestal->SetRotation(newRotation);
}

void Obstacle::PlayerCollision()
{
	if(hitSound != nullptr)
		hitSound->Play();
}
