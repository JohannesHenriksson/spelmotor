#pragma once
#include <Spelmotor/Camera.h>
#include <Spelmotor/Player.h>
#include <vector>
class GameCamera :
	public Camera
{
public:
	GameCamera(float aspectRatioHbW = (1280.f / 720.f), float nearPlane = 0.1f, float farPlane = 300.0f, float fieldOfView = DirectX::XM_PIDIV4);
	GameCamera(DirectX::XMFLOAT3 roadPosition, DirectX::XMFLOAT3 offsetDirection, float aspectRatioHbW = 1280.f / 720.f, float nearPlane = 0.1f, float farPlane = 300.0f, float fieldOfView = DirectX::XM_PIDIV4);
	~GameCamera();

	void Update(float dt) override;
	void AddPlayer(Player* player);
	void AddPlayers(const std::vector<Player*>& players);
	void IncreaseMinSpeed(float speedIncrease);
	void SetNewRoadLocation(const DirectX::XMFLOAT3& roadPos);
	DirectX::XMFLOAT3 m_roadPosition;
private:
	float m_minSpeed;
	float m_speed;
	std::vector<Player*> m_trackedPlayers;
	
	DirectX::XMFLOAT3 m_offsetDirection;
	float m_cameraDistance;
};

