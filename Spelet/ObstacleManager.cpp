#include "ObstacleManager.h"

#include <Spelmotor\AssetManager.h>
#include <Spelmotor\GameEngine.h>
#include "newOverride.h"
#include <Spelmotor\DDSTexture.h>
#include <Spelmotor\Crc64.h>
#include <math.h>
#include <Spelmotor\Player.h>
#include <Spelmotor\CollisionBox.h>

#include "GameCamera.h"

ObstacleManager::ObstacleManager(DirectX::XMFLOAT3* spawnOrigin)
	: spawnCooldown(200.f)
	, spawnOrigin(spawnOrigin)
	, backLine(30.f)
	, trackWidth(20.f)
	, spawnDistance(40.f)
	, levelDistance(100)
	, currentLevel(1)
	, clearedLevels(0)
{
	SetLevel(4);
	spawnTimer.Start();
	poolAllocator = GameEngine::GetInstance()->GetMemoryManager()->CreatePool(100, sizeof(Obstacle), 0); // #TODO: How many obstacles are we gonna use?


	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Set what collision box, texture and sound the obstacle should use. Set to 0 if no asset should be used.
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	packageName = "gameAssets4"; // The package that GetGuid() looks in
	ObstacleAssets oa;
	// Dwarf
	oa.collisionBox = GetGuid("dwarf.col");
	oa.texture = GetGuid("dwarf.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("dwarf.qwe"), oa));

	// Mii
	oa.collisionBox = GetGuid("mii.col");
	oa.texture = GetGuid("mii.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("mii.qwe"), oa));

	// Luigi
	oa.collisionBox = GetGuid("luigi.col");
	oa.texture = GetGuid("luigi.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("luigi.qwe"), oa));

	// Mushroom
	oa.collisionBox = GetGuid("mushroom.col");
	oa.texture = GetGuid("mushroom.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("mushroom.qwe"), oa));

	////////////////////////////////////////////
	// Animal theme
	////////////////////////////////////////////
	packageName = "gameAssets5";
	// Wolf
	oa.collisionBox = GetGuid("wolf.col");
	oa.texture = GetGuid("wolf.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("wolf.qwe"), oa));

	// Boar
	oa.collisionBox = GetGuid("boar.col");
	oa.texture = GetGuid("boar.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("boar.qwe"), oa));

	// Bear
	oa.collisionBox = GetGuid("bear.col");
	oa.texture = GetGuid("bear.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("bear.qwe"), oa));

	// Wolf
	oa.collisionBox = GetGuid("deer.col");
	oa.texture = GetGuid("deer.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("deer.qwe"), oa));

	////////////////////////////////////////////
	// Random theme
	////////////////////////////////////////////
	packageName = "gameAssets6";
	// Sofa
	oa.collisionBox = GetGuid("sofa.col");
	oa.texture = GetGuid("sofa.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("sofa.qwe"), oa));

	// Duck
	oa.collisionBox = GetGuid("duck.col");
	oa.texture = GetGuid("duck.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("duck.qwe"), oa));

	// Horse
	oa.collisionBox = GetGuid("horse.col");
	oa.texture = GetGuid("horse.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("horse.qwe"), oa));

	// Pot
	oa.collisionBox = GetGuid("pot.col");
	oa.texture = GetGuid("pot.dds");
	oa.sound = 0;
	obstaclePrefabs.insert(std::make_pair(GetGuid("pot.qwe"), oa));

	floorColors.push_back({ 0.f / 255.f, 0.f / 255.f, 0.f / 255.f, 1.f });
	floorColors.push_back({ 120.f / 255.f, 0.f / 255.f, 10.f / 255.f, 1.f });
	floorColors.push_back({ 100.f / 255.f, 0.f / 255.f, 100.f / 255.f, 1.f });

	floorColor = floorColors[0];

	// Create walls
	leftWall = new StaticPhysicsGameObject(GameEngine::GetInstance()->GetGraphicsManager(), GameEngine::GetInstance()->GetAssetManager(), 
	{ -1 * trackWidth*0.5f - 1.f, 1.25f, 23000.f }, 
	{ 1.0f, 5.0f, 50000.0f },
	{ 0.f / 255.f, 0.f / 255.f, 0.f / 255.f, 1.f });

	rightWall = new StaticPhysicsGameObject(GameEngine::GetInstance()->GetGraphicsManager(), GameEngine::GetInstance()->GetAssetManager(), 
	{ trackWidth*0.5f + 1.f, 1.25f, 23000.f }, 
	{ 1.0f, 5.0f, 50000.0f }, 
	{ 0.f / 255.f, 0.f / 255.f, 0.f / 255.f, 1.f });

	Prefetch(currentLevel + 4);
}

ObstacleManager::~ObstacleManager()
{

}

void ObstacleManager::Update()
{
	playerPosition = { spawnOrigin->x, spawnOrigin->z };

	if(playerPosition.y / (levelDistance * (clearedLevels + 1)) >= 1)
		ChangeLevel();

	// Spawn new obstacles when timer reaches spawnCooldown
	if(spawnTimer.GetTimeMilliseconds() > spawnCooldown)
	{
		SpawnNewObstacle();
		//spawnCooldown = GetRandomFloat(250, 300);
		spawnTimer.Reset();
	}

	// Update obstacles and removes them if they are behind backline
	for(int i = 0; i < obstacles.size(); i++)
	{
		obstacles[i]->Update();
		if(IsObstacleBehindBackLine(obstacles[i]))
			RemoveObstacle(obstacles[i], i);
	}

	leftWall->Update();
	rightWall->Update();
}

void ObstacleManager::Draw()
{
	for(auto& obstacle : obstacles)
	{
		obstacle->Draw();
	}

	//Draw Floor
	GameEngine::GetInstance()->GetGraphicsManager()->DrawCube(
	{ 0.f,-0.5f, playerPosition.y },
	{ trackWidth+2.f,1.f,2000.f },
	floorColor);

	leftWall->Draw();
	rightWall->Draw();
}

void ObstacleManager::PauseSpawning()
{
	spawnTimer.Stop();
}

void ObstacleManager::ResumeSpawning()
{
	spawnTimer.Start();
}

void ObstacleManager::SpawnNewObstacle()
{
	// Get random model and find prefab for that model
	int randomValue = GetRandomFloat(0, modelGuids.size());
	uint64_t modelGuid = modelGuids[randomValue];
	ObstacleAssets ao = obstaclePrefabs[modelGuid];

	// Load model
	Model* model = GameEngine::GetInstance()->GetAssetManager()->LoadAsync<Model>(modelGuid);
	
	
	CollisionBox* collisionBox = nullptr;
	if (ao.collisionBox != 0)
		collisionBox = GameEngine::GetInstance()->GetAssetManager()->Load<CollisionBox>(ao.collisionBox);
	else
		std::cout << "Can not create obstacle with no collsion box!, guid of the obstacle model: " << modelGuid << "\n";

	// If the prefab has texture and sound load them as well 
	DDSTexture* texture = nullptr;
	if(ao.texture != 0)
		texture = GameEngine::GetInstance()->GetAssetManager()->LoadAsync<DDSTexture>(ao.texture);
	
	Sound* sound = nullptr;
	if(ao.sound != 0)
		sound = GameEngine::GetInstance()->GetAssetManager()->LoadAsync(ao.sound);

	// Uses pool allocator to save obstacle in memory
	if (collisionBox)
	{
		Obstacle* obstacle = new(Obstacle, *poolAllocator, model, collisionBox, GetRandomSpawnLocation());

		obstacle->SetTexture(texture);
		obstacle->SetSound(sound);

		obstacles.push_back(obstacle);
	}
}

void ObstacleManager::SetLevel(int level)
{
	// Update model guids with models guids from package: number level
	modelGuids = GameEngine::GetInstance()->GetAssetManager()->GetModelGUIDSFromLevel(level);
}

void ObstacleManager::AddPlayer(Player* player)
{
	players.push_back(player);
}

DirectX::XMFLOAT3 ObstacleManager::GetRandomSpawnLocation()
{
	float randomXposition = GetRandomFloat(-trackWidth / 2.f, trackWidth / 2.f);

	float x = randomXposition;
	float z = spawnOrigin->z + spawnDistance;

	return DirectX::XMFLOAT3(x, 0, z);
}

bool ObstacleManager::IsObstacleBehindBackLine(Obstacle* obstacle)
{
	if(obstacle->m_position.z + backLine < playerPosition.y)
		return true;
	else
		return false;
}

void ObstacleManager::RemoveObstacle(Obstacle* obstacle, int i)
{
	delete(Obstacle, *poolAllocator, obstacle);
	obstacles.erase(obstacles.begin() + i);
}

uint64_t ObstacleManager::GetGuid(std::string fileName)
{
	std::string filePath = packageName + fileName;
	uint64_t guid = CRCGenerator::Crc64(filePath.c_str(), filePath.size());
	return guid;
}

float ObstacleManager::GetRandomFloat(float min, float max)
{
	return (min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min))));
}

void ObstacleManager::ChangeLevel()
{
	clearedLevels++;
	currentLevel++;
	currentLevel = Wrap(currentLevel, 1, 3);
	SetLevel(currentLevel + 3);

	float speedIncrease = GetCameraSpeedIncrement();
	Prefetch(currentLevel % 3 + 4);
	static_cast<GameCamera*>(GameEngine::GetInstance()->GetCamera())->IncreaseMinSpeed(speedIncrease);

	for (auto& player : players)
		player->IncreaseMinSpeeds(speedIncrease);

	spawnCooldown -= spawnCooldown / 5.f;
	//std::cout << "Increased camera speed with: " << GetCameraSpeedIncrement() << "\n";
	floorColor = floorColors[currentLevel - 1];

}

int ObstacleManager::Wrap(int kX, int const kLowerBound, int const kUpperBound)
{
	int range_size = kUpperBound - kLowerBound + 1;

	if (kX < kLowerBound)
		kX += range_size * ((kLowerBound - kX) / range_size + 1);

	return kLowerBound + (kX - kLowerBound) % range_size;
}

float ObstacleManager::GetCameraSpeedIncrement()
{
	return(1 / std::powf(((((float)clearedLevels - 1.f) / 2.f) + 1.1f), 2.f) + 0.1);
}

void ObstacleManager::Prefetch(int archiveNumber)
{

	std::string packageName = "gameAssets" + std::to_string(archiveNumber);

	GameEngine::GetInstance()->GetAssetManager()->LoadRipPackage(packageName);
}
