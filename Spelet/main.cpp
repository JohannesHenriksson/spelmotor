//Comment out to stop using custom memory manager
#define USING_MEMORY_MANAGER


#include <iostream>
#include <vector>
#include <cstdarg>
#include <new>


#include <boost\thread.hpp>
#include <boost\chrono.hpp>

#include <assert.h>

#include <Spelmotor/Timer.h>

#include "newOverride.h"

#include <Spelmotor\AssetManager.h>
#include <Spelmotor\GameEngine.h>
#include <Spelmotor\Sound.h>

#include <Spelmotor/VertexShader.h>
#include <Spelmotor/PixelShader.h>
#include <Spelmotor/Model.h>
#include <Spelmotor/GraphicsManager.h>
#include <Spelmotor/GameObject.h>
#include <Spelmotor/PhysicsGameObject.h>
#include <Spelmotor/GameEngine.h>
#include <Spelmotor/Camera.h>
#include <Spelmotor/Player.h>
#include <Spelmotor/DDSTexture.h>
#include "GameCamera.h"

#include <Spelmotor\FreeFlightCamera.h>

#include "ObstacleManager.h"

struct Vertex
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT2 Tex_Coord;
	Vertex(DirectX::XMFLOAT3 pos, DirectX::XMFLOAT3 norm, DirectX::XMFLOAT2 tex)
		: Position(pos), Normal(norm), Tex_Coord(tex)
	{ };
};


int main()
{
	GameEngine* gameEngine = GameEngine::GetInstance();
	gameEngine->GetAssetManager()->SetMaxRamUsage(28'000'000);
	FreeFlightCamera ffCamera = FreeFlightCamera();
	GameCamera gameCamera = GameCamera();
	GameCamera scenarioCamera = GameCamera({ 0.0f, 0.0f, 0.0f }, { -1.0f, 0.75f, 0.0f });

	Timer gameTimer;
	
	Player player = Player(gameEngine->GetGraphicsManager(), gameEngine->GetAssetManager(), .5f);
	player.SetPosition({ 0.f, 2.f, 20.f });
	player.SetBind(PLAYER_ACTION::MOVE_FORWARD, 'W');
	player.SetBind(PLAYER_ACTION::MOVE_BACKWARD, 'S');
	player.SetBind(PLAYER_ACTION::MOVE_LEFT, 'A');
	player.SetBind(PLAYER_ACTION::MOVE_RIGHT, 'D');

	Player player2 = Player(gameEngine->GetGraphicsManager(), gameEngine->GetAssetManager(), .5f);
	player2.SetPosition({ 10.f, 2.f, 20.f });
	player2.SetBind(PLAYER_ACTION::MOVE_FORWARD, 'I');
	player2.SetBind(PLAYER_ACTION::MOVE_BACKWARD, 'K');
	player2.SetBind(PLAYER_ACTION::MOVE_LEFT, 'J');
	player2.SetBind(PLAYER_ACTION::MOVE_RIGHT, 'L');

	ObstacleManager om = ObstacleManager(&gameCamera.m_roadPosition);
	om.AddPlayer(&player);
	om.AddPlayer(&player2);

	gameCamera.AddPlayer(&player);
	gameCamera.AddPlayer(&player2);

	gameEngine->SetCamera(&gameCamera);

	gameTimer.Start();
	while (gameEngine->IsRunning())
	{
		float dtNS = gameTimer.GetDeltaTimeNanoseconds();
		float dtMS = dtNS * 0.000'001;
		float dtS = dtMS * 0.001;
		gameEngine->UpdateLightPosition(DirectX::XMFLOAT4(player.GetPosition().x, 2, player.GetPosition().z - 2, 1), DirectX::XMFLOAT4(player2.GetPosition().x, 2, player2.GetPosition().z - 2, 1));
		gameEngine->Update(dtMS);

		if (Input::GetAsyncKeyState('O'))
			gameEngine->SetCamera(&gameCamera);

		if (Input::GetAsyncKeyState('P'))
			gameEngine->SetCamera(&scenarioCamera);

		if (Input::GetAsyncKeyState('U'))
			gameEngine->GetAssetManager()->PrintMemoryUsage();


		om.Update();
		player.Update();
		player.Draw(); 
		player2.Update();
		player2.Draw();

		gameCamera.Update(dtS);
		scenarioCamera.SetNewRoadLocation(gameCamera.m_roadPosition);

		ffCamera.Update(dtS);

		om.Draw();
		gameEngine->Draw();
	}

	delete GameEngine::GetInstance();
	return 0;
}

