#pragma once

#include <vector>
#include <Spelmotor\GameObject.h>
#include <Spelmotor\Timer.h>
#include "Obstacle.h"
#include <Spelmotor\memoryManager.h>
#include <Spelmotor/StaticPhysicsGameObject.h>

class Player;
class DDSTexture;

class ObstacleManager
{
public:
	/**
	* Creates an Obstacle manager which handles obstacle spawning an deletion
	*
	* \param spawnOrigin pointer to camera position to know where to spawn obstacles
	*/
	ObstacleManager(DirectX::XMFLOAT3* spawnOrigin);
	~ObstacleManager();

	/**
	* Updates obstacle manager
	*
	*/
	void Update();
	/**
	* Draws all obstacles
	*
	*/
	void Draw();
	/**
	* Pauses the spawning of new obstacles
	*
	*/
	void PauseSpawning();
	/**
	* Resumes spawning of new obstacles
	*
	*/
	void ResumeSpawning();

	/**
	* Sets level, level is the package that the obstacle manager will get the assets for obstacles from
	*
	*/
	void SetLevel(int level);

	void AddPlayer(Player* player);
private:

	// The assets that are used by an obstacle, the only thing needed for an obstacle is a model but its nice with collision, textures and sound too
	struct ObstacleAssets
	{
		uint64_t collisionBox;
		uint64_t texture;
		uint64_t sound;
	};

	/**
	* Spawns a new obstacle
	*
	*/
	void SpawnNewObstacle();
	/**
	* Get random location, uses 
	*
	*/
	DirectX::XMFLOAT3 GetRandomSpawnLocation();
	/**
	* Checks if the obstacle is behind the back line
	*
	* \param obstacle Pointer to the obstacle that should be checked
	*
	* \returns true if obstacle is behind back line
	*
	*/
	bool IsObstacleBehindBackLine(Obstacle* obstacle);
	/**
	* Remves an obstacle
	*
	*/
	void RemoveObstacle(Obstacle* obstacle, int i);
	/**
	* Helper function to get guid from file name, Set member variable 'packageName' to the package you want the guid from
	*
	* \param fileName The name of the file with file ending (ex: "apa.txt")
	*/
	uint64_t GetGuid(std::string fileName);
	std::string packageName;

	/**
	* Helper function to get a random float between two values
	*
	* \param min Lowest possible value
	* \param max Highest possible value
	*
	* \returns random float
	*/
	float GetRandomFloat(float min, float max);

	void ChangeLevel();
	int Wrap(int kX, int const kLowerBound, int const kUpperBound);
	float GetCameraSpeedIncrement();

	void Prefetch(int archiveNumber);

	// vector with pointers to all active obstacles
	std::vector<Obstacle*> obstacles;

	// Maps from model guid to texture and sound if there are any
	std::map<uint64_t, ObstacleAssets> obstaclePrefabs;

	// vector with all models in current levels package
	std::vector<uint64_t> modelGuids;
	
	Timer spawnTimer;
	float spawnCooldown;

	DirectX::XMFLOAT2 playerPosition;

	DirectX::XMFLOAT3* spawnOrigin;

	StaticPhysicsGameObject* leftWall;
	StaticPhysicsGameObject* rightWall;

	PoolAllocator* poolAllocator;

	// When obstacle is behind back line it gets removed
	float backLine;
	// Width of the "track", obstacles spawns inside this width
	float trackWidth;
	// How far ahead of the player that obtstacles should pop up
	float spawnDistance;

	int clearedLevels;
	int currentLevel;
	int levelDistance;

	std::vector<DirectX::XMFLOAT4> floorColors;
	DirectX::XMFLOAT4 floorColor;

	std::vector<Player*> players;
};