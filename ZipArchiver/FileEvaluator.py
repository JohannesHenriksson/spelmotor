import os
import zipfile
import ZipArchiver

# Checks for file updates/removes/additions and rebuilds zip archives if changed
if __name__ == '__main__':
	os.chdir("../Spelet/Assets")

	guidsHasChanged = False
	hasRun = False
	# Loops all directories and files in root
	for root, dirs, files in os.walk(os.getcwd()):
		# For all directories in root, excluding root directory
		if hasRun:
			# Checks if zip exists for directory, otherwise creates it
			if archives[0] + ".zip" in zips:
				archivedWhen = os.stat(archives[0] + ".zip").st_mtime
				zipf = zipfile.ZipFile(archives[0] + '.zip', 'r', zipfile.ZIP_DEFLATED)
				counter = 0
				# Checks if same amount of files in zip and directory
				if len(files) != len(zipf.filelist):
					print("Rebuilt zip archive: " + archives[0])
					guidsHasChanged = True
					ZipArchiver.ArchiveZip(archives[0])
				else:
					# Loops files in directory
					for file in files:
						# Checks if file is not same as in zip
						if zipf.filelist[counter].filename != file:
							print("Rebuilt zip archive: " + archives[0])
							guidsHasChanged = True
							ZipArchiver.ArchiveZip(archives[0])
							break
						counter += 1
						# Checks if file is updated since last zip
						if archivedWhen < os.stat(archives[0] + '/' + file).st_mtime:
							print("Rebuilt zip archive: " + archives[0])
							guidsHasChanged = True
							ZipArchiver.ArchiveZip(archives[0])
							break
				zipf.close()
			# Creates zip if not existing for currect directory
			else:
				print("Created zip archive: " + archives[0])
				guidsHasChanged = True
				ZipArchiver.ArchiveZip(archives[0])
			archives.pop(0)
		# For root directory
		else:
			# Saves root directory list 
			archives = dirs[:]
			zips = files[:]
			# Loops zips in root and checks if directory is removed
			for zip in zips:
				if zip.split(".")[0] not in archives:
					if zip.split(".")[1] == "zip":
						print("Removed zip archive: " + zip.split(".")[0])
						os.remove(zip)
			hasRun = True

	# Rebuild enum GUID
	if guidsHasChanged:
		guidFile = open("../../Spelmotor/GUID.h", 'r')
		# Saves C++ code before and after enum
		lines = guidFile.readlines()
		counter = 0
		for line in lines:
			counter += 1
			if '{' in line:
				break
		startLines = lines[:counter]
		counter = 0
		for line in lines:
			counter += 1
			if '};' in line:
				break
		middleLines = lines[counter - 1:]
		counter = 0
		switchRow = 0
		for line in middleLines:
			counter += 1
			if 'switch (guid)' in line:
				switchRow = counter
			if 'default:' in line:
				break
		endLines = middleLines[counter - 1:]
		middleLines = middleLines[:switchRow + 1]
		guidFile.close()
		guidFile = open("../../Spelmotor/GUID.h", 'w')
		guidFile.writelines(startLines)
		
		fileNames = []
		guids = []
		archiveNumber = 0
		fileNumber = 0
		hasRun = False
		# Writes enum
		for root, dirs, files in os.walk(os.getcwd()):
			if hasRun:
				for file in files:
					fileNames.append(file.split('.')[0] + "_" + file.split('.')[1])
					guids.append(" = 0x" + format(archiveNumber, '02x') + format(fileNumber, '02x') + "0000" + format(os.stat(archives[0] + '/' + file).st_size, '08x'))
					fileNumber += 1
				fileNumber = 0
				archiveNumber += 1
				archives.pop(0)
			else:
				archives = dirs[:]
				hasRun = True
		
		counter = 0
		for file in fileNames:
			guidFile.writelines('\t' + file + guids[counter] + ',\n')
			counter += 1

		guidFile.writelines(middleLines)

		counter = 0
		for file in fileNames:
			guidFile.writelines('\t' + 'case ' + file + ':\n')
			guidFile.writelines('\t\treturn "' + file + '";\n')
			counter += 1

		guidFile.writelines(endLines)
		guidFile.flush()
		guidFile.close()