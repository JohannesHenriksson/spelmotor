﻿import xml.etree.cElementTree as ET
import os
import hashlib
import crc64
import zlib

def ArchiveFolder(archiveName):
	archiveFile = open(archiveName + '.rip', 'wb')
	for root, dirs, files in os.walk(os.getcwd() + '/' + archiveName):
		break
	lines = []
	offset = 0
	compressedFiles = []
	archiveFile.writelines([len(files).to_bytes(2, byteorder='little', signed=False)])
	for file in files:
        # Open and compress file
		fs = open(os.getcwd() + '/' + archiveName + '/' + file, 'rb')
		compressedFile = zlib.compress(fs.read(), 9)
		compressedSize = len(compressedFile)
		lines.append(int(crc64.crc64digest(archiveName + file), 16).to_bytes(8, byteorder='little', signed=False))
		lines.append((os.stat(os.getcwd() + '/' + archiveName + '/' + file).st_size).to_bytes(4, byteorder='little', signed=False))
		lines.append(compressedSize.to_bytes(4, byteorder='little', signed=False))
		lines.append(offset.to_bytes(4, byteorder='little', signed=False))
		offset += compressedSize
		compressedFiles.append(compressedFile)
	archiveFile.writelines(lines)
	archiveFile.writelines(compressedFiles)
	archiveFile.flush()
	archiveFile.close()


if __name__ == '__main__':
	os.chdir("../Spelet/Assets")

	guidsHasChanged = False
	hasRun = False
	# Loops all directories and files in root
	for root, dirs, files in os.walk(os.getcwd()):
		# For all directories in root, excluding root directory
		if hasRun:
			# Checks if rip exists for directory, otherwise creates it
			if archives[0] + ".rip" in rips:
				archivedWhen = os.stat(archives[0] + ".rip").st_mtime
				ripf = open(archives[0] + '.rip', 'rb')
				data = ripf.read()
				amountOfFiles = data[0] + data[1] * 256
				counter = 0
				# Checks if same amount of files in rip and directory
				if len(files) != amountOfFiles:
					print("Rebuilt rip archive: " + archives[0])
					guidsHasChanged = True
					ArchiveFolder(archives[0])
				else:
					# Loops files in directory
					for file in files:
						# Checks if file is not same as in rip
						guid = data[2 + counter * 20:10 + counter * 20]
						# guid = int.from_bytes(archivedNameList, byteorder='big', signed=False)
						hashedFile = int(crc64.crc64digest(archives[0] + file), 16).to_bytes(8, byteorder='little', signed=False) 
						if guid != hashedFile:
							print("Rebuilt zip archive: " + archives[0])
							guidsHasChanged = True
							ArchiveFolder(archives[0])
							break
						counter += 1
						# Checks if file is updated since last rip
						if archivedWhen < os.stat(archives[0] + '/' + file).st_mtime:
							print("Rebuilt rip archive: " + archives[0])
							guidsHasChanged = True
							ArchiveFolder(archives[0])
							break
				ripf.close()
			# Creates rip if not existing for currect directory
			else:
				print("Created rip archive: " + archives[0])
				guidsHasChanged = True
				ArchiveFolder(archives[0])
			archives.pop(0)
		# For root directory
		else:
			# Saves root directory list 
			archives = dirs[:]
			rips = files[:]
			# Loops rips in root and checks if directory is removed
			for rip in rips:
				if rip.split(".")[0] not in archives:
					if rip.split(".")[1] == "rip":
						print("Removed rip archive: " + rip.split(".")[0])
						os.remove(rip)
			if "GUID.xml" not in rips:
				guidsHasChanged = True
			hasRun = True

	# Rebuild GUID.XML
	if guidsHasChanged:
		print("Creating GUID.xml")
		fileNames = []
		guids = []
		fileSizes = []
		archiveNumber = 0
		fileNumber = 0
		hasRun = False
		# Writes GUIDs
		for root, dirs, files in os.walk(os.getcwd()):
			if hasRun:
				fileNames.append(files[:])
				for file in files:
					file.encode('utf-8')
					size = os.stat(archives[0] + '/' + file).st_size
					guids.append([crc64.crc64digest(archives[0] + file), str(fileNumber), file, archives[0], file.split('.')[1], str(size)])
					fileSizes.append(size)
					fileNumber += 1
				fileNumber = 0
				archiveNumber += 1
				archives.pop(0)
			else:
				archives = dirs[:]
				hasRun = True
		
		# Create XML
		roots = ET.Element("root")
		for guid in guids:
			sub = ET.SubElement(roots, '0x' + guid[0])
			ET.SubElement(sub, 'FileNumber').text = guid[1]
			ET.SubElement(sub, 'FileName').text = guid[2]
			ET.SubElement(sub, 'PackageName').text = guid[3]
			ET.SubElement(sub, 'FileType').text = guid[4].upper()
			ET.SubElement(sub, 'Size').text = guid[5]

		tree = ET.ElementTree(roots)
		tree.write("GUID.xml")
