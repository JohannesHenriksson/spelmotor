import os
import zipfile

# Creates zip archive from directory
def ArchiveZip(archiveName):
	zipf = zipfile.ZipFile(archiveName + '.zip', 'w', zipfile.ZIP_DEFLATED)
	for root, dirs, files in os.walk(os.getcwd() + '/' + archiveName):
		for file in files:
			zipf.write(archiveName + '/' + file, file)
	zipf.close()

# Rebuilds all zip archives in Assets
if __name__ == '__main__':
	os.chdir("../Spelet/Assets")
	guidFile = open("../../Spelmotor/GUID.h", 'r')
	lines = guidFile.readlines()
	counter = 0
	# Saves C++ code before and after enum
	for line in lines:
		counter += 1
		if '{' in line:
			break
	startLines = lines[:counter]
	counter = 0
	for line in lines:
		counter += 1
		if '};' in line:
			break
	middleLines = lines[counter - 1:]
	counter = 0
	switchRow = 0
	for line in middleLines:
		counter += 1
		if 'switch (guid)' in line:
			switchRow = counter
		if 'default:' in line:
			break
	endLines = middleLines[counter - 1:]
	middleLines = middleLines[:switchRow + 1]
	guidFile.close()
	
	guidFile = open("../../Spelmotor/GUID.h", 'w')
	guidFile.writelines(startLines)
	
	fileNames = []
	guids = []
	archiveNumber = 0
	fileNumber = 0
	hasRun = False
	# Loops all directories and files in Assets
	for root, dirs, files in os.walk(os.getcwd()):
		# For directory in root
		if hasRun:
			# Create zip archive for directory
			zipf = zipfile.ZipFile(archives[0] + '.zip', 'w', zipfile.ZIP_DEFLATED)
			for file in files:
				# Writes file to zip
				zipf.write(archives[0] + '/' + file, file)
				# Write enum
				fileNames.append(file.split('.')[0] + "_" + file.split('.')[1])
				guids.append(" = 0x" + format(archiveNumber, '02x') + format(fileNumber, '02x') + "0000" + format(os.stat(archives[0] + '/' + file).st_size, '08x'))
				fileNumber += 1
			zipf.close()
			fileNumber = 0
			archiveNumber += 1
			archives.pop(0)
		# For root directory
		else:
			# Saves root directory list 
			archives = dirs[:]
			zips = files[:]
			for zip in zips:
				if zip.split(".")[1] == "zip":
					os.remove(zip)
			hasRun = True
				
	counter = 0
	for file in fileNames:
		guidFile.writelines('\t' + file + guids[counter] + ',\n')
		counter += 1

	guidFile.writelines(middleLines)

	counter = 0
	for file in fileNames:
		guidFile.writelines('\t' + 'case ' + file + ':\n')
		guidFile.writelines('\t\treturn "' + file + '";\n')
		counter += 1
	guidFile.writelines(endLines)
	guidFile.flush()
	guidFile.close()
