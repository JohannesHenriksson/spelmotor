﻿import xml.etree.cElementTree as ET
import os
import hashlib
import zipfile
import ZipArchiver

if __name__ == '__main__':
	os.chdir("../Spelet/Assets")

	guidsHasChanged = False
	hasRun = False
	# Loops all directories and files in root
	for root, dirs, files in os.walk(os.getcwd()):
		# For all directories in root, excluding root directory
		if hasRun:
			# Checks if zip exists for directory, otherwise creates it
			if archives[0] + ".zip" in zips:
				archivedWhen = os.stat(archives[0] + ".zip").st_mtime
				zipf = zipfile.ZipFile(archives[0] + '.zip', 'r', zipfile.ZIP_DEFLATED)
				counter = 0
				# Checks if same amount of files in zip and directory
				if len(files) != len(zipf.filelist):
					print("Rebuilt zip archive: " + archives[0])
					guidsHasChanged = True
					ZipArchiver.ArchiveZip(archives[0])
				else:
					# Loops files in directory
					for file in files:
						# Checks if file is not same as in zip
						if zipf.filelist[counter].filename != file:
							print("Rebuilt zip archive: " + archives[0])
							guidsHasChanged = True
							ZipArchiver.ArchiveZip(archives[0])
							break
						counter += 1
						# Checks if file is updated since last zip
						if archivedWhen < os.stat(archives[0] + '/' + file).st_mtime:
							print("Rebuilt zip archive: " + archives[0])
							guidsHasChanged = True
							ZipArchiver.ArchiveZip(archives[0])
							break
				zipf.close()
			# Creates zip if not existing for currect directory
			else:
				print("Created zip archive: " + archives[0])
				guidsHasChanged = True
				ZipArchiver.ArchiveZip(archives[0])
			archives.pop(0)
		# For root directory
		else:
			# Saves root directory list 
			archives = dirs[:]
			zips = files[:]
			# Loops zips in root and checks if directory is removed
			for zip in zips:
				if zip.split(".")[0] not in archives:
					if zip.split(".")[1] == "zip":
						print("Removed zip archive: " + zip.split(".")[0])
						os.remove(zip)
			if "GUID.xml" not in zips:
				guidsHasChanged = True
			hasRun = True

	# Rebuild GUID.XML
	if guidsHasChanged:
		print("Creating GUID.xml")
		fileNames = []
		guids = []
		fileSizes = []
		archiveNumber = 0
		fileNumber = 0
		hasRun = False
		# Writes GUIDs
		for root, dirs, files in os.walk(os.getcwd()):
			if hasRun:
				fileNames.append(files[:])
				for file in files:
					file.encode('utf-8')
					guids.append(hashlib.md5((archives[0] + file).encode('utf-8')).hexdigest())
					fileSizes.append(os.stat(archives[0] + '/' + file).st_size)
					fileNumber += 1
				fileNumber = 0
				archiveNumber += 1
				archives.pop(0)
			else:
				archives = dirs[:]
				archives2 = dirs[:]
				hasRun = True
		
		# Create XML
		roots = ET.Element("root")
		counter = 0
		archiveCounter = 0
		arch = []
		for archive in fileNames:
			fileNumber = 0
			arch.append(ET.SubElement(roots, archives2[0]))
			archives2.pop(0)
			for file in archive:
				ET.SubElement(arch[archiveCounter], file.split('.')[0], type=file.split('.')[1], fileNumber=str(fileNumber), size=str(fileSizes[counter])).text = guids[counter]
				counter += 1
				fileNumber += 1
			archiveCounter += 1
		tree = ET.ElementTree(roots)
		tree.write("GUID.xml")
