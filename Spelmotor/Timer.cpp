#include "Timer.h"



Timer::Timer() 
	: running(false)
{ 
	m_startTime = std::chrono::high_resolution_clock::now();
	m_stopTime = std::chrono::high_resolution_clock::now();
}

Timer::~Timer()
{ }

void Timer::Start()
{
	running = true;
	m_lastDeltaTime = std::chrono::high_resolution_clock::now();
}

void Timer::Stop()
{
	running = false;
	m_stopTime = std::chrono::high_resolution_clock::now();
}

void Timer::Reset()
{
	m_startTime = std::chrono::high_resolution_clock::now();
	m_lastDeltaTime = std::chrono::high_resolution_clock::now();
}

float Timer::GetTimeSeconds()
{
	if(running)
		return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - m_startTime).count();
	else
		return std::chrono::duration_cast<std::chrono::seconds>(m_stopTime - m_startTime).count();

}

float Timer::GetTimeMilliseconds()
{
	if(running)
		return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - m_startTime).count();
	else
		return std::chrono::duration_cast<std::chrono::milliseconds>(m_stopTime - m_startTime).count();
}

float Timer::GetTimeMicroseconds()
{
	if(running)
		return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - m_startTime).count();
	else
		return std::chrono::duration_cast<std::chrono::microseconds>(m_stopTime - m_startTime).count();
}

long long Timer::GetDeltaTimeNanoseconds()
{
	if (running)
	{
		std::chrono::high_resolution_clock::time_point currentTime = std::chrono::high_resolution_clock::now();
		long long microSeconds = std::chrono::duration_cast<std::chrono::nanoseconds>(currentTime - m_lastDeltaTime).count();
		m_lastDeltaTime = currentTime;
		return microSeconds;
	}
	return 0;
}

