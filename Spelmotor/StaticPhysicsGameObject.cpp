#include "StaticPhysicsGameObject.h"
#include "PhysicsManager.h"



StaticPhysicsGameObject::StaticPhysicsGameObject(GraphicsManager* gm, AssetManager* assetManager, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 size, DirectX::XMFLOAT4 color)
	: GameObject(gm, assetManager)
{
	m_physObj = PhysicsManager::GetInstance()->CreateStatic(physx::PxVec3(position.x, position.y, position.z), physx::PxBoxGeometry( size.x * .5f, size.y * 5.f, size.z * .5f), 1.0f, 1.0f, 0.34f);
	m_scale = size;
	m_position = position;
	m_model = Model::GetDefaultModel();
	m_instanceData.color = color;
}

StaticPhysicsGameObject::~StaticPhysicsGameObject()
{
}
