#ifndef DDS_TEXTURE_H
#define DDS_TEXTURE_H

#include "Texture.h"

class DDSTexture :
	public Texture
{
public:
	DDSTexture();
	~DDSTexture();

private:
	ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int size, const std::string& name, MemoryTracker* memTracker) override;
	void Unload(MemoryTracker* memTracker);
	friend class AssetManager;
};
#endif
