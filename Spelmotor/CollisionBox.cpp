#include "CollisionBox.h"


CollisionBox::CollisionBox()
{
}


CollisionBox::~CollisionBox()
{
}

ASSET_ERROR_CODES CollisionBox::Load(ID3D11Device * device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker)
{
	float* floatData = static_cast<float*>(data);

	middlePoint = { floatData[0], floatData[1], floatData[2] };
	halfExtents = { floatData[3], floatData[4], floatData[5] };

	memTracker->Submit_RAM(AssetType::PHYSICS, GetSize(), dataSize);

	isLoaded = true;
	return ASSET_ERROR_CODES::SUCCESS;
}

void CollisionBox::Unload(MemoryTracker* memTracker)
{

}

size_t CollisionBox::GetSize()
{
	return sizeof(CollisionBox);
}
