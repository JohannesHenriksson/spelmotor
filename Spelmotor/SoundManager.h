#pragma once

#include <DirectXMath.h>
#include <iostream>
#include "fmod.hpp"
#include "fmod_errors.h"
#include <map>
#include "AssetErrorCodes.h"

class Sound;
class Audio;

class SoundManager
{


public:
	~SoundManager();
	static SoundManager* GetInstance();
	
	void Update();
	//Positive value to increase negative to decrease.
	void ChangeVolume(Sound* sound, float value);
	void Pause(Sound* sound);
	void PlayLooped(Sound* sound);
	void PlayLooped(Sound* sound, DirectX::XMFLOAT3* position, DirectX::XMFLOAT3* velocity = nullptr);
	void PlayOnce(Sound* sound);
	void PlayOnce(Sound* sound, DirectX::XMFLOAT3* position);
	ASSET_ERROR_CODES CreateSound(void* data, int size, Audio* audio);
private:
	ASSET_ERROR_CODES FMODErrorCheck(FMOD_RESULT result);
	FMOD::Channel* Play(FMOD::Sound* sound);
	std::map<Sound, int> channelsUsed;
	FMOD_SPEAKERMODE speakerMode;
	FMOD::System *system;
	FMOD_RESULT result;

	SoundManager();
	static SoundManager* instance;
};

