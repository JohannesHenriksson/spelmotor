#ifndef ASSET_MANAGER
#define ASSET_MANAGER

#include <map>
#include <boost\thread.hpp>
#include <boost\chrono.hpp>

#include "GUID.h"
#include "Unpacker.h"
#include <d3d11.h>
#include "memoryManager.h"
#include <vector>
#include "Crc64.h"
#include "ReplacementPolicies.h"
#include "AssetUnloader.h"
#include "MemoryTracker.h"


class Asset;
class MemoryManager;
class Sound;
class AssetUnloader;

enum class PackageFormat : int
{
	RIP,
	ZIP
};
enum class FileType : int 
{
	DDS,
	QWE,
	CSO,
	OBJ,
	WAV,
	COL,
	OGG,
	UNKNOWN
};

class AssetManager
{
private:
	struct GUIDTableInfo
	{
		unsigned int fileNumber;
		std::string fileName;
		std::string packageName;
		FileType fileType;
		unsigned int size;
	};

	std::map<uint64_t/*GUID*/, Asset*> assets;
	MemoryManager* memoryManager;
	Unpacker* unpacker;
	ID3D11Device* device;
	AssetUnloader* unloader;
	MemoryTracker* memTracker;
	std::vector<std::pair<bool, StackAllocator*>> loadStacks;
	std::mutex stackLock;
	AssetType GetAssetType(FileType fileType);
	void SetMaxVramUsage(int maxVram);
	//Checks if wee need to free memory and does so if necessary
	bool ManageMemory(GUIDTableInfo fileInfo);


	std::map<uint64_t/*GUID*/, GUIDTableInfo> m_guidLookupTable;

	void LoadGUIDLookupTable();

	void StartLoadSoundFileFromPackage(std::string archivePath, uint64_t guid, GUIDTableInfo lookupData, uint32_t offset, uint32_t compressedSize, std::string fileName);
	void LoadSoundFileFromPackage(std::string archivePath, uint64_t guid, GUIDTableInfo lookupData, uint32_t offset, uint32_t compressedSize, std::string fileName);

	template <class A>
	void StartLoadThreadFromPackage(std::string archivePath, uint64_t guid, GUIDTableInfo lookupData, uint32_t offset, uint32_t compressedSize, std::string fileName)
	{
		boost::thread* t = new boost::thread(&AssetManager::LoadFileFromPackage<A>, this, archivePath, guid, lookupData, offset, compressedSize, fileName);
	}

	template <class A>
	void LoadFileFromPackage(std::string archivePath, uint64_t guid, GUIDTableInfo lookupData, uint32_t offset, uint32_t compressedSize, std::string fileName)
	{
		auto it = assets.find(guid);
		if (it != assets.end())
			return;
		A* newAsset = new A();

		if (!ManageMemory(lookupData))
		{
			std::cout << "failed to load memory" << std::endl;
			delete newAsset;
			return;
		}

		stackLock.lock();

		std::pair<bool, StackAllocator*>* loadStackPair = FindFreeLoadStack();
		while (loadStackPair == nullptr)
		{
			loadStackPair = FindFreeLoadStack();
		}
		StackAllocator* loadStack = loadStackPair->second;
		loadStackPair->first = false;

		stackLock.unlock();

		void* data = loadStack->Alloc(lookupData.size);

		//Read data from package
		unpacker->ReadFromArchiveOffsetRip(archivePath, offset, compressedSize, lookupData.size, data);

		//load the asset with the data
		ASSET_ERROR_CODES res = newAsset->Load(device, data, lookupData.size, fileName, memTracker);
		if (res == ASSET_ERROR_CODES::SUCCESS)
		{
			newAsset->m_nrOfReferences = 0;
			newAsset->m_GUID = guid;
			assets[guid] = newAsset;
		}
		else
		{
			delete newAsset;
			std::cout << "Failed to load content with GUID: " << guid << "Error code: " << GetAssetErrorCodeString(res) << std::endl;
		}

		loadStack->Release();
		loadStackPair->first = true;
	}


	std::pair<bool, StackAllocator*>* FindFreeLoadStack();
public:
	static std::mutex freeMemLock;

	AssetManager(MemoryManager* memManager, ID3D11Device* device);
	AssetManager(const AssetManager &obj);
	void UnloadAllAssets();
	~AssetManager();
	void SetReplacementPolicy(ReplacementPolicy policy);

	void LoadRipPackage(const std::string& packageName);
	Sound* Load(uint64_t guid, PackageFormat packageFormat = PackageFormat::RIP);
	Sound* LoadAsync(uint64_t guid, boost::thread_group& threadGroup, PackageFormat packageFormat = PackageFormat::RIP);
	Sound* LoadAsync(uint64_t guid, PackageFormat packageFormat = PackageFormat::RIP);
	void LoadSoundAsync(uint64_t guid, Sound*& sound, PackageFormat packageFormat);

	void PrintMemoryUsage();
	//Max ram in bytes
	void SetMaxRamUsage(int maxRam);

	/**
	* Get all model guids from specific package
	*
	* \param level Which package to get models from
	*
	* \returns Vector with guids
	*/
	std::vector<uint64_t> GetModelGUIDSFromLevel(int level);
	
	template <class A>
	void Unload(Asset* asset)
	{
		if (!asset)
			return;
		asset->m_nrOfReferences--;
		if (asset->m_nrOfReferences <= 0)
		{
			unloader->ReportUnusedAsset(asset);

			/*if(m_guidLookupTable[asset->m_GUID].fileType == FileType::QWE)
				std::cout << "Asset: " << m_guidLookupTable[asset->m_GUID].fileName << " is unused! \n";*/
		}
	}

	template <class A>
	A* LoadAsync(uint64_t guid, boost::thread_group& threadGroup, PackageFormat packageFormat = PackageFormat::RIP)
	{
		
		auto a = assets.find(guid);
		if(a != assets.end())
		{
			a->second->m_nrOfReferences++;
			return static_cast<A*>(a->second);
		}

		A* newAsset = new A();

		newAsset->m_nrOfReferences++;
		assets[guid] = newAsset;
		boost::thread* t = new boost::thread(&AssetManager::LoadTask<A>, this, guid, newAsset, packageFormat);

		threadGroup.add_thread(t);
		return newAsset;
	}

	template <class A>
	A* LoadAsync(uint64_t guid, PackageFormat packageFormat = PackageFormat::RIP)
	{
		auto a = assets.find(guid);
		if (a != assets.end())
		{
			a->second->m_nrOfReferences++;
			return static_cast<A*>(a->second);
		}

		A* newAsset = new A();

		newAsset->m_nrOfReferences++;
		assets[guid] = newAsset;

		/*if(m_guidLookupTable[guid].fileType == FileType::QWE)
			std::cout << "Asset: " << m_guidLookupTable[guid].fileName << " is being loaded \n";*/
		boost::thread* t = new boost::thread(&AssetManager::LoadTask<A>, this, guid, newAsset, packageFormat);

		return newAsset;
	}
	
	template <class A>
	A* LoadTask(uint64_t guid, A* assetPtr, PackageFormat packageFormat = PackageFormat::RIP)
	{
		auto lookUpData = m_guidLookupTable.find(guid);
		if(lookUpData == m_guidLookupTable.end())
		{
			std::cout << "Unrecognized GUID: " << guid << std::endl;
			return nullptr;
		}
		int size = lookUpData->second.size;

		if (!ManageMemory(lookUpData->second))
		{
			std::cout << "Failed to unload memory for new asset" << std::endl;
			return nullptr;
		}

		stackLock.lock();

		std::pair<bool, StackAllocator*>* loadStackPair = FindFreeLoadStack();
		while(loadStackPair == nullptr)
		{
			boost::this_thread::sleep(boost::posix_time::milliseconds(20));
			loadStackPair = FindFreeLoadStack();
		}
		StackAllocator* loadStack = loadStackPair->second;
		loadStackPair->first = false;

		stackLock.unlock();

		void* data = loadStack->Alloc(size);

		boost::this_thread::sleep(boost::posix_time::milliseconds(2000));

		//Read data from package
		switch(packageFormat)
		{
			case PackageFormat::RIP:
				unpacker->ReadFromArchiveOffsetCustom(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
				break;
			case PackageFormat::ZIP:
				unpacker->ReadFromArchiveOffsetZip(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
				break;
			default:
				std::cout << "Unrecognized package format" << std::endl;
				break;
		}

		//load the asset with the data
		ASSET_ERROR_CODES res = assetPtr->Load(device, data, size, lookUpData->second.fileName, memTracker);
		if(res == ASSET_ERROR_CODES::SUCCESS)
		{
			//assetPtr->m_nrOfReferences = 1;
			assetPtr->m_GUID = guid;
			

			//memTracker->PrintMemoryUsePerType();

			loadStack->Release();
			loadStackPair->first = true;
			return static_cast<A*>(assets[guid]);
		}
		else
		{
			delete assetPtr;
			std::cout << "Failed to load content with GUID: " << guid << "Error code: " << GetAssetErrorCodeString(res) << std::endl;

			assets[guid] = nullptr;
			loadStack->Release();
			loadStackPair->first = true;
			return nullptr;
		}
	}

	template <class A>
	A* Load(uint64_t guid, PackageFormat packageFormat = PackageFormat::RIP)
	{
		auto a = assets.find(guid);
		if (a != assets.end())
		{
			a->second->m_nrOfReferences++;
			return static_cast<A*>(a->second);
		}
		
		A* newAsset = new A();
		auto lookUpData = m_guidLookupTable.find(guid);
		if (lookUpData == m_guidLookupTable.end())
		{
			std::cout << "Unrecognized GUID: " << guid << std::endl;
			delete newAsset;
			return nullptr;
		}
		int size = lookUpData->second.size;
		
		if (!ManageMemory(lookUpData->second))
		{
			std::cout << "failed to unload memory" << std::endl;
			delete newAsset;
			return nullptr;
		}
		
		void* data = malloc(size);

		//Read data from package
		switch (packageFormat)
		{
		case PackageFormat::RIP:
			unpacker->ReadFromArchiveOffsetCustom(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
			break;
		case PackageFormat::ZIP:
			unpacker->ReadFromArchiveOffsetZip(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
			break;
		default:
			std::cout << "Unrecognized package format" << std::endl;
			break;
		}

		//load the asset with the data
		ASSET_ERROR_CODES res = newAsset->Load(device, data, size, lookUpData->second.fileName, memTracker);
		if (res == ASSET_ERROR_CODES::SUCCESS)
		{
			newAsset->m_nrOfReferences = 1;
			newAsset->m_GUID = guid;
			assets[guid] = newAsset;

			/*loadStack->Release();
			loadStackPair->first = true;*/
			free(data);
			//memTracker->PrintMemoryUsePerType();
			return static_cast<A*>(assets[guid]);
		}
		else
		{
			delete newAsset;
			std::cout << "Failed to load content with GUID: " << guid << "Error code: " << GetAssetErrorCodeString(res) << std::endl;

			/*loadStack->Release();
			loadStackPair->first = true;*/
			free(data);
			return nullptr;
		}
	}

	//Filename should include .filetype, packageName should not include filetype
	//Do not use in release version
	template <class A>
	A* Load(const std::string& fileName, const std::string& packageName, PackageFormat packageFormat = PackageFormat::RIP)
	{
		std::string filePath = packageName + fileName;
		auto guid = CRCGenerator::Crc64(filePath.c_str(), filePath.size());
		return Load<A>(guid, packageFormat);
	}

	void PrintUnloaded(uint64_t guid)
	{
		auto lookUpData = m_guidLookupTable.find(guid);
		if (lookUpData == m_guidLookupTable.end())
		{
			return;
		}
		std::cout << "Unloaded: " << lookUpData->second.fileName << "\n";
	}
};

#endif // !ASSET_MANAGER