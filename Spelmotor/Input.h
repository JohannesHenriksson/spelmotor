#ifndef Input_h__
#define Input_h__

#include <functional>
#include <chrono>
#include <vector>

#include "keyState.h"
#include "timer.h"

#include <windows.h>
#include <DirectXMath.h>
#include <iostream>

class Input
{
public:
	/**
	* Initializes input and listens to hWnd
	*
	* \param hWnd
	*/
	static void Init(HWND hWnd);
	/**
	* Updates mouse delta and locks the cursor if cursor locking is on
	*
	* \see LockCursor
	*/
	static void Update();

	/**@{*/

	/**
	* Sets the callback function for a given action
	*
	* \param callback
	*/
	static int RegisterKeyCallback(std::function<void(const KeyState&)> callback);
	static int RegisterMouseButtonCallback(std::function<void(const KeyState&)> callback);
	static int RegisterCharCallback(std::function<void(int)> callback);
	static int RegisterScrollCallback(std::function<void(UINT)> callback);
	static int RegisterResizeCallback(std::function<void(int, int)> callback);
	/**@}*/

	/**
	* Unregisters a callback function for a given action
	*
	* \param callback
	*/
	static void UnregisterKeyCallback(int keyCallbackID);
	static void UnregisterMouseButtonCallback(int mouseButtonCallbackID);
	static void UnregisterCharCallback(int charCallbackID);
	static void UnregisterScrollCallback(int scrollCallbackID);
	static void UnregisterResizeCallback(int resizeCallbackID);

	/**
	* Locks the cursor to te given position.
	*
	* Position is relative to the screen. Set xPosition and yPosition to -1 to stop locking
	*
	* \param xPosition x position to lock to or -1 to stop locking
	* \param yPosition y position to lock to or -1 to stop locking
	*/
	static void LockCursor(int xPosition, int yPosition);

	/**
	* Hides the cursor
	*/
	static void HideCursor();
	/**
	* Shows the cursor
	*/
	static void ShowCursor();

	/**
	* Gets the position of the mouse when the given button was pressed
	*
	* \param button
	*
	* \returns cursor position
	*/
	static DirectX::XMFLOAT2 GetMouseDownPos(MOUSE_BUTTON button);
	/**
	* Gets the position of the mouse when the given button was released
	*
	* \param button
	*
	* \returns cursor position
	*/
	static DirectX::XMFLOAT2 GetMouseUpPos(MOUSE_BUTTON button);

	/**
	* Gets the current mouse position
	*
	* \returns the current position of the mouse
	*/
	static DirectX::XMFLOAT2 GetMousePosition();
	/**
	* Gets how far the mouse has moved since the last time Update() was called
	*
	* \returns the distance the mouse has moved
	*/
	static DirectX::XMFLOAT2 GetMouseDelta();
	/**
	* Whether or not the mouse has moved since the last time Update() was called
	*
	* \returns whether or not the mouse has moved
	*/
	static bool MouseMoved();

	/**
	* Gets the current window size (client area)
	*
	* \returns
	*/
	static DirectX::XMFLOAT2 GetWindowSize();

	/**
	* Sets the maximum time (in milliseconds) for two consecutive clicks to count as double clicks
	*
	* \param ms
	*/
	static void SetDoubleClickDelay(unsigned int ms);

	/**
	* Gets the HWND reference to the window that Input currently listens to
	*
	* \returns
	*/
	static HWND GetListenWindow();

	/**
	* Checks whether or not the given key is currently down
	*
	* Example:
	* if(Input::GetAsyncKeyState(VK_LEFT))
	*     //Left arrow key is down
	*
	* \param key
	* \returns
	*/
	static bool GetAsyncKeyState(int key);
	/**
	* \see GetAsyncKeyState, supports multiple keys e.g. CTRL | ALT
	*/
	static bool GetAsyncKeyModifierState(KEY_MODIFIERS modifier);

	/**@{*/
	/**
	* Events that are called from WndProc
	*
	* \param msg
	* \param wParam
	* \param lParam
	*/
	static void KeyEvent(UINT msg, WPARAM wParam, LPARAM lParam);
	static void CharEvent(unsigned int key);
	static void MouseButtonEvent(UINT msg, WPARAM wParam);
	static void ScrollEvent(UINT distance);
	static void ResizeEvent(int xResolution, int yResolution);
	/**@}*/
private:
	Input();
	~Input();

	static KEY_MODIFIERS GetCurrentModifierKeys();
	static HWND listenWindow;

	static std::vector<std::pair<int, std::function<void(const KeyState&)>>> keyCallbacks;
	static std::vector<std::pair<int, std::function<void(const KeyState&)>>> mouseButtonCallbacks;
	static std::vector<std::pair<int, std::function<void(int)>>>			 charCallbacks;
	static std::vector<std::pair<int, std::function<void(UINT)>>>			 scrollCallbacks;
	static std::vector<std::pair<int, std::function<void(int, int)>>>		 resizeCallbacks;

	////////////////////////////////////////////////////////////
	//MOUSE
	////////////////////////////////////////////////////////////
	static DirectX::XMFLOAT2 mouseDownPos[3];
	static DirectX::XMFLOAT2 mouseUpPos[3];
	static DirectX::XMFLOAT2 mousePosition;
	static DirectX::XMFLOAT2 oldMousePosition;

	static MOUSE_BUTTON lastMouseButton;
	static Timer lastClickTimer;
	static std::chrono::milliseconds doubleClickDelay;

	const static int maxDoubleClickDistance = 2;

	static int xLock;
	static int yLock;
};

#endif // Input_h__