#ifndef POOL_ALLOCATOR_H
#define POOL_ALLOCATOR_H

#include <cerrno>
#include <cstring>
#include <string>
#include <iostream>
#include <mutex>

class PoolAllocator
{
private:
	union Block
	{
		Block* next;
	};

	char* pool;
	void* start;

	Block* nextFreeBlock;
	int currentBlockID;
	int lastBlockID;

	size_t elementSize;
	int numberOfBlocks;
	std::mutex mutexLock;
	
public:
	PoolAllocator(){}
	//PoolAllocator(PoolAllocator& poolCopy) {}
	// Allocates memory for the whole pool, size_t poolSize is size in bytes
	PoolAllocator(int numberOfElements, int blockSize)
		: nextFreeBlock(nullptr)
	{
		elementSize = blockSize;

		// Min element size is the size of a pointer
		if (elementSize < sizeof(char*))
			elementSize = sizeof(char*);

		numberOfBlocks = numberOfElements;
		size_t poolSize = elementSize * numberOfBlocks;

		start = reinterpret_cast<void*>(malloc(poolSize + elementSize));
		pool = reinterpret_cast<char*>(start);

		// Outputs error message if malloc failes
		char buff[256];
		if (pool == nullptr)
		{
			strerror_s(buff, errno);
			std::cout << "Malloc failed: " << buff << '\n';	
			
		}

		// Alignment
		//charPointer += 1; // Unalign for testing
		size_t padding = PadPointer(pool, alignof(Block));
		pool = reinterpret_cast<char*>(pool + padding);

		//bool isaligned = !((long)pool % alignof(Block));

		lastBlockID = (int)(poolSize / elementSize) - 1;
		if (padding > 0)
			currentBlockID = 1;
		else
			currentBlockID = 0;
	};


	~PoolAllocator() 
	{
		free(start);
	};

	// Allocates the next free block in the pool, returns pointer to the allocated block.
	template <typename T>
	T* Allocate(bool threadSafe = false) 
	{
		if (sizeof(T) > elementSize)
		{
			std::cout << "Cannot allocate memory for elements with sizes bigger than \"" << elementSize << "\" in this pool";
			return  nullptr;
		}
		if(threadSafe)
			mutexLock.lock();
		T* result;
		if (nextFreeBlock != nullptr)
		{
			result = reinterpret_cast<T*>(nextFreeBlock);
			nextFreeBlock = nextFreeBlock->next;
		}
		else
		{
			if (currentBlockID > lastBlockID)
			{
				std::cout << "Pool is full \n";
				result = nullptr;
			}
			else
			{
				result = reinterpret_cast<T*>(pool + (elementSize * currentBlockID++));
			}
		}
		if (threadSafe)
			mutexLock.unlock();
		return result;
	};

	// Deallocates the block that the inparameter pointer points to
	void Deallocate(void* pointer, bool threadSafe = false)
	{
		if (threadSafe)
			mutexLock.lock();
		if (pointer != nullptr)
		{
			reinterpret_cast<Block*>(pointer)->next = nextFreeBlock;
			nextFreeBlock = reinterpret_cast<Block*>(pointer);
		}
		if (threadSafe)
			mutexLock.unlock();
	};

	size_t PadPointer(char* p, size_t align)
	{
		uintptr_t result = reinterpret_cast<uintptr_t>(p);
		return ((align - result) % align);
	};
};
#endif // POOL_ALLOCATOR_H