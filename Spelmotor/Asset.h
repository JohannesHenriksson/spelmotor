#ifndef ASSET_H
#define ASSET_H


#include <d3d11.h>
#include "AssetErrorCodes.h"
#include <string>
#include "MemoryTracker.h"

#ifndef SafeRelease
#define SafeRelease(x) if(x) {x->Release(); x = nullptr;}
#endif


class Asset
{
public:
	Asset();
	~Asset();
	
	bool IsLoaded() const 
	{
		return isLoaded;
	};
protected:

	virtual ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker) = 0;
	virtual void Unload(MemoryTracker* memTracker) = 0;
	virtual size_t GetSize() = 0;


	int m_nrOfReferences;
	uint64_t m_GUID;
	bool isLoaded;

private:

	friend class AssetManager;
	friend class AssetUnloader;
};


inline void SetDebugObjectName( ID3D11DeviceChild* resource, const std::string& name)
{
#if defined(_DEBUG) || defined(PROFILE)
	resource->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.c_str());
#endif
}

struct ComDeleter
{
	void operator()(IUnknown* ptr) const
	{
		if(ptr != nullptr)
			ptr->Release();
		ptr = nullptr;
	}
};

#endif