#include "VertexShader.h"
#include <comdef.h>
#include <iostream>

VertexShader::VertexShader()
{ }


VertexShader::~VertexShader()
{ }

ID3D11VertexShader* VertexShader::GetShader() const
{
	return m_vertexShader.get();
}

ID3D11InputLayout* VertexShader::GetInputLayout() const
{
	return m_inputLayout.get();
}

void VertexShader::SetInputLayout(ID3D11InputLayout* inputLayout)
{
	m_inputLayout.reset(inputLayout);
}

void VertexShader::CreateInputLayout(ID3D11Device* device, const std::vector<D3D11_INPUT_ELEMENT_DESC>& inputDescs)
{
	ID3D11InputLayout* tempLayout = nullptr;

	if (!isLoaded)
	{
		std::cout << "Shader is not loaded, can't create input layout" << std::endl;
		return;
	}

	auto hr = device->CreateInputLayout(&inputDescs[0], inputDescs.size(), m_shaderSource->GetBufferPointer(), m_shaderSource->GetBufferSize(), &tempLayout);
	if(FAILED(hr))
	{
		_com_error err(hr);
		LPCTSTR errMsg = err.ErrorMessage();
		std::cout << "Failed to create input layout: " << errMsg << std::endl;
		return;
	}
	m_inputLayout.reset(tempLayout);
}

ASSET_ERROR_CODES VertexShader::Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker)
{
	LoadShaderSource(data, dataSize);
	if(m_shaderSource == nullptr)
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_READ_FILE;

	ID3D11VertexShader* tempShader;
	HRESULT hr = device->CreateVertexShader(m_shaderSource->GetBufferPointer(), m_shaderSource->GetBufferSize(), nullptr, &tempShader);

	if(FAILED(hr))
	{
		_com_error err(hr);
		LPCTSTR errMsg = err.ErrorMessage();
		std::cout << "Failed to create vertexshader: " << errMsg << std::endl;
		return ASSET_ERROR_CODES::ERROR_UNKOWN;
	}

	m_vertexShader.reset(tempShader);

	SetDebugObjectName(m_vertexShader.get(), name);

	isLoaded = true;

	memTracker->Submit_RAM(AssetType::SHADER, GetSize(), dataSize);

	return ASSET_ERROR_CODES::SUCCESS;
	//Shader reflections mabye??!
}

void VertexShader::Unload(MemoryTracker* memTracker)
{
	memTracker->Remove_RAM(AssetType::SHADER, GetSize());

	m_inputLayout.reset(nullptr);
	m_vertexShader.reset(nullptr);
	m_shaderSource.reset(nullptr);

	isLoaded = false;
}
