#include "DDSTexture.h"

#include "ddstextureloader.h"
#include <string>
#include <iostream>

DDSTexture::DDSTexture()
{ }


DDSTexture::~DDSTexture()
{ }


ASSET_ERROR_CODES DDSTexture::Load(ID3D11Device* device, void* data, int size, const std::string& name, MemoryTracker* memTracker)
{
	ID3D11Resource* tempResource;
	ID3D11ShaderResourceView* tempSRV;
	if(FAILED(DirectX::CreateDDSTextureFromMemory(device, reinterpret_cast<uint8_t*>(data), size, &tempResource, &tempSRV)))
	{

		std::cout << "Failed to read dds texture from data" << std::endl;
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_CREATE_DIRECTXOBJECT;
	}

	tempResource->QueryInterface(IID_ID3D11Texture2D, (void**)&m_Texture);
	SafeRelease(tempResource);

	m_TextureSRV.reset(tempSRV);

	D3D11_TEXTURE2D_DESC texDesc;
	m_Texture->GetDesc(&texDesc);

	m_textureWidth = texDesc.Width;
	m_textureHeight = texDesc.Height;
	m_bitsPerPixel = GetBitsPerPixel(texDesc.Format);

	isLoaded = true;

	memTracker->Submit_RAM(AssetType::TEXTURE, GetSize(), size);
	return ASSET_ERROR_CODES::SUCCESS;
}

void DDSTexture::Unload(MemoryTracker* memTracker)
{
	memTracker->Remove_RAM(AssetType::TEXTURE, GetSize());

	m_TextureSRV.reset(nullptr);
	m_Texture.reset(nullptr);
	isLoaded = false;
}