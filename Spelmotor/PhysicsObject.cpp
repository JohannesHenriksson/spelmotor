#include "PhysicsObject.h"
#include "DxMath.h"


PhysicsObject::PhysicsObject(CollisionMesh * collisionMesh, DirectX::XMFLOAT3* renderPosition, DirectX::XMFLOAT4* rotation)
	: m_renderPosition(renderPosition)
	, m_renderRotation(rotation)
	, m_physManager(PhysicsManager::GetInstance())
{
}

PhysicsObject::PhysicsObject(SimpleShapes shape, DirectX::XMFLOAT3* renderPosition, DirectX::XMFLOAT4* rotation, DirectX::XMFLOAT3 halfExtents)
	: m_renderPosition(renderPosition)
	, m_renderRotation(rotation)
	, m_physManager(PhysicsManager::GetInstance())
{
	switch (shape)
	{
	case SimpleShapes::BOX:
		m_dynamic = m_physManager->CreateDynamic(physx::PxVec3(renderPosition->x, renderPosition->y, renderPosition->z), physx::PxBoxGeometry(halfExtents.x, halfExtents.y, halfExtents.z));
		break;
	case SimpleShapes::SPHERE:
		m_dynamic = m_physManager->CreateDynamic(physx::PxVec3(renderPosition->x, renderPosition->y, renderPosition->z), physx::PxSphereGeometry(halfExtents.x), nullptr);
		break;
	}
}

void PhysicsObject::Update()
{
	*m_renderRotation = { m_dynamic->getGlobalPose().q.x, m_dynamic->getGlobalPose().q.y, m_dynamic->getGlobalPose().q.z, m_dynamic->getGlobalPose().q.w };

	if (m_renderOffset != nullptr)
	{
		DirectX::XMFLOAT3 rotatedRenderOffset;
		DirectX::XMStoreFloat3(&rotatedRenderOffset, DirectX::XMVector3Rotate(DirectX::XMLoadFloat3(m_renderOffset), DirectX::XMLoadFloat4(m_renderRotation)));
		*m_renderPosition = DirectX::XMFLOAT3(m_dynamic->getGlobalPose().p.x, m_dynamic->getGlobalPose().p.y, m_dynamic->getGlobalPose().p.z) - rotatedRenderOffset;
	}
	else
		*m_renderPosition = DirectX::XMFLOAT3(m_dynamic->getGlobalPose().p.x, m_dynamic->getGlobalPose().p.y, m_dynamic->getGlobalPose().p.z);

	
}


void PhysicsObject::AddForce(DirectX::XMFLOAT3 direction, float magnitude)
{
	DirectX::XMFLOAT3 tempVec = direction * magnitude;
	physx::PxVec3 resPxVec = { tempVec.x, tempVec.y, tempVec.z };
	m_dynamic->addForce(resPxVec);
}

physx::PxRigidDynamic* PhysicsObject::GetRigidDynamic() const
{
	return m_dynamic;
}

PhysicsObject::~PhysicsObject()
{
	m_physManager->RemoveObject(m_dynamic);
}
