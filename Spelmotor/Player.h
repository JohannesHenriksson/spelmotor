#pragma once
#include "PhysicsGameObject.h"

enum class PLAYER_ACTION : uint8_t
{
	MOVE_RIGHT,
	MOVE_LEFT,
	MOVE_FORWARD,
	MOVE_BACKWARD,
	JUMP
};

class Player :
	public PhysicsGameObject
{
public:
	Player(GraphicsManager* gm, AssetManager* assetManager, float radius);
	~Player();

	void Update() override;
	void HandleInput();
	void SetBind(PLAYER_ACTION action, int newKeybinding);
	float GetPlayerZVelocity();

	void IncreaseMinSpeeds(float speedIncrease);
private:
	float m_accelerationForce;
	float m_maxSpeedZ;
	float m_maxSpeedX;
	std::map<PLAYER_ACTION, int/*KeyBind*/> m_keyBinds;
};

