#pragma once
#include "CollisionMesh.h"
#include "SimplePhysicsShapes.h"
#include "PhysicsManager.h"
#include <d3d11.h>
#include <directxmath.h>

class PhysicsObject
{
public:
	PhysicsObject(CollisionMesh* collisionMesh, DirectX::XMFLOAT3* position, DirectX::XMFLOAT4* rotation);
	PhysicsObject(SimpleShapes shape, DirectX::XMFLOAT3* position, DirectX::XMFLOAT4* rotation, DirectX::XMFLOAT3 halfExtents);
	void Update();
	void AddForce(DirectX::XMFLOAT3 direction, float magnitude);
	physx::PxRigidDynamic* GetRigidDynamic() const;
	~PhysicsObject();

	// offset from collision box origin and render origin
	DirectX::XMFLOAT3* m_renderOffset = nullptr;
private:
	CollisionMesh* m_collisionMesh = nullptr;
	physx::PxRigidDynamic* m_dynamic = nullptr;
	PhysicsManager* m_physManager = nullptr;
	DirectX::XMFLOAT3* m_renderPosition = nullptr;
	DirectX::XMFLOAT4* m_renderRotation = nullptr;
};

