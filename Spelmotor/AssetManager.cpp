#include "AssetManager.h"
#include "memoryManager.h"
#include "Sound.h"
#include "Model.h"
#include "Texture.h"
#include "DDSTexture.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "CollisionBox.h"

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"
#include "AssetUnloader.h"

std::mutex AssetManager::freeMemLock;

AssetManager::AssetManager(MemoryManager* memManager, ID3D11Device* device) 
	: memoryManager(memManager)
	, device(device)
{
	unpacker = new Unpacker();
	memTracker = new MemoryTracker();
	LoadGUIDLookupTable();
	unloader = new AssetUnloader(&assets);
	


	Model::LoadDefaultModel(device, memTracker);
	Texture::LoadDefaultTexture(device, memTracker);

	for (int i = 0; i < 5; i++)
	{
		StackAllocator* loadStack = memManager->CreateStack(5'000'000, i); // #TODO: How to determine size?
		loadStacks.push_back(std::make_pair(true, loadStack));
	}
}

void AssetManager::UnloadAllAssets()
{
	Model::defaultModel->Unload(memTracker);
	Texture::defaultTexture->Unload(memTracker);
	delete Model::defaultModel;
	delete Texture::defaultTexture;

	for (auto it : assets)
	{
		it.second->Unload(memTracker);
		delete it.second;
	}
	assets.clear();
}

AssetManager::AssetManager(const AssetManager &obj)
{
}

AssetManager::~AssetManager()
{
	delete unpacker;
	UnloadAllAssets();
}

void AssetManager::SetReplacementPolicy(ReplacementPolicy policy)
{
	unloader->SetPolicy(policy);
}

std::pair<bool, StackAllocator*>* AssetManager::FindFreeLoadStack()
{
	for(int i = 0; i < loadStacks.size(); i++)
	{
		if (loadStacks[i].first == true)
		{
			//std::cout << "loadstack nr: " << i << " is free \n";
			return &loadStacks[i];
		}	
	}

	//std::cout << "No free stacks";

	return nullptr;
}

void AssetManager::LoadRipPackage(const std::string& packageName)
{
	std::vector<uint64_t> fileVector = unpacker->GetRipArchiveGuids(packageName);
	auto it = fileVector.begin();
	int headerOffset = *it;
	uint64_t guid;
	uint32_t compressedSize;
	uint32_t offset;
	GUIDTableInfo assetData;
	for (it++; it < fileVector.end(); it++)
	{
		guid = *it;
		it++;
		compressedSize = (uint32_t)*it;
		offset = headerOffset + (uint32_t)(*it >> 32);

		auto a = assets.find(guid);
		if (a != assets.end())
			continue;

		assetData = m_guidLookupTable[guid];

		//std::cout << "Loading: " << assetData.fileName << "\n";
		switch (assetData.fileType)
		{
		case FileType::CSO:
			//TODO: Vertex/Pixel shader
			StartLoadThreadFromPackage<VertexShader>(packageName, guid, assetData, offset, compressedSize, assetData.fileName);
			break;
		case FileType::DDS:
			StartLoadThreadFromPackage<DDSTexture>(packageName, guid, assetData, offset, compressedSize, assetData.fileName);
			break;
		case FileType::OBJ:
			StartLoadThreadFromPackage<Model>(packageName, guid, assetData, offset, compressedSize, assetData.fileName);
			break;														
		case FileType::QWE:												
			StartLoadThreadFromPackage<Model>(packageName, guid, assetData, offset, compressedSize, assetData.fileName);
			break;
		case FileType::WAV:
		case FileType::OGG:
			StartLoadSoundFileFromPackage(packageName, guid, assetData, offset, compressedSize, assetData.fileName);
			break;
		case FileType::COL:
			StartLoadThreadFromPackage<CollisionBox>(packageName, guid, assetData, offset, compressedSize, assetData.fileName);
			break;
		case FileType::UNKNOWN:
		default:
			break;
		}
	}
}

Sound* AssetManager::LoadAsync(uint64_t guid, boost::thread_group& threadGroup, PackageFormat packageFormat)
{
	Sound* soundDummy = new Sound();

	auto a = assets.find(guid);
	if (a != assets.end())
	{
		a->second->m_nrOfReferences++;
		Audio* audio = static_cast<Audio*>(a->second);
		soundDummy->Init(audio);
		return soundDummy;
	}

	boost::thread* t = new boost::thread(&AssetManager::LoadSoundAsync, this, guid, soundDummy, packageFormat);
	threadGroup.add_thread(t);

	return soundDummy;
}

Sound* AssetManager::LoadAsync(uint64_t guid, PackageFormat packageFormat)
{
	Sound* soundDummy = new Sound();

	auto a = assets.find(guid);
	if (a != assets.end())
	{
		a->second->m_nrOfReferences++;
		Audio* audio = static_cast<Audio*>(a->second);
		soundDummy->Init(audio);
		return soundDummy;
	}

	boost::thread* t = new boost::thread(&AssetManager::LoadSoundAsync, this, guid, soundDummy, packageFormat);

	return soundDummy;
}

void AssetManager::LoadSoundAsync(uint64_t guid, Sound*& sound, PackageFormat packageFormat = PackageFormat::RIP)
{
	auto lookUpData = m_guidLookupTable.find(guid);
	if (lookUpData == m_guidLookupTable.end())
	{
		std::cout << "Unrecognized GUID: " << guid << std::endl;
	}
	int size = lookUpData->second.size;

	if(!ManageMemory(lookUpData->second))
	{
		std::cout << "Failed to unload memory for new asset" << std::endl;
		return;
	}

	stackLock.lock();

	std::pair<bool, StackAllocator*>* loadStackPair = FindFreeLoadStack();

	// If no stack is available, wait for one to become free
	while(loadStackPair == nullptr)
	{
		loadStackPair = FindFreeLoadStack();
	}

	StackAllocator* loadStack = loadStackPair->second;
	loadStackPair->first = false;

	stackLock.unlock();

	void* data = loadStack->Alloc(size);

	//Read data from package
	switch (packageFormat)
	{
	case PackageFormat::RIP:
		unpacker->ReadFromArchiveOffsetCustom(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
		break;
	case PackageFormat::ZIP:
		unpacker->ReadFromArchiveOffsetZip(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
		break;
	default:
		std::cout << "Unrecognized package format" << std::endl;
		break;
	}

	// Do stuff with data here
	Audio* audio = new Audio();
	ASSET_ERROR_CODES res = audio->Load(nullptr, data, size, lookUpData->second.fileName, memTracker);

	audio->m_nrOfReferences = 1;
	audio->m_GUID = guid;
	assets[guid] = audio;

	sound->Init(audio);
	

	// After data has been use, remove it from memory
	loadStack->Release();
	loadStackPair->first = true;

	// If function calls has been made from another thread while sound was loading, execute the calls now when the sound has been completely initialized
	while(sound->functionCalls.size() > 0)
	{
		sound->functionCalls[0]();
		sound->functionCalls.erase(sound->functionCalls.begin());
	}
}

void AssetManager::PrintMemoryUsage()
{
	memTracker->PrintMemoryUsePerType();
}

std::vector<uint64_t> AssetManager::GetModelGUIDSFromLevel(int level)
{
	std::string packageName = "gameAssets" + std::to_string(level);
	std::vector<uint64_t> fileVector = unpacker->GetRipArchiveGuids(packageName);
	std::vector<uint64_t> models;

	for(int i = 1; i < fileVector.size(); i+=2)
	{
		GUIDTableInfo assetInfo;
		try
		{
			assetInfo = m_guidLookupTable.at(fileVector[i]);
		}
		catch(const std::exception&)
		{
			std::cout << "Couldn't find guid: " << fileVector[i] << " in guid lookup table \n";
		}
		
		if(assetInfo.fileType == FileType::QWE || assetInfo.fileType == FileType::OBJ)
		{
			models.push_back(fileVector[i]);
		}
	}

	return models;
}

void AssetManager::LoadSoundFileFromPackage(std::string archivePath, uint64_t guid, GUIDTableInfo lookupData, uint32_t offset, uint32_t compressedSize, std::string fileName)
{

	if (!ManageMemory(lookupData))
	{
		std::cout << "Failed to load memory for new asset" << std::endl;
		return;
	}

	stackLock.lock();

	std::pair<bool, StackAllocator*>* loadStackPair = FindFreeLoadStack();

	// If no stack is available, wait for one to become free
	while (loadStackPair == nullptr)
		loadStackPair = FindFreeLoadStack();

	StackAllocator* loadStack = loadStackPair->second;
	loadStackPair->first = false;


	stackLock.unlock();

	void* data = loadStack->Alloc(lookupData.size);

	//Read data from package
	unpacker->ReadFromArchiveOffsetRip(archivePath, offset, compressedSize, lookupData.size, data);

	// Do stuff with data here
	Audio* audio = new Audio();
	audio->Load(nullptr, data, lookupData.size, fileName, memTracker);
	assets[guid] = audio;

	// After data has been use, remove it from memory
	loadStack->Release();
	loadStackPair->first = true;
}

void AssetManager::StartLoadSoundFileFromPackage(std::string archivePath, uint64_t guid, GUIDTableInfo lookupData, uint32_t offset, uint32_t compressedSize, std::string fileName)
{
	auto a = assets.find(guid);
	if (a != assets.end())
	{
		a->second->m_nrOfReferences++;
		Audio* audio = static_cast<Audio*>(a->second);
		return;
	}

	boost::thread* t = new boost::thread(&AssetManager::LoadSoundFileFromPackage, this, archivePath, guid, lookupData, offset, compressedSize, fileName);
}

Sound* AssetManager::Load(uint64_t guid, PackageFormat packageFormat)
{
	auto a = assets.find(guid);
	if (a != assets.end())
	{
		a->second->m_nrOfReferences++;
		Audio* audio = static_cast<Audio*>(a->second);
		Sound* sound = new Sound(audio);
		return sound;
	}

	auto lookUpData = m_guidLookupTable.find(guid);
	if (lookUpData == m_guidLookupTable.end())
	{
		std::cout << "Unrecognized GUID: " << guid << std::endl;
		return nullptr;
	}
	int size = lookUpData->second.size;

	stackLock.lock();

	std::pair<bool, StackAllocator*>* loadStackPair = FindFreeLoadStack();
	while (loadStackPair == nullptr)
	{
		loadStackPair = FindFreeLoadStack();
	}
	StackAllocator* loadStack = loadStackPair->second;
	loadStackPair->first = false;

	stackLock.unlock();

	void* data = loadStack->Alloc(size);

	//Read data from package
	switch (packageFormat)
	{
	case PackageFormat::RIP:
		unpacker->ReadFromArchiveOffsetCustom(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
		break;
	case PackageFormat::ZIP:
		unpacker->ReadFromArchiveOffsetZip(lookUpData->second.packageName, lookUpData->second.fileNumber, size, data);
		break;
	default:
		std::cout << "Unrecognized package format" << std::endl;
		break;
	}

	//If there is no loaded asset we create it from soundManager
	Audio* audio = new Audio();
	audio->Load(nullptr, data, size, lookUpData->second.fileName, memTracker);

	// After data has been use, remove it from memory
	loadStack->Release();
	loadStackPair->first = true;

	return new Sound(audio);
}

AssetType AssetManager::GetAssetType(FileType fileType)
{
	switch (fileType)
	{
		case FileType::CSO:
			return AssetType::SHADER;
		case FileType::DDS:
			return AssetType::TEXTURE;
		case FileType::OBJ:
		case FileType::QWE:
			return AssetType::MODEL;
		case FileType::WAV:
		case FileType::OGG:
			return AssetType::AUDIO;
		case FileType::COL:
			return AssetType::PHYSICS;
	}
	return AssetType::UNKNOWN;
}

void AssetManager::SetMaxRamUsage(int maxRam)
{
	memTracker->SetMax_RAM_Usage(maxRam);
}

void AssetManager::SetMaxVramUsage(int maxVram)
{
	memTracker->SetMax_VRAM_Usage(maxVram);
}

bool AssetManager::ManageMemory(GUIDTableInfo fileInfo)
{
	AssetManager::freeMemLock.lock();
	int size = fileInfo.size;
	if (fileInfo.fileType == FileType::QWE || fileInfo.fileType == FileType::OBJ)
		size *= 2.125f;
	auto memLeft = memTracker->Get_RAM_left();
	int availablRAM = memLeft - size;
	if (availablRAM < 0)
	{
		bool retVal = unloader->FreeMemory(abs(availablRAM), memTracker);
		if (retVal)
			memTracker->ReserveMemory(size);
		freeMemLock.unlock();
		return retVal;
	}
	memTracker->ReserveMemory(size);
	AssetManager::freeMemLock.unlock();
	return true;
}

void AssetManager::LoadGUIDLookupTable()
{
	using namespace rapidxml;
	//Load the GUID Lookup table
	rapidxml::file<>* xmlFile;
	rapidxml::xml_document<> assetGUIDTable;
	xmlFile = new rapidxml::file<>("Assets/GUID.xml");
	assetGUIDTable.parse<0>(xmlFile->data());

	auto rootNode = assetGUIDTable.first_node();

	for (xml_node<char>* currentNode = rootNode->first_node(); currentNode; currentNode = currentNode->next_sibling())
	{
		GUIDTableInfo assetInfo;
		std::string s(currentNode->name(), currentNode->name_size());
		uint64_t guid = std::strtoull(s.c_str(), NULL, 16);
		xml_node<char>* attributeNode = currentNode->first_node(); 

		//Read file number(offset in files in the package)
		assetInfo.fileNumber = atoi(attributeNode->value());
		
		//Read file name
		attributeNode = attributeNode->next_sibling();
		assetInfo.fileName = attributeNode->value();

		//Read package name
		attributeNode = attributeNode->next_sibling();
		assetInfo.packageName = attributeNode->value();

		//File type
		attributeNode = attributeNode->next_sibling();
		std::string val = attributeNode->value();
		if (val == "DDS")
			assetInfo.fileType = FileType::DDS;
		else if (val == "QWE")
			assetInfo.fileType = FileType::QWE;
		else if (val == "CSO")
			assetInfo.fileType = FileType::CSO;
		else if (val == "OBJ")
			assetInfo.fileType = FileType::OBJ;
		else if (val == "WAV")
			assetInfo.fileType = FileType::WAV;
		else if (val == "COL")
			assetInfo.fileType = FileType::COL;
		else if (val == "OGG")
			assetInfo.fileType = FileType::OGG;
		else
			assetInfo.fileType = FileType::UNKNOWN;

		//Read file size
		attributeNode = attributeNode->next_sibling();
		assetInfo.size = std::stoul(attributeNode->value());



		m_guidLookupTable[guid] = assetInfo;
	}
	delete xmlFile;
}