#pragma once
#include <map>
#include <vector>
#include "Shader.h"
#include "Model.h"
#include "Texture.h"
#include "VertexShader.h"
#include "PixelShader.h"

#ifdef _MSC_VER
#pragma warning(disable : 4503)
#endif


class Window;
struct InstanceData
{
	DirectX::XMFLOAT4X4 worldMatrix;
	DirectX::XMFLOAT4 color;
	
	InstanceData() : color({1.0f, 0.0f, 1.0f, 1.0f })
	{
		DirectX::XMStoreFloat4x4(&worldMatrix, DirectX::XMMatrixIdentity());
	}

	InstanceData(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT4 color) : color(color)
	{
		DirectX::XMStoreFloat4x4(&worldMatrix, DirectX::XMMatrixMultiply(
			DirectX::XMMatrixTranslationFromVector(DirectX::XMLoadFloat3(&position)),
			DirectX::XMMatrixScalingFromVector(DirectX::XMLoadFloat3(&scale))));
	}
};

struct ShaderPair
{
	VertexShader* vertexShader;
	PixelShader* pixelShader;

	ShaderPair() 
		: vertexShader(nullptr), pixelShader(nullptr)
	{ };
	ShaderPair(VertexShader* vertexShader, PixelShader* pixelShader)
		: vertexShader(vertexShader), pixelShader(pixelShader)
	{ };

	bool operator==(const ShaderPair& otherPair) const
	{
		return vertexShader == otherPair.vertexShader && pixelShader == otherPair.pixelShader;
	}
};

struct LightPos
{
	DirectX::XMFLOAT4 lightPos[2];
};

class GraphicsManager
{

public:
	GraphicsManager(Window* window, AssetManager* assetManager);
	~GraphicsManager();

	

	void InsertDrawData(ShaderPair* shaderPair, Model* model, Texture* texture, InstanceData instanceData);
	void DrawCube(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT4 color, Texture* tex = nullptr);

	ShaderPair* GetDefaultShaderPair() const;

	void DrawAllObjects();

	void UpdateConstantBuffer(DirectX::XMFLOAT4X4 viewProjectionMatrix, LightPos lightPos);
private:

	ID3D11Device* device;
	ID3D11DeviceContext* deviceContext;
	Window* window;
	AssetManager* assetManager;

	//Map for holding all objects sorted on shader, model and texture
	std::map < ShaderPair*, std::map < Model*, std::map < Texture*, std::vector<InstanceData>>>> drawMap;

	ShaderPair* m_DefaultShaderPair;
	ShaderPair* m_DefaultShaderPairNoTex;

	std::unique_ptr<ID3D11Buffer, ComDeleter> m_viewProjectionConstantBuffer;
	std::unique_ptr<ID3D11SamplerState, ComDeleter> m_samplerState;
	std::unique_ptr<ID3D11RasterizerState, ComDeleter> m_rasterizerState;

	static const int MAX_INSTANCES = 512;
	std::vector<InstanceData> cubes;
	ID3D11Buffer* m_instanceBuffer;
	ID3D11Buffer* m_lightBuffer;


	public:

};

