#include "stackAllocator.h"
#include <stdlib.h> 
#include <iostream>

StackAllocator::StackAllocator(size_t bufferSize) :
	m_buffer(nullptr),
	m_bufferSize(bufferSize),
	m_currentOffset(0),
	m_stackOverFlow(0)
{
	AllocateMemory(bufferSize);
}


StackAllocator::~StackAllocator()
{
	free(m_buffer);
}

void StackAllocator::Release()
{
	if (m_stackOverFlow > 0) 
	{
		std::cout << "\n**Stack too small, allocating more space**" << std::endl;
		AllocateMemory(CalcNewStackSize());
	}
	m_stackOverFlow = 0;
	m_currentOffset = 0;
}

int StackAllocator::CalcNewStackSize()
{
	return (m_stackOverFlow + m_bufferSize + (m_stackOverFlow / 2));
}

void StackAllocator::AllocateMemory(int bytes)
{
	//Realloc works just like malloc if the ptr is a nullptr
	std::cout << "Allocating " << bytes << " bytes" << std::endl;
	m_buffer = (char*)realloc(m_buffer, bytes);

	if (m_buffer == nullptr) 
	{
		std::cout << "Failed to allocate memory!";
	}

	m_bufferSize = bytes;
}

int StackAllocator::MemoryInUse()
{
	return m_currentOffset;
}

int StackAllocator::GetFreeMemory()
{
	return (m_bufferSize - m_currentOffset);
}

bool StackAllocator::isStackOverFLow(int dataTypeSize)
{
	int totalDataSize = m_currentOffset + dataTypeSize;
	int overFlow = totalDataSize - m_bufferSize;

	if (overFlow > 0)
	{
		//If we have an overflow we add the number of bytes overflowing to our local variable and expand our stack when the stack is cleared the next time.
		m_stackOverFlow += overFlow;
		return true;
	}
	else
	{
		return false;
	}

}



