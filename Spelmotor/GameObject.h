#pragma once
#include <d3d11.h>
#include <directxmath.h>
#include "GraphicsManager.h"
#include "AssetManager.h"

class Model;
class Physics;

class GameObject
{
public:
	GameObject(GraphicsManager* gm, AssetManager* assetManager);
	GameObject(GraphicsManager* gm, Model* model);
	~GameObject();

	virtual void Update();
	virtual void Draw();
	virtual void SetPosition(XMFLOAT3 newPos);
	virtual void SetRotation(XMFLOAT4 newRotation);
	virtual void SetColor(const XMFLOAT4& color);
	virtual void SetModel(Model* model);
	virtual void SetTexture(Texture* texture);

	XMFLOAT3 GetPosition() { return m_position; }
	Model* GetModel() { return m_model; }
	Texture* GetTexture() { return m_texture; }

	XMFLOAT3 m_position;
	XMFLOAT4 m_rotation;
	XMFLOAT3 m_scale;
protected:
	Model* m_model;
	InstanceData m_instanceData;
	ShaderPair* m_shaderPair;
	Texture* m_texture;

	bool m_draw;
private:
	GraphicsManager* m_graphicsManager;
};

