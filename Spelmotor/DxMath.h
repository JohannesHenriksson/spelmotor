/** \file dxMath.h
* Contains useful DirectXMath related functions
*/

#ifndef DXMath_h__
#define DXMath_h__
#include <cmath>

#include <DirectXMath.h>

namespace DirectX
{
	/**@{*/
	/**
	* Storing helpers. Pads with 0s if needed
	*/
	inline DirectX::XMFLOAT2 XMStoreFloat2(const DirectX::XMVECTOR& source)
	{
		DirectX::XMFLOAT2 returnFloat2;

		DirectX::XMStoreFloat2(&returnFloat2, source);

		return returnFloat2;
	}
	inline DirectX::XMFLOAT2 XMStoreFloat2(const DirectX::XMFLOAT3& float3)
	{
		return DirectX::XMFLOAT2(float3.x, float3.y);
	}
	inline DirectX::XMFLOAT2 XMStoreFloat2(const DirectX::XMFLOAT4& float4)
	{
		return DirectX::XMFLOAT2(float4.x, float4.y);
	}

	inline DirectX::XMFLOAT3 XMStoreFloat3(const DirectX::XMVECTOR& source)
	{
		DirectX::XMFLOAT3 returnFloat3;

		DirectX::XMStoreFloat3(&returnFloat3, source);

		return returnFloat3;
	}
	inline DirectX::XMFLOAT3 XMStoreFloat3(const DirectX::XMFLOAT2& float2, float z = 0.0f)
	{
		return DirectX::XMFLOAT3(float2.x, float2.y, z);
	}
	inline DirectX::XMFLOAT3 XMStoreFloat3(const DirectX::XMFLOAT4& float4)

	{
		return DirectX::XMFLOAT3(float4.x, float4.y, float4.z);
	}

	inline DirectX::XMFLOAT4 XMStoreFloat4(const DirectX::XMVECTOR& source)
	{
		DirectX::XMFLOAT4 returnFloat4;

		DirectX::XMStoreFloat4(&returnFloat4, source);

		return returnFloat4;
	}
	inline DirectX::XMFLOAT4 XMStoreFloat4(const DirectX::XMFLOAT2& float2, float z = 0.0f, float w = 0.0f)
	{
		return DirectX::XMFLOAT4(float2.x, float2.y, z, w);
	}
	inline DirectX::XMFLOAT4 XMStoreFloat4(const DirectX::XMFLOAT3& float3, float w = 0.0f)
	{
		return DirectX::XMFLOAT4(float3.x, float3.y, float3.z, w);
	}

	inline DirectX::XMFLOAT2 XMStoreFloat2(const DirectX::XMINT2& source)
	{
		DirectX::XMFLOAT2 returnFloat2;

		returnFloat2.x = static_cast<float>(source.x);
		returnFloat2.y = static_cast<float>(source.y);

		return returnFloat2;
	}
	inline DirectX::XMFLOAT3 XMStoreFloat3(const DirectX::XMINT3& source)
	{
		DirectX::XMFLOAT3 returnFloat3;

		returnFloat3.x = static_cast<float>(source.x);
		returnFloat3.y = static_cast<float>(source.y);
		returnFloat3.z = static_cast<float>(source.z);

		return returnFloat3;
	}
	inline DirectX::XMFLOAT4 XMStoreFloat4(const DirectX::XMINT4& source)
	{
		DirectX::XMFLOAT4 returnFloat4;

		returnFloat4.x = static_cast<float>(source.x);
		returnFloat4.y = static_cast<float>(source.y);
		returnFloat4.z = static_cast<float>(source.z);
		returnFloat4.w = static_cast<float>(source.w);

		return returnFloat4;
	}

	inline DirectX::XMINT2 XMStoreSInt2(const DirectX::XMVECTOR& source)
	{
		DirectX::XMINT2 returnInt2;

		DirectX::XMStoreSInt2(&returnInt2, source);

		return returnInt2;
	}
	inline DirectX::XMINT2 XMStoreSInt2(const DirectX::XMINT3& Int3)
	{
		return DirectX::XMINT2(Int3.x, Int3.y);
	}
	inline DirectX::XMINT2 XMStoreSInt2(const DirectX::XMINT4& Int4)
	{
		return DirectX::XMINT2(Int4.x, Int4.y);
	}

	inline DirectX::XMINT3 XMStoreSInt3(const DirectX::XMVECTOR& source)
	{
		DirectX::XMINT3 returnInt3;

		DirectX::XMStoreSInt3(&returnInt3, source);

		return returnInt3;
	}
	inline DirectX::XMINT3 XMStoreSInt3(const DirectX::XMINT2& Int2, int z = 0.0f)
	{
		return DirectX::XMINT3(Int2.x, Int2.y, z);
	}
	inline DirectX::XMINT3 XMStoreSInt3(const DirectX::XMINT4& Int4)

	{
		return DirectX::XMINT3(Int4.x, Int4.y, Int4.z);
	}

	inline DirectX::XMINT4 XMStoreSInt4(const DirectX::XMVECTOR& source)
	{
		DirectX::XMINT4 returnInt4;

		DirectX::XMStoreSInt4(&returnInt4, source);

		return returnInt4;
	}
	inline DirectX::XMINT4 XMStoreSInt4(const DirectX::XMINT2& Int2, int z = 0.0f, int w = 0.0f)
	{
		return DirectX::XMINT4(Int2.x, Int2.y, z, w);
	}
	inline DirectX::XMINT4 XMStoreSInt4(const DirectX::XMINT3& Int3, int w = 0.0f)
	{
		return DirectX::XMINT4(Int3.x, Int3.y, Int3.z, w);
	}

	inline DirectX::XMINT2 XMStoreSInt2(const DirectX::XMFLOAT2& source)
	{
		DirectX::XMINT2 returnInt2;

		returnInt2.x = static_cast<int>(source.x);
		returnInt2.y = static_cast<int>(source.y);

		return returnInt2;
	}
	inline DirectX::XMINT3 XMStoreSInt3(const DirectX::XMFLOAT3& source)
	{
		DirectX::XMINT3 returnInt3;

		returnInt3.x = static_cast<int>(source.x);
		returnInt3.y = static_cast<int>(source.y);
		returnInt3.z = static_cast<int>(source.z);

		return returnInt3;
	}
	inline DirectX::XMINT4 XMStoreSInt4(const DirectX::XMFLOAT4& source)
	{
		DirectX::XMINT4 returnInt4;

		returnInt4.x = static_cast<int>(source.x);
		returnInt4.y = static_cast<int>(source.y);
		returnInt4.z = static_cast<int>(source.z);
		returnInt4.w = static_cast<int>(source.w);

		return returnInt4;
	}

	inline DirectX::XMFLOAT4X4 XMStoreFloat4x4(const DirectX::XMMATRIX& source)
	{
		DirectX::XMFLOAT4X4 returnMatrix;

		DirectX::XMStoreFloat4x4(&returnMatrix, source);

		return returnMatrix;
	}
	/**@}*/

	/**@{*/
	/**
	* Loading helpers. Pads with 0s if needed
	*/

	inline DirectX::XMVECTOR XMLoadFloat4(float x, float y, float z, float w)
	{
		DirectX::XMVECTOR returnVector;
		DirectX::XMFLOAT4 tempFloat(x, y, z, w);

		returnVector = DirectX::XMLoadFloat4(&tempFloat);

		return returnVector;
	}
	inline DirectX::XMVECTOR XMLoadFloat3(float x, float y, float z)
	{
		return XMLoadFloat4(x, y, z, 0.0f);
	}
	inline DirectX::XMVECTOR XMLoadFloat2(float x, float y)
	{
		return XMLoadFloat4(x, y, 0.0f, 0.0f);
	}
	/**@}*/

	//Are pitch and yaw mixed up?
	/**
	* Extracts pitch from \p quaternion
	*
	* \param quaternion
	*
	* \returns extracted pitch
	*/
	inline float XMQuaternionGetPitch(const DirectX::XMFLOAT4& quaternion)
	{
		return std::atan2f(2 * quaternion.y*quaternion.w - 2 * quaternion.x*quaternion.z, 1 - 2 * quaternion.y*quaternion.y - 2 * quaternion.z*quaternion.z);
	}
	/**
	* Extracts yaw from \p quaternion
	*
	* \param quaternion
	*
	* \returns extracted yaw
	*/
	inline float XMQuaternionGetYaw(const DirectX::XMFLOAT4& quaternion)
	{
		return std::atan2f(2 * quaternion.x*quaternion.w - 2 * quaternion.y*quaternion.z, 1 - 2 * quaternion.x*quaternion.x - 2 * quaternion.z*quaternion.z);
	}
}

//Argument dependent lookup apparently not a thing in VS
//////////////////////////////////////////////////
//XMVECTOR
//////////////////////////////////////////////////
inline DirectX::XMVECTOR operator+(DirectX::XMVECTOR lhs, DirectX::XMVECTOR rhs)
{
	return DirectX::operator+(lhs, rhs);
}
inline DirectX::XMVECTOR operator+(DirectX::XMVECTOR lhs, float rhs)
{
	return DirectX::operator+(lhs, DirectX::XMVectorReplicate(rhs));
}

inline DirectX::XMVECTOR operator-(DirectX::XMVECTOR lhs, DirectX::XMVECTOR rhs)
{
	return DirectX::operator-(lhs, rhs);
}
inline DirectX::XMVECTOR operator-(DirectX::XMVECTOR lhs, float rhs)
{
	return DirectX::operator-(lhs, DirectX::XMVectorReplicate(rhs));
}

inline DirectX::XMVECTOR operator*(DirectX::XMVECTOR lhs, DirectX::XMVECTOR rhs)
{
	return DirectX::operator*(lhs, rhs);
}
inline DirectX::XMVECTOR operator*(DirectX::XMVECTOR lhs, float rhs)
{
	return DirectX::operator*(lhs, rhs);
}
inline DirectX::XMVECTOR operator*(float lhs, DirectX::XMVECTOR rhs)
{
	return DirectX::operator*(lhs, rhs);
}

inline DirectX::XMVECTOR operator/(DirectX::XMVECTOR lhs, DirectX::XMVECTOR rhs)
{
	return DirectX::operator/(lhs, rhs);
}
inline DirectX::XMVECTOR operator/(DirectX::XMVECTOR lhs, float rhs)
{
	return DirectX::operator/(lhs, rhs);
}
inline DirectX::XMVECTOR operator/(float lhs, DirectX::XMVECTOR rhs)
{
	return DirectX::operator/(DirectX::XMVectorReplicate(lhs), rhs);
}

inline DirectX::XMVECTOR& operator+=(DirectX::XMVECTOR& lhs, const DirectX::XMVECTOR& rhs)
{
	return DirectX::operator+=(lhs, rhs);
}
inline DirectX::XMVECTOR& operator+=(DirectX::XMVECTOR& lhs, float rhs)
{
	return DirectX::operator+=(lhs, DirectX::XMVectorReplicate(rhs));
}

inline DirectX::XMVECTOR& operator-=(DirectX::XMVECTOR& lhs, const DirectX::XMVECTOR& rhs)
{
	return DirectX::operator-=(lhs, rhs);
}
inline DirectX::XMVECTOR& operator-=(DirectX::XMVECTOR& lhs, float rhs)
{
	return DirectX::operator-=(lhs, DirectX::XMVectorReplicate(rhs));
}

inline DirectX::XMVECTOR& operator*=(DirectX::XMVECTOR& lhs, const DirectX::XMVECTOR& rhs)
{
	return DirectX::operator*=(lhs, rhs);
}
inline DirectX::XMVECTOR& operator*=(DirectX::XMVECTOR& lhs, float rhs)
{
	return DirectX::operator*=(lhs, DirectX::XMVectorReplicate(rhs));
}

inline DirectX::XMVECTOR& operator/=(DirectX::XMVECTOR& lhs, const DirectX::XMVECTOR& rhs)
{
	return DirectX::operator/=(lhs, rhs);
}
inline DirectX::XMVECTOR& operator/=(DirectX::XMVECTOR& lhs, float rhs)
{
	return DirectX::operator/=(lhs, DirectX::XMVectorReplicate(rhs));
}

inline DirectX::XMVECTOR operator+(DirectX::XMVECTOR vector)
{
	return vector;
}
inline DirectX::XMVECTOR operator-(DirectX::XMVECTOR vector)
{
	return DirectX::XMVectorNegate(vector);
}

//////////////////////////////////////////////////
//XMFLOAT
//////////////////////////////////////////////////
inline DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat2(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat3(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat4(DirectX::operator+(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator+=(DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator+=(DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator+=(DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMFLOAT2& operator+=(DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator+=(DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator+=(DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator-(const DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);


	return DirectX::XMStoreFloat2(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat3(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator-(const DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat4(DirectX::operator-(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator-=(DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator-=(DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator-=(DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT2& operator-=(DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator-=(DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator-=(DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat2(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat3(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat4(DirectX::operator*(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator*=(DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator*=(DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator*=(DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT2& operator*=(DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator*=(DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator*=(DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator/(const DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator/(const DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat2(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator/(const DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat3(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator/(const DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	return DirectX::XMStoreFloat4(DirectX::operator/(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator/=(DirectX::XMFLOAT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator/=(DirectX::XMFLOAT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator/=(DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT2& operator/=(DirectX::XMFLOAT2& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator/=(DirectX::XMFLOAT3& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator/=(DirectX::XMFLOAT4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMVectorReplicate(rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}

//////////////////////////////////////////////////
//XMINT op XMFLOAT
//////////////////////////////////////////////////
inline DirectX::XMINT2 operator+(const DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreSInt2(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMINT3 operator+(const DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreSInt3(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMINT4 operator+(const DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreSInt4(DirectX::operator+(xmLhs, xmRhs));
}

inline DirectX::XMINT2& operator+=(DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreSInt2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT3& operator+=(DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreSInt3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT4& operator+=(DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreSInt4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMINT2 operator-(const DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreSInt2(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMINT3 operator-(const DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreSInt3(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMINT4 operator-(const DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreSInt4(DirectX::operator-(xmLhs, xmRhs));
}

inline DirectX::XMINT2& operator-=(DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreSInt2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT3& operator-=(DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreSInt3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT4& operator-=(DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreSInt4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMINT2 operator*(const DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreSInt2(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMINT3 operator*(const DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreSInt3(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMINT4 operator*(const DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreSInt4(DirectX::operator*(xmLhs, xmRhs));
}

inline DirectX::XMINT2& operator*=(DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreSInt2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT3& operator*=(DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreSInt3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT4& operator*=(DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreSInt4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMINT2 operator/(const DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	return DirectX::XMStoreSInt2(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMINT3 operator/(const DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	return DirectX::XMStoreSInt3(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMINT4 operator/(const DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreSInt4(DirectX::operator/(xmLhs, xmRhs));
}

inline DirectX::XMINT2& operator/=(DirectX::XMINT2& lhs, const DirectX::XMFLOAT2& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt2(&lhs);
	auto xmRhs = DirectX::XMLoadFloat2(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreSInt2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT3& operator/=(DirectX::XMINT3& lhs, const DirectX::XMFLOAT3& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt3(&lhs);
	auto xmRhs = DirectX::XMLoadFloat3(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreSInt3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMINT4& operator/=(DirectX::XMINT4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadSInt4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreSInt4(&lhs, xmLhs);

	return lhs;
}

//////////////////////////////////////////////////
//XMFLOAT op XMINT
//////////////////////////////////////////////////
inline DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2& lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator+(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator+(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator+=(DirectX::XMFLOAT2&lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator+=(DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator+=(DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	DirectX::operator+=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2& lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator-(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator-(const DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator-(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator-=(DirectX::XMFLOAT2&lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator-=(DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator-=(DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	DirectX::operator-=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2& lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator*(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator*(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator*=(DirectX::XMFLOAT2&lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator*=(DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator*=(DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	DirectX::operator*=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}

inline DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2& lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	return DirectX::XMStoreFloat2(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT3 operator/(const DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	return DirectX::XMStoreFloat3(DirectX::operator/(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4 operator/(const DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::operator/(xmLhs, xmRhs));
}

inline DirectX::XMFLOAT2& operator/=(DirectX::XMFLOAT2&lhs, const DirectX::XMINT2& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat2(&lhs);
	auto xmRhs = DirectX::XMLoadSInt2(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat2(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT3& operator/=(DirectX::XMFLOAT3& lhs, const DirectX::XMINT3& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat3(&lhs);
	auto xmRhs = DirectX::XMLoadSInt3(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat3(&lhs, xmLhs);

	return lhs;
}
inline DirectX::XMFLOAT4& operator/=(DirectX::XMFLOAT4& lhs, const DirectX::XMINT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadSInt4(&rhs);

	DirectX::operator/=(xmLhs, xmRhs);

	DirectX::XMStoreFloat4(&lhs, xmLhs);

	return lhs;
}


//////////////////////////////////////////////////
//XMFLOAT4X4
//////////////////////////////////////////////////

//OBS: No lambda whatsoever
inline bool operator==(const DirectX::XMFLOAT4X4& lhs, const DirectX::XMFLOAT4X4& rhs)
{
	return(lhs._11 == rhs._11 &&
		   lhs._12 == rhs._12 &&
		   lhs._13 == rhs._13 &&
		   lhs._14 == rhs._14 &&
		   lhs._21 == rhs._21 &&
		   lhs._22 == rhs._22 &&
		   lhs._23 == rhs._23 &&
		   lhs._24 == rhs._24 &&
		   lhs._31 == rhs._31 &&
		   lhs._32 == rhs._32 &&
		   lhs._33 == rhs._33 &&
		   lhs._34 == rhs._34 &&
		   lhs._41 == rhs._41 &&
		   lhs._42 == rhs._42 &&
		   lhs._43 == rhs._43 &&
		   lhs._44 == rhs._44);
}
inline bool operator!=(const DirectX::XMFLOAT4X4& lhs, const DirectX::XMFLOAT4X4& rhs)
{
	return !(lhs == rhs);
}

inline DirectX::XMFLOAT4X4 operator*(const DirectX::XMFLOAT4X4& lhs, const DirectX::XMFLOAT4X4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4x4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4x4(&rhs);

	return DirectX::XMStoreFloat4x4(DirectX::XMMatrixMultiply(xmLhs, xmRhs));
}
inline DirectX::XMFLOAT4X4& operator*=(DirectX::XMFLOAT4X4& lhs, const DirectX::XMFLOAT4X4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4x4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4x4(&rhs);

	DirectX::XMStoreFloat4x4(&lhs, DirectX::XMMatrixMultiply(xmLhs, xmRhs));

	return lhs;
}

inline DirectX::XMFLOAT4X4 operator*(const DirectX::XMFLOAT4X4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4x4(&lhs);

	xmLhs *= rhs;

	return DirectX::XMStoreFloat4x4(xmLhs);
}
inline DirectX::XMFLOAT4X4& operator*=(DirectX::XMFLOAT4X4& lhs, float rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4x4(&lhs);

	DirectX::XMStoreFloat4x4(&lhs, xmLhs * rhs);

	return lhs;
}

inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4X4& lhs, const DirectX::XMFLOAT4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4x4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::XMVector4Transform(xmRhs, xmLhs));
}
inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4& lhs, const DirectX::XMFLOAT4X4& rhs)
{
	auto xmLhs = DirectX::XMLoadFloat4(&lhs);
	auto xmRhs = DirectX::XMLoadFloat4x4(&rhs);

	return DirectX::XMStoreFloat4(DirectX::XMVector4Transform(xmLhs, xmRhs));
}
#endif // DXMath_h__