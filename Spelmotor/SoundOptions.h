#pragma once

enum AudioOpt{
	LOOP_SOUND = 0x1,
	PLAY_ONCE = 0x2,
	STYLE3 = 0x4,
	STYLE4 = 0x8,
	STYLE5 = 0x10,
	STYLE6 = 0x20,
	STYLE7 = 0x40,
	STYLE8 = 0x80
};