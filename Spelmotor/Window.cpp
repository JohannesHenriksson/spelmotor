#include "Window.h"

#include <dxgidebug.h>


#include <vector>

#include "Input.h"

bool Window::windowIsFocused;
UINT Window::m_windowWidth = 1280;
UINT Window::m_windowHeight = 720;

Window::Window()
	: m_device(nullptr)
	, m_deviceContext(nullptr)
	, m_swapChain(nullptr)
	, m_backbufferRTV(nullptr)
	, m_backbufferUAV(nullptr)
	, m_depthStencilBuffer(nullptr)
	, m_depthStencilView(nullptr)
{
	InitWindow();
	InitDevice();
}


Window::~Window()
{
	m_deviceContext->ClearState();

	SafeRelease(m_backbufferRTV);
	SafeRelease(m_backbufferUAV);

	SafeRelease(m_depthStencilView);
	SafeRelease(m_depthStencilBuffer);


#ifdef _DEBUG || DEBUG

	ID3D11Debug* debugDev;
	m_device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&debugDev));
	debugDev->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	debugDev->Release();
#endif // DEBUG


	SafeRelease(m_swapChain);
	SafeRelease(m_deviceContext);
	SafeRelease(m_device);
}

ID3D11Device* Window::GetDevice() const
{
	return m_device;
}

ID3D11DeviceContext* Window::GetDeviceContext() const
{
	return m_deviceContext;
}

void Window::InitWindow()
{
	m_hInstance = GetModuleHandle(NULL);

	WNDCLASSEX wndcex;
	wndcex.cbSize = sizeof(WNDCLASSEX);
	wndcex.style = CS_HREDRAW | CS_VREDRAW;
	wndcex.lpfnWndProc = WndProc;
	wndcex.cbClsExtra = 0;
	wndcex.cbWndExtra = 0;
	wndcex.hInstance = m_hInstance;
	wndcex.hIcon = NULL;
	wndcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndcex.lpszMenuName = NULL;
	wndcex.lpszClassName = L"WindowClass";
	wndcex.hIconSm = 0;

	if(!RegisterClassEx(&wndcex))
	{
		//auto err = GetLastError();
		//auto errC = HRESULT_FROM_WIN32(err);
		OutputDebugStringA("Failed to register window class");
		return;
	}

	RECT windowRect = { 0, 0, m_windowWidth, m_windowHeight };
	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, false);

	m_hWnd = CreateWindow(
		L"WindowClass",
		L"Spelmotor",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		m_hInstance,
		NULL);

	if(!m_hWnd)
	{
		OutputDebugStringA("CreateWindow failed\nWindow::Window(std::string name)");
		return;
	}
	ShowWindow(m_hWnd, 1);
}

void Window::InitDevice()
{

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChainDesc.BufferDesc.Width = m_windowWidth;
	swapChainDesc.BufferDesc.Height = m_windowHeight;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;

	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_UNORDERED_ACCESS;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = m_hWnd;
	swapChainDesc.Windowed = true;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0;

	UINT flags = 0;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif 

	std::vector<D3D_FEATURE_LEVEL> featureLevels;
	featureLevels.push_back(D3D_FEATURE_LEVEL_11_1);
	featureLevels.push_back(D3D_FEATURE_LEVEL_11_0);

	auto hr = D3D11CreateDeviceAndSwapChain(nullptr
											, D3D_DRIVER_TYPE_HARDWARE
											, nullptr
											, flags
											, &featureLevels[0]
											, featureLevels.size()
											, D3D11_SDK_VERSION
											, &swapChainDesc
											, &m_swapChain
											, &m_device
											, nullptr //ChosenFeatureLevel
											, &m_deviceContext);


	if(FAILED(hr))
	{
		OutputDebugStringA("Failed to create swap chain");
		return;
	}

	ID3D11Texture2D* backBufferPointer;

	hr = m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPointer);
	if(FAILED(hr))
	{
		OutputDebugStringA("Failed to get backbuffer from swapchain");
		return;
	}

	hr = m_device->CreateRenderTargetView(backBufferPointer, nullptr, &m_backbufferRTV);
	if(FAILED(hr))
	{
		OutputDebugStringA("Failed to create backbuffer rtv");
		return;
	}
	SetDebugObjectName(m_backbufferRTV, "BackBufferRTV");

	hr = m_device->CreateUnorderedAccessView(backBufferPointer, nullptr, &m_backbufferUAV);
	if(FAILED(hr))
	{
		OutputDebugStringA("Failed to create backbuffer uav");
		return;
	}
	SetDebugObjectName(m_backbufferUAV, "BackBufferUAV");

	backBufferPointer->Release();

	D3D11_TEXTURE2D_DESC depthStencilTexDesc;
	depthStencilTexDesc.ArraySize = 1;
	depthStencilTexDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	depthStencilTexDesc.CPUAccessFlags = 0;
	depthStencilTexDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	depthStencilTexDesc.MipLevels = 1;
	depthStencilTexDesc.MiscFlags = 0;
	depthStencilTexDesc.SampleDesc.Count = 1;
	depthStencilTexDesc.SampleDesc.Quality = 0;
	depthStencilTexDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilTexDesc.Width = m_windowWidth;
	depthStencilTexDesc.Height = m_windowHeight;
	
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Flags = 0;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	hr = m_device->CreateTexture2D(&depthStencilTexDesc, nullptr, &m_depthStencilBuffer);
	if(FAILED(hr))
	{
		OutputDebugStringA("Failed to create depth stencil buffer");
		return;
	}
	SetDebugObjectName(m_depthStencilBuffer, "DepthStencilBuffer");

	hr = m_device->CreateDepthStencilView(m_depthStencilBuffer, &depthStencilViewDesc, &m_depthStencilView);
	if(FAILED(hr))
	{
		OutputDebugStringA("CreateDepthStencilView failed in Graphics::Initialize");
		return;
	}
	SetDebugObjectName(m_depthStencilView, "DepthStencilView");

	D3D11_VIEWPORT viewPort;
	// Setup the viewport for rendering.
	viewPort.Width = static_cast<float>(m_windowWidth);
	viewPort.Height = static_cast<float>(m_windowHeight);
	viewPort.MinDepth = 0.0f;
	viewPort.MaxDepth = 1.0f;
	viewPort.TopLeftX = 0.0f;
	viewPort.TopLeftY = 0.0f;

	// Create the viewport.
	m_deviceContext->RSSetViewports(1, &viewPort);

}



void Window::SetAndClearRenderTargets()
{
	float color[4];


	// Setup the color to clear the buffer to.
	color[0] = 101.f/255.f;
	color[1] = 156.f/255.f;
	color[2] = 239.f/255.f;
	color[3] = 0;

	m_deviceContext->OMSetRenderTargets(1, &m_backbufferRTV, m_depthStencilView);
	// Clear the back buffer.
	m_deviceContext->ClearRenderTargetView(m_backbufferRTV, color);

	// Clear the depth buffer.
	m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	return;
}

void Window::Present()
{
	// Present as fast as possible.
	m_swapChain->Present(0, 0);
	return;
}

bool Window::PeekMessages()
{
	MSG msg = { 0 };

	while(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);

		if(msg.message == WM_QUIT)
			return false;

		DispatchMessage(&msg);
	}
	return true;
}

bool Window::GetWindowIsFocused()
{
	return windowIsFocused;
}

UINT Window::GetWindowWidth()
{
	return m_windowWidth;
}

UINT Window::GetWindowHeight()
{
	return m_windowHeight;
}

LRESULT Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_SETFOCUS:
		windowIsFocused = true;
		Input::LockCursor(m_windowWidth / 2, m_windowHeight / 2);
		Input::HideCursor();
		break;
	case WM_KILLFOCUS:
		windowIsFocused = false;
		Input::LockCursor(-1, -1);
		Input::ShowCursor();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
		break;
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
		break;
	case WM_KEYDOWN:
		if(wParam == VK_ESCAPE)
			PostQuitMessage(0);
		break;
	case WM_KEYUP:
		break;
	case WM_MOUSEWHEEL:
		break;

	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
		break;
	}
}