#include "AssetUnloader.h"

//// Needed for print unloaded
//#include "GameEngine.h"
//#include "AssetManager.h"

AssetUnloader::AssetUnloader(std::map<uint64_t, Asset*>* assetMapPtr, ReplacementPolicy policy)
	: m_policy(policy)
	, assetMap(assetMapPtr)
{
}


AssetUnloader::~AssetUnloader()
{
}

void AssetUnloader::SetPolicy(ReplacementPolicy policy)
{
	m_policy = policy;
}

bool AssetUnloader::FreeMemory(int size, MemoryTracker* memTracker)
{
	switch (m_policy)
	{
		case ReplacementPolicy::LRU:
			return FreeMem_LRU(size, memTracker);
			break;
		case ReplacementPolicy::MRU:
			return FreeMem_MRU(size, memTracker);
			break;
	}
}

bool AssetUnloader::FreeMem_LRU(int size, MemoryTracker* memTracker)
{
	int removedSize = 0;
	while (removedSize < size && unusedAssets.size() > 0)
	{
		Asset* asset = unusedAssets.front();
		if (asset->m_nrOfReferences > 0)
		{
			unusedAssets.pop_front();
		}
		else
		{
			//GameEngine::GetInstance()->GetAssetManager()->PrintUnloaded(asset->m_GUID);
			unusedAssets.pop_front();
			removedSize += asset->GetSize();
			asset->Unload(memTracker);
			assetMap->erase(asset->m_GUID);
		}
	}

	if (removedSize < size)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool AssetUnloader::FreeMem_MRU(int size, MemoryTracker* memTracker)
{
	int removedSize = 0;
	while (removedSize < size && unusedAssets.size() > 0)
	{
		Asset* asset = unusedAssets.back();
		if (asset->m_nrOfReferences > 0)
		{
			unusedAssets.pop_back();
		}
		else
		{
			//GameEngine::GetInstance()->GetAssetManager()->PrintUnloaded(asset->m_GUID);
			removedSize += asset->GetSize();
			asset->Unload(memTracker);
			assetMap->erase(asset->m_GUID);
		}
	}
	if (removedSize < size)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void AssetUnloader::ReportUnusedAsset(Asset* asset)
{
	unusedAssets.emplace_front(asset);
}
 