#include "GraphicsManager.h"
#include <iostream>
#include "AssetManager.h"
#include "Window.h"
#include <algorithm>


GraphicsManager::GraphicsManager(Window* window, AssetManager* assetManager)
	: window(window)
	, device(window->GetDevice())
	, deviceContext(window->GetDeviceContext())
	, assetManager(assetManager)
{
	m_DefaultShaderPair = new ShaderPair();
	//Load Default Shaders
	m_DefaultShaderPair->vertexShader = assetManager->Load<VertexShader>(0x937C0DEF6C21C023);
	m_DefaultShaderPair->pixelShader = assetManager->Load<PixelShader>(0x937C0DEF6C246023);

	m_DefaultShaderPairNoTex = new ShaderPair();
	m_DefaultShaderPairNoTex->vertexShader = assetManager->Load<VertexShader>(0x937C0DEF6C21C023);
	m_DefaultShaderPairNoTex->pixelShader = assetManager->Load<PixelShader>(0x53332D9261B7E358);

	//Create input layout for default vertex shader
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputDescs;
	inputDescs.push_back({ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 });
	inputDescs.push_back({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 });
	inputDescs.push_back({ "TEX_COORDS", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 });
	inputDescs.push_back({ "WORLD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 });
	inputDescs.push_back({ "WORLD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 });
	inputDescs.push_back({ "WORLD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1 });
	inputDescs.push_back({ "WORLD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1 });
	inputDescs.push_back({ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 64, D3D11_INPUT_PER_INSTANCE_DATA, 1 });
	if(m_DefaultShaderPair->vertexShader)
		m_DefaultShaderPair->vertexShader->CreateInputLayout(device, inputDescs);

	//Create view projection constant buffer
	D3D11_BUFFER_DESC viewProjBufferDesc;
	viewProjBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	viewProjBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	viewProjBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	viewProjBufferDesc.MiscFlags = 0;
	viewProjBufferDesc.StructureByteStride = 0;
	viewProjBufferDesc.ByteWidth = sizeof(DirectX::XMFLOAT4X4);

	ID3D11Buffer* tempBuffer;
	HRESULT hr = device->CreateBuffer(&viewProjBufferDesc, nullptr, &tempBuffer);
	if (FAILED(hr))
		std::cout << "Failed to create index buffer" << std::endl;
	else
		m_viewProjectionConstantBuffer.reset(tempBuffer);

	//Create instance buffer for cubes
	D3D11_BUFFER_DESC instanceDataBufferDesc;
	instanceDataBufferDesc.ByteWidth = sizeof(InstanceData) * MAX_INSTANCES;
	instanceDataBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instanceDataBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	instanceDataBufferDesc.StructureByteStride = sizeof(InstanceData);
	instanceDataBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	instanceDataBufferDesc.MiscFlags = 0;

	hr = device->CreateBuffer(&instanceDataBufferDesc, nullptr, &m_instanceBuffer);
	if (FAILED(hr))
		std::cout << "Failed to create instance buffer" << std::endl;
	else
		SetDebugObjectName(m_instanceBuffer, "GraphManInstanceBuffer");


	D3D11_BUFFER_DESC lightPositionBufferDesc;
	lightPositionBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightPositionBufferDesc.ByteWidth = sizeof(LightPos) * 2;
	lightPositionBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightPositionBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightPositionBufferDesc.MiscFlags = 0;
	lightPositionBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	hr = device->CreateBuffer(&lightPositionBufferDesc, NULL, &m_lightBuffer);
	if (FAILED(hr))
		std::cout << "Failed to create light buffer" << std::endl;
	else
		SetDebugObjectName(m_instanceBuffer, "GraphManLightBuffer");

	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.MaxAnisotropy = 0;
	samplerDesc.MipLODBias = 0;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	ID3D11SamplerState* tempSamplerState;
	hr = device->CreateSamplerState(&samplerDesc, &tempSamplerState); 
	
	if (FAILED(hr))
		std::cout << "Failed to create samplerState" << std::endl;
	else
	{
		SetDebugObjectName(tempSamplerState, "GraphManSamplerState");
		m_samplerState.reset(tempSamplerState);
	}

	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	ID3D11RasterizerState* tempRasterState;
	hr = device->CreateRasterizerState(&rasterDesc, &tempRasterState);
	if (FAILED(hr))
		std::cout << "Failed to create rasterizer state" << std::endl;
	else
	{
		SetDebugObjectName(tempRasterState, "GraphManRasterState");
		m_rasterizerState.reset(tempRasterState);
	}

}

GraphicsManager::~GraphicsManager()
{
	device = nullptr;
	deviceContext = nullptr;
	window = nullptr;
	assetManager = nullptr;
	SafeRelease(m_instanceBuffer);
	if(m_lightBuffer)
		m_lightBuffer->Release();
}
void GraphicsManager::InsertDrawData(ShaderPair* shaderPair, Model* model, Texture* texture, InstanceData instanceData)
{
	if (shaderPair == nullptr)
		shaderPair = m_DefaultShaderPair;
	if (model == nullptr || !model->IsLoaded())
	{
		model = Model::GetDefaultModel();
		texture = Texture::GetDefaultTexture();
		shaderPair = m_DefaultShaderPair;
	}
	if (!texture)
		shaderPair = m_DefaultShaderPairNoTex;
	else if(!texture->IsLoaded())
		texture = Texture::GetDefaultTexture();


	drawMap[shaderPair][model][texture].push_back(instanceData);
}

void GraphicsManager::DrawCube(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT4 color, Texture* tex /*= nullptr*/)
{
	InstanceData iData;
	iData.color = color;
	DirectX::XMStoreFloat4x4(&iData.worldMatrix, DirectX::XMMatrixMultiply(DirectX::XMMatrixScalingFromVector(DirectX::XMLoadFloat3(&scale)), DirectX::XMMatrixTranslationFromVector(DirectX::XMLoadFloat3(&position))));
	if(tex)
		drawMap[m_DefaultShaderPair][Model::GetDefaultModel()][tex].push_back(iData);
	else
		drawMap[m_DefaultShaderPairNoTex][Model::GetDefaultModel()][nullptr].push_back(iData);
}

ShaderPair* GraphicsManager::GetDefaultShaderPair() const
{
	return m_DefaultShaderPair;
}

void GraphicsManager::DrawAllObjects()
{
	window->SetAndClearRenderTargets();
	deviceContext->RSSetState(m_rasterizerState.get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	auto tempBuffer = m_viewProjectionConstantBuffer.get();
	deviceContext->VSSetConstantBuffers(0, 1, &tempBuffer);
	deviceContext->VSSetConstantBuffers(1, 1, &m_lightBuffer);
	ID3D11SamplerState* tempSamplerState = m_samplerState.get();
	deviceContext->PSSetSamplers(0, 1, &tempSamplerState);



	//Iterate over shader pairs
	for (auto& shaderPair : drawMap)
	{
		if (!shaderPair.first->vertexShader || !shaderPair.first->pixelShader)
			continue;
		deviceContext->VSSetShader(shaderPair.first->vertexShader->GetShader(), nullptr, 0);
		deviceContext->IASetInputLayout(shaderPair.first->vertexShader->GetInputLayout());
		deviceContext->PSSetShader(shaderPair.first->pixelShader->GetShader(), nullptr, 0);

		for (auto& model : shaderPair.second)
		{
			auto tempBuf = model.first->GetVertexBuffer();
			auto tempIndexBuf = model.first->GetIndexBuffer();
			UINT stride = model.first->GetStride();
			UINT offset = model.first->GetOffset();
			deviceContext->IASetVertexBuffers(0, 1, &tempBuf, &stride, &offset);
			deviceContext->IASetIndexBuffer(tempIndexBuf, DXGI_FORMAT_R32_UINT, 0);
		

			for (auto& texture : model.second)
			{
				if (texture.first != nullptr)
				{
					ID3D11ShaderResourceView* tempTex = texture.first->GetTextureShaderResourceView();
					deviceContext->PSSetShaderResources(0, 1, &tempTex);
				}
				int nrOfInstances = texture.second.size();
				if (nrOfInstances > MAX_INSTANCES)
				{
					nrOfInstances = MAX_INSTANCES;
					std::cout << "Warning: instance buffer not big enough for all instances" << std::endl;
				}
				if (nrOfInstances)
				{
					//Update Instance Data
					D3D11_MAPPED_SUBRESOURCE resource;
					HRESULT hr = deviceContext->Map(m_instanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
					memcpy(resource.pData, &texture.second[0], sizeof(InstanceData) *nrOfInstances);
					deviceContext->Unmap(m_instanceBuffer, 0);
					UINT stride = sizeof(InstanceData);
					offset = 0;
					deviceContext->IASetVertexBuffers(1, 1, &m_instanceBuffer, &stride, &offset);
					//Draw
					deviceContext->DrawIndexedInstanced(model.first->GetIndexCount(), nrOfInstances, 0, 0, 0);
				}
			}
		}
	}

	//Unbind shaders
	deviceContext->VSSetShader(nullptr, nullptr, 0);
	deviceContext->PSSetShader(nullptr, nullptr, 0);
	ID3D11Buffer* buffers[2] = { nullptr, nullptr };
	UINT offset = 0;
	deviceContext->IASetVertexBuffers(0, 2, buffers, &offset, &offset);
	deviceContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_R32_UINT, 0);
	ID3D11ShaderResourceView* srv = nullptr;
	deviceContext->PSSetShaderResources(0, 1, &srv);

	window->Present();

	drawMap.clear();
}

void GraphicsManager::UpdateConstantBuffer(DirectX::XMFLOAT4X4 viewProjectionMatrix, LightPos lightPos)
{
	D3D11_MAPPED_SUBRESOURCE resource;
	HRESULT hr = deviceContext->Map(m_viewProjectionConstantBuffer.get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
	memcpy(resource.pData, &viewProjectionMatrix, sizeof(DirectX::XMFLOAT4X4));
	deviceContext->Unmap(m_viewProjectionConstantBuffer.get(), 0);

	hr = deviceContext->Map(m_lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
	memcpy(resource.pData, &lightPos, sizeof(LightPos) * 2);
	deviceContext->Unmap(m_lightBuffer, 0);
}
