#pragma once

#include "PxPhysicsAPI.h"

class PhysicsManager
{
private:
	physx::PxDefaultErrorCallback gDefaultErrorCallback;
	physx::PxDefaultAllocator gDefaultAllocatorCallback;
	physx::PxFoundation* m_foundation;
	physx::PxPhysics* m_physics;
	physx::PxVisualDebuggerConnection* m_connection;
	physx::PxDefaultCpuDispatcher*	m_dispatcher;
	physx::PxScene*	m_scene;
	physx::PxCooking* m_cooking;
	static PhysicsManager* instance;
	physx::PxRigidDynamic* dynamic;
	PhysicsManager();
public:
	static PhysicsManager* GetInstance();
	physx::PxMaterial* CreateMaterial(float staticFriction, float dynamicFriction, float restitution);
	physx::PxRigidDynamic* CreateDynamic(physx::PxVec3 position, physx::PxSphereGeometry geometry, physx::PxMaterial* material);
	physx::PxRigidDynamic* CreateDynamic(physx::PxVec3 position, physx::PxGeometry geometry, physx::PxMaterial* material);
	physx::PxRigidDynamic* CreateDynamic(physx::PxVec3 position, physx::PxBoxGeometry geometry);
	physx::PxRigidStatic* CreateStatic(physx::PxVec3 position, physx::PxBoxGeometry geometry, float staticFriction, float dynamicFriction, float restitution);
	void RemoveObject(physx::PxRigidDynamic* object);
	~PhysicsManager();
	void Update(float deltaTime);
	bool Init();
};

