#pragma once
#include "GraphicsManager.h"


class MemoryManager;
class SoundManager;
class AssetManager;
class PhysicsManager;
class Camera;
class Window;
class Input;

struct LightPos;

class GameEngine
{
public:
	~GameEngine();
	static GameEngine* GetInstance();
	void Draw();
	void Update(float dt);
	void UpdateLightPosition(DirectX::XMFLOAT4 pos1, DirectX::XMFLOAT4 pos2);

	GraphicsManager* GetGraphicsManager() const;
	AssetManager* GetAssetManager() const;
	MemoryManager* GetMemoryManager() const;
	Window* GetWindow() const;
	Camera* GetCamera() const;

	bool IsRunning() const;

	void SetCamera(Camera* camera);
private:

	Window* m_window;
	MemoryManager* m_memoryManager;
	AssetManager* m_assetManager;
	GraphicsManager* m_graphicsManager;
	Input* m_input;
	PhysicsManager* m_physicsManager;

	Camera* m_camera;
	Camera* m_defaultCamera;
	bool m_isRunning;

	void Init();
	GameEngine();
	static GameEngine* m_instance;

	LightPos m_light;
};

