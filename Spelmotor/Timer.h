#ifndef TIMER_HEADER
#define TIMER_HEADER

#include <chrono>

class Timer
{
public:
	Timer();
	~Timer();

	void Start();
	void Stop();

	void Reset();

	float GetTimeSeconds();
	float GetTimeMilliseconds();
	float GetTimeMicroseconds();

	

	long long GetDeltaTimeNanoseconds();
private:
	std::chrono::high_resolution_clock::time_point m_startTime;
	std::chrono::high_resolution_clock::time_point m_stopTime;
	std::chrono::high_resolution_clock::time_point m_lastDeltaTime;
	bool running;
};

#endif