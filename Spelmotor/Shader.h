#ifndef SHADER_H
#define SHADER_H

#include "Asset.h"
#include <string>
#include <memory>

class Shader :
	public Asset
{
public:
	Shader();
	~Shader();

	virtual size_t GetSize();

protected:
	//Microsoft::WRL::ComPtr<ID3DBlob> m_shaderSource;
	std::unique_ptr<ID3DBlob, ComDeleter> m_shaderSource;

	void LoadShaderSource(void* data, int dataSize);
};

#endif