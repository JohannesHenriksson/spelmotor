
Texture2D<float4> tex : register(t0);
SamplerState samplerState : register(s0);

struct PSInput
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 TexCoords : TEX_COORDS;
	float4 Color : COLOR;
	float3 lightPos1 : LIGHTPOS1;
	float3 lightPos2 : LIGHTPOS2;
};

float4 main(PSInput input) : SV_TARGET
{
	float4 textureColor;
	float lightIntensity1, lightIntensity2;
	float4 color, color1, color2;
	float4 diffuseColor[2];
	float3 lightDir1, lightDir2;
	float length1, length2;

	diffuseColor[0] = float4(1, 0, 1, 1);
	diffuseColor[1] = float4(0, 1, 1, 1);

	textureColor = tex.Sample(samplerState, input.TexCoords);
	
	lightDir1 = normalize(input.lightPos1);
	lightDir2 = normalize(input.lightPos2);

	length1 = length(input.lightPos1) / 5;
	length2 = length(input.lightPos2) / 5;

	lightIntensity1 = dot(input.Normal, lightDir1) / (length1 * 3);
	lightIntensity2 = dot(input.Normal, lightDir2) / (length2 * 3);

	color1 = diffuseColor[0] * lightIntensity1;
	color2 = diffuseColor[1] * lightIntensity2;

	color = (color1 + color2) + textureColor * dot(input.Normal, float3(0, 1, -1) * .5);// +textureColor * 0.2f;
	return color;

	return textureColor * dot(input.Normal, float3(0, 1, -1));
	//return input.Color;
}