#include "CollisionMesh.h"



CollisionMesh::CollisionMesh()
{
}


CollisionMesh::~CollisionMesh()
{
}

ASSET_ERROR_CODES CollisionMesh::Load(ID3D11Device * device, void * data, int dataSize, const std::string & name, MemoryTracker * memTracker)
{
	memTracker->Submit_RAM(AssetType::PHYSICS, GetSize(), dataSize);
	return ASSET_ERROR_CODES();
}
