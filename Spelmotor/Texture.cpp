#include "Texture.h"
#include <vector>
#include <limits>
#include <iostream>
#include <comdef.h>

Texture* Texture::defaultTexture;

Texture::Texture()
	: m_textureWidth(0)
	, m_textureHeight(0)
	, m_bitsPerPixel(0)
{ }


Texture::~Texture()
{ }

ID3D11ShaderResourceView* Texture::GetTextureShaderResourceView() const
{
	return m_TextureSRV.get();
}

ID3D11Texture2D* Texture::GetTexture() const
{
	return m_Texture.get();
}

Texture* Texture::GetDefaultTexture()
{
	return defaultTexture;
}

size_t Texture::GetSize()
{
	if(isLoaded)
		return m_textureHeight * m_textureWidth * m_bitsPerPixel / 8.f;
	return 0;
}

unsigned int Texture::GetBitsPerPixel(DXGI_FORMAT format)
{

	switch (static_cast<int>(format))
	{
	case DXGI_FORMAT_R32G32B32A32_TYPELESS:
	case DXGI_FORMAT_R32G32B32A32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_UINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:
		return 128;

	case DXGI_FORMAT_R32G32B32_TYPELESS:
	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32_SINT:
		return 96;

	case DXGI_FORMAT_R16G16B16A16_TYPELESS:
	case DXGI_FORMAT_R16G16B16A16_FLOAT:
	case DXGI_FORMAT_R16G16B16A16_UNORM:
	case DXGI_FORMAT_R16G16B16A16_UINT:
	case DXGI_FORMAT_R16G16B16A16_SNORM:
	case DXGI_FORMAT_R16G16B16A16_SINT:
	case DXGI_FORMAT_R32G32_TYPELESS:
	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32_SINT:
	case DXGI_FORMAT_R32G8X24_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
	case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
	case DXGI_FORMAT_Y416:
	case DXGI_FORMAT_Y210:
	case DXGI_FORMAT_Y216:
		return 64;

	case DXGI_FORMAT_R10G10B10A2_TYPELESS:
	case DXGI_FORMAT_R10G10B10A2_UNORM:
	case DXGI_FORMAT_R10G10B10A2_UINT:
	case DXGI_FORMAT_R11G11B10_FLOAT:
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
	case DXGI_FORMAT_R8G8B8A8_UINT:
	case DXGI_FORMAT_R8G8B8A8_SNORM:
	case DXGI_FORMAT_R8G8B8A8_SINT:
	case DXGI_FORMAT_R16G16_TYPELESS:
	case DXGI_FORMAT_R16G16_FLOAT:
	case DXGI_FORMAT_R16G16_UNORM:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16_SNORM:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R32_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT:
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R24G8_TYPELESS:
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
	case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
	case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
	case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
	case DXGI_FORMAT_R8G8_B8G8_UNORM:
	case DXGI_FORMAT_G8R8_G8B8_UNORM:
	case DXGI_FORMAT_B8G8R8A8_UNORM:
	case DXGI_FORMAT_B8G8R8X8_UNORM:
	case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
	case DXGI_FORMAT_B8G8R8A8_TYPELESS:
	case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
	case DXGI_FORMAT_B8G8R8X8_TYPELESS:
	case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
	case DXGI_FORMAT_AYUV:
	case DXGI_FORMAT_Y410:
	case DXGI_FORMAT_YUY2:
		return 32;

	case DXGI_FORMAT_P010:
	case DXGI_FORMAT_P016:
		return 24;

	case DXGI_FORMAT_R8G8_TYPELESS:
	case DXGI_FORMAT_R8G8_UNORM:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8_SNORM:
	case DXGI_FORMAT_R8G8_SINT:
	case DXGI_FORMAT_R16_TYPELESS:
	case DXGI_FORMAT_R16_FLOAT:
	case DXGI_FORMAT_D16_UNORM:
	case DXGI_FORMAT_R16_UNORM:
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16_SNORM:
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_B5G6R5_UNORM:
	case DXGI_FORMAT_B5G5R5A1_UNORM:
	case DXGI_FORMAT_A8P8:
	case DXGI_FORMAT_B4G4R4A4_UNORM:
		return 16;

	case DXGI_FORMAT_NV12:
	case DXGI_FORMAT_420_OPAQUE:
	case DXGI_FORMAT_NV11:
		return 12;

	case DXGI_FORMAT_R8_TYPELESS:
	case DXGI_FORMAT_R8_UNORM:
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8_SNORM:
	case DXGI_FORMAT_R8_SINT:
	case DXGI_FORMAT_A8_UNORM:
	case DXGI_FORMAT_AI44:
	case DXGI_FORMAT_IA44:
	case DXGI_FORMAT_P8:
		return 8;

	case DXGI_FORMAT_R1_UNORM:
		return 1;

	case DXGI_FORMAT_BC1_TYPELESS:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
	case DXGI_FORMAT_BC4_TYPELESS:
	case DXGI_FORMAT_BC4_UNORM:
	case DXGI_FORMAT_BC4_SNORM:
		return 4;

	case DXGI_FORMAT_BC2_TYPELESS:
	case DXGI_FORMAT_BC2_UNORM:
	case DXGI_FORMAT_BC2_UNORM_SRGB:
	case DXGI_FORMAT_BC3_TYPELESS:
	case DXGI_FORMAT_BC3_UNORM:
	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC5_TYPELESS:
	case DXGI_FORMAT_BC5_UNORM:
	case DXGI_FORMAT_BC5_SNORM:
	case DXGI_FORMAT_BC6H_TYPELESS:
	case DXGI_FORMAT_BC6H_UF16:
	case DXGI_FORMAT_BC6H_SF16:
	case DXGI_FORMAT_BC7_TYPELESS:
	case DXGI_FORMAT_BC7_UNORM:
	case DXGI_FORMAT_BC7_UNORM_SRGB:
		return 8;

	default:
		return 0;
	}

}

ASSET_ERROR_CODES Texture::LoadDefaultTexture(ID3D11Device* device, MemoryTracker* memTracker)
{
	defaultTexture = new Texture();

	ID3D11Texture2D* tempTex;

	std::vector<uint8_t> textureData;
	//Magenta 
	textureData.push_back(std::numeric_limits<uint8_t>::max());
	textureData.push_back(0);
	textureData.push_back(0);
	textureData.push_back(0);

	//Black
	textureData.push_back(0);
	textureData.push_back(0);
	textureData.push_back(0);
	textureData.push_back(std::numeric_limits<uint8_t>::max());

	//Black
	textureData.push_back(0);
	textureData.push_back(255);
	textureData.push_back(0);
	textureData.push_back(std::numeric_limits<uint8_t>::max());

	//Magenta 
	textureData.push_back(0);
	textureData.push_back(0);
	textureData.push_back(std::numeric_limits<uint8_t>::max());
	textureData.push_back(std::numeric_limits<uint8_t>::max());

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = &textureData[0];
	initData.SysMemSlicePitch = 0;
	initData.SysMemPitch = 2 * sizeof(uint8_t) * 4;

	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = 2;
	textureDesc.Height = 2;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;

	HRESULT hr = device->CreateTexture2D(&textureDesc, &initData, &tempTex);
	if (FAILED(hr))
	{
		std::cout << "Failed to create Texture2D object for Default Texture" << std::endl;
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_CREATE_DIRECTXOBJECT;
	}
	SetDebugObjectName(tempTex, "Default_Texture_Object");
	defaultTexture->m_Texture.reset(tempTex);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	//srvDesc.Format = textureDesc.Format;
	//srvDesc.
	ID3D11ShaderResourceView* tempSRV;

	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;


	hr = device->CreateShaderResourceView(defaultTexture->m_Texture.get(), nullptr, &tempSRV);
	if (FAILED(hr))
	{
		_com_error error(hr);
		std::cout << "Failed to create SRV object for Default Texture: " << error.ErrorMessage() << std::endl;
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_CREATE_DIRECTXOBJECT;
	}

	SetDebugObjectName(tempSRV, "Default_Texture_SRV");
	defaultTexture->m_TextureSRV.reset(tempSRV);

	Texture::defaultTexture->isLoaded = true;

	memTracker->Submit_RAM(AssetType::TEXTURE, 16, 0);
	return ASSET_ERROR_CODES::SUCCESS;
}

ASSET_ERROR_CODES Texture::Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker)
{
	return ASSET_ERROR_CODES::ERROR_UNKOWN;
}

void Texture::Unload(MemoryTracker* memTracker)
{
	memTracker->Remove_RAM(AssetType::TEXTURE, GetSize());

	m_TextureSRV.reset(nullptr);
	m_Texture.reset(nullptr);
}

