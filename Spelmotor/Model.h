#pragma once
#include <d3d11.h>
#include <directxmath.h>
#include <vector>
#include "Asset.h"

using namespace DirectX;

class Model :
	public Asset
{

public:

	struct Vertex
	{
		DirectX::XMFLOAT3 Position;
		DirectX::XMFLOAT3 Normal;
		DirectX::XMFLOAT2 TexCoords;

		Vertex()
		{ }
		Vertex(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 normal, DirectX::XMFLOAT2 texCoords)
			:Position(position), Normal(normal), TexCoords(texCoords)
		{ }
		Vertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float texU, float texV)
			: Position(posX, posY, posZ), Normal(normX, normY, normZ), TexCoords(texU, texV)
		{ }
	};

	void Shutdown();

	virtual size_t GetSize();

	int GetIndexCount();
	int GetVertexCount() const;

	static Model* GetDefaultModel();

	ID3D11Buffer* GetVertexBuffer() const; 
	ID3D11Buffer* GetIndexBuffer() const;

	unsigned int GetStride() const;
	unsigned int GetOffset() const;


private:

	Model();
	~Model();

	//Creates a default model that will be used while waiting for models to load asynchronously
	static ASSET_ERROR_CODES LoadDefaultModel(ID3D11Device* device, MemoryTracker* memTracker);

	ASSET_ERROR_CODES Load(ID3D11Device * device, void * data, int dataSize, const std::string& name, MemoryTracker* memTracker);
	//TODO: implement
	void Unload(MemoryTracker* memTracker);

	bool InitializeBuffers(ID3D11Device* device, void* data, int dataSize, const std::string& name);
	void ShutdownBuffers();

	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	unsigned int m_vertexStride;
	unsigned int m_verterxOffset;
	
	static Model* defaultModel;
	friend class AssetManager;
};

