#include "PhysicsManager.h"
#include <iostream>

#define PVD_HOST "127.0.0.1"	//Set this to the IP address of the system running the PhysX Visual Debugger that you want to connect to.

PhysicsManager* PhysicsManager::instance;

PhysicsManager::PhysicsManager()
{
}


PhysicsManager* PhysicsManager::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new PhysicsManager();
		instance->Init();
	}
	return instance;
}

physx::PxMaterial* PhysicsManager::CreateMaterial(float staticFriction, float dynamicFriction, float restitution)
{
	return m_physics->createMaterial(staticFriction, dynamicFriction, restitution);
}

physx::PxRigidDynamic* PhysicsManager::CreateDynamic(physx::PxVec3 position, physx::PxSphereGeometry geometry, physx::PxMaterial* material)
{
	if (material == nullptr)
		material = CreateMaterial(1, 1, 0.1f);
	auto dyn =  physx::PxCreateDynamic(*m_physics, physx::PxTransform(position), geometry, *material, 1.0f);
	m_scene->addActor(*dyn);
	return dyn;
}
physx::PxRigidDynamic* PhysicsManager::CreateDynamic(physx::PxVec3 position, physx::PxBoxGeometry geometry)
{
	auto dyn = physx::PxCreateDynamic(*m_physics, physx::PxTransform(position), geometry, *CreateMaterial(1, 1, 1), 1.0f);
	m_scene->addActor(*dyn);
	return dyn;
}

void PhysicsManager::RemoveObject(physx::PxRigidDynamic* object)
{
	m_scene->removeActor(*object);
}

physx::PxRigidDynamic* PhysicsManager::CreateDynamic(physx::PxVec3 position, physx::PxGeometry geometry, physx::PxMaterial* material)
{
	return physx::PxCreateDynamic(*m_physics, physx::PxTransform(position), geometry, *material, 1.0f);
}

physx::PxRigidStatic* PhysicsManager::CreateStatic(physx::PxVec3 position, physx::PxBoxGeometry geometry, float staticFriction, float dynamicFriction, float restitution)
{
	auto stat = physx::PxCreateStatic(*m_physics, physx::PxTransform(position), geometry, *CreateMaterial(staticFriction, dynamicFriction, restitution));
	m_scene->addActor(*stat);
	 return stat;
}

PhysicsManager::~PhysicsManager()
{
}

void PhysicsManager::Update(float deltaTime)
{
	m_scene->simulate(deltaTime);
	m_scene->fetchResults(true);
}

bool PhysicsManager::Init()
{

	m_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
	physx::PxProfileZoneManager* profileZoneManager = &physx::PxProfileZoneManager::createProfileZoneManager(m_foundation);
	m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_foundation, physx::PxTolerancesScale(), true, profileZoneManager);

	if (!m_foundation || !profileZoneManager || !m_physics || !m_foundation)
		return false;


	if (m_physics->getPvdConnectionManager())
	{
		m_physics->getVisualDebugger()->setVisualizeConstraints(true);
		m_physics->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_CONTACTS, true);
		m_physics->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_SCENEQUERIES, true);
		m_connection = physx::PxVisualDebuggerExt::createConnection(m_physics->getPvdConnectionManager(), PVD_HOST, 5425, 10);
	}

	physx::PxSceneDesc sceneDesc(m_physics->getTolerancesScale());
	sceneDesc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);
	m_dispatcher = physx::PxDefaultCpuDispatcherCreate(2);
	sceneDesc.cpuDispatcher = m_dispatcher;
	sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;
	m_scene = m_physics->createScene(sceneDesc);


	auto gMaterial = m_physics->createMaterial(0.5f, 0.5f, 0.6f);
	physx::PxRigidStatic* groundPlane = physx::PxCreatePlane(*m_physics, physx::PxPlane(0, 1, 0, 0), *gMaterial);
	m_scene->addActor(*groundPlane);


}
