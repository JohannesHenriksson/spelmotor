#include "PixelShader.h"

#include <iostream>
#include <comdef.h>


PixelShader::PixelShader()
{ }


PixelShader::~PixelShader()
{ }

ID3D11PixelShader* PixelShader::GetShader() const
{
	return m_pixelShader.get();
}

size_t PixelShader::GetVRAMSize()
{
	return 0;
}

ASSET_ERROR_CODES PixelShader::Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker)
{
	LoadShaderSource(data, dataSize);
	if(m_shaderSource == nullptr)
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_READ_FILE;

	ID3D11PixelShader* tempShader;
	HRESULT hr = device->CreatePixelShader(m_shaderSource->GetBufferPointer(), m_shaderSource->GetBufferSize(), nullptr, &tempShader);

	if(FAILED(hr))
	{
		_com_error err(hr);
		LPCTSTR errMsg = err.ErrorMessage();
		std::cout << "Failed to create pixelshader: " << errMsg << std::endl;
		return ASSET_ERROR_CODES::ERROR_UNKOWN;
	}

	m_pixelShader.reset(tempShader);

	SetDebugObjectName(m_pixelShader.get(), name);

	memTracker->Submit_RAM(AssetType::SHADER, GetSize(), dataSize);

	isLoaded = true;
	return ASSET_ERROR_CODES::SUCCESS;
	//Shader reflections mabye??!

}

void PixelShader::Unload(MemoryTracker* memTracker)
{
	memTracker->Remove_RAM(AssetType::SHADER, GetSize());

	m_pixelShader.reset(nullptr);
	m_shaderSource.reset(nullptr);

	isLoaded = false;
}
