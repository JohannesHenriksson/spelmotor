#pragma once
#include <map>
#include "AssetTypes.h"
class MemoryTracker
{
public:
	//Default allows for 100mb of assets in RAM
	MemoryTracker(int max_RAM_Size = 100'000'000, int max_VRAM_Size = 50'000);
	~MemoryTracker();
	void Submit_RAM(AssetType type, int memoryInBytes, int reservedMemory);
	void Submit_VRAM(AssetType type, int memoryInBytes);
	void Remove_RAM(AssetType type, int memoryInBytes);
	void Remove_VRAM(AssetType type, int memoryInBytes);
	int Get_RAM_For(AssetType type);
	int Get_VRAM_For(AssetType type);
	int GetTotal_RAM();
	int GetTotal_VRAM();
	void SetMax_RAM_Usage(int maxRam);
	void SetMax_VRAM_Usage(int maxVram);
	int GetMax_RAM();
	int GetMax_Vram();
	int Get_RAM_left();
	int Get_VRAM_left();
	void PrintMemoryUsePerType();
	void ReserveMemory(int memory);
	std::string GetEnumString(AssetType type);
private:
	int m_maxRAM;
	int m_maxVram;
	int m_totalRAM;
	int m_totalVRAM;
	int m_reservedMemory;
	//Holds how much data each asset type is occupying
	std::map<AssetType, std::pair<int, int>> m_assetMemory;
};

