#pragma once
#include "Asset.h"
#include "fmod.hpp"
#include "fmod_errors.h"

class Audio :
	public Asset
{
public:
	Audio();
	~Audio();
	ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker) override;
	void Unload(MemoryTracker* memTracker) override;
	FMOD::Sound* GetAudioSource();
	void SetAudioSource(FMOD::Sound* sound);

	virtual size_t GetSize();
private:
	int m_size;
	FMOD::Sound* audioSource;
};

