#include "FreeFlightCamera.h"



FreeFlightCamera::FreeFlightCamera()
{
	pitch = 0;
	yaw = 0;
	this->input = input;

	LookAt(this->m_position + this->m_forward);
}


FreeFlightCamera::~FreeFlightCamera()
{

}

void FreeFlightCamera::Update(float time)
{
	float speed = 0.008f;
	DirectX::XMFLOAT2 mouseDelta = Input::GetMouseDelta();

	if (!Window::GetWindowIsFocused())
		return;

	float pitchDelta = mouseDelta.y / 1000;
	float yawDelta = mouseDelta.x / 1000;

	if (pitch < -3.14f / 2.f)
		pitch = -3.14f / 2.f;
	else if (pitch > 3.14f / 2.f)
		pitch = 3.14f / 2.f;
	else
		pitch += pitchDelta;
	yaw += yawDelta;

	// Reset camera axis
	m_forward = DirectX::XMFLOAT3(0, 0, 1);
	m_up = DirectX::XMFLOAT3(0, 1, 0);
	m_right = DirectX::XMFLOAT3(1, 0, 0);

	// Calculate yaw rotation matrix and apply it on the forward and right vector
	DirectX::XMMATRIX rotationMatrixYaw = DirectX::XMMatrixRotationAxis(DirectX::XMLoadFloat3(&m_up), yaw);
	RotateVec3(this->m_forward, rotationMatrixYaw);
	RotateVec3(this->m_right, rotationMatrixYaw);

	// Calculate pitch rotation matrix and apply it on the forward and up vector
	DirectX::XMMATRIX rotationMatrixPitch = DirectX::XMMatrixRotationAxis(DirectX::XMLoadFloat3(&m_right), pitch);
	RotateVec3(this->m_forward, rotationMatrixPitch);
	RotateVec3(this->m_up, rotationMatrixPitch);

	// Look at new direction
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&(m_position + m_forward)), DirectX::XMLoadFloat3(&m_up)));

	if(Input::GetAsyncKeyState('A'))
	{
		this->m_position -= this->m_right * speed;
	}
	if(input->GetAsyncKeyState('D'))
	{
		this->m_position += this->m_right * speed;
	}
	if(input->GetAsyncKeyState('W'))
	{
		this->m_position += this->m_forward * speed;
	}
	if(input->GetAsyncKeyState('S'))
	{
		this->m_position -= this->m_forward * speed;
	}
	if(input->GetAsyncKeyState(VK_LCONTROL))
	{
		this->m_position -= this->m_up * speed;
	}
	if(input->GetAsyncKeyState(VK_LSHIFT))
	{
		this->m_position += this->m_up * speed;
	}
}



