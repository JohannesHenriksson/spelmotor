#ifndef VERTEX_SHADER_H
#define VERTEX_SHADER_H

#include "Shader.h"
#include <vector>

class VertexShader :
	public Shader
{
public:
	VertexShader();
	~VertexShader();

	ID3D11VertexShader* GetShader() const;
	ID3D11InputLayout* GetInputLayout() const;

	void SetInputLayout(ID3D11InputLayout* inputLayout);
	void CreateInputLayout(ID3D11Device* device, const std::vector<D3D11_INPUT_ELEMENT_DESC>& inputDescs);


private:

	ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker);
	void Unload(MemoryTracker* memTracker);

	std::unique_ptr<ID3D11VertexShader, ComDeleter> m_vertexShader;
	std::unique_ptr<ID3D11InputLayout, ComDeleter> m_inputLayout;

	friend class AssetManager;
};
#endif
