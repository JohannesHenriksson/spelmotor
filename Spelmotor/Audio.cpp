#include "Audio.h"
#include "SoundManager.h"
#include "AssetTypes.h"

Audio::Audio()
{
}


Audio::~Audio()
{
}

ASSET_ERROR_CODES Audio::Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker)
{
	m_size = dataSize;
	memTracker->Submit_RAM(AssetType::AUDIO, GetSize(), dataSize);
	return SoundManager::GetInstance()->CreateSound(data, dataSize, this);
}

void Audio::Unload(MemoryTracker* memTracker)
{
	memTracker->Remove_RAM(AssetType::AUDIO, GetSize());
}

void Audio::SetAudioSource(FMOD::Sound* sound)
{
	this->audioSource = sound;
}

size_t Audio::GetSize()
{
	return m_size;
}


FMOD::Sound* Audio::GetAudioSource()
{
	return this->audioSource;
}
