#include "GameObject.h"
#include "Input.h"
#include "DxMath.h"

GameObject::GameObject(GraphicsManager* gm, AssetManager* assetManager)
	: m_graphicsManager(gm)
	, m_model(nullptr)
	, m_shaderPair(nullptr)
	, m_texture(nullptr)
	, m_draw(true)
	, m_rotation({0.0f, 0.0f, 0.0f, 1.0f})
	, m_position({ 0.0f, 2.0f, 0.0f })
	, m_scale({ 1.0f, 1.0f, 1.0f })
{
}

GameObject::GameObject(GraphicsManager* gm, Model* model)
	: m_graphicsManager(gm)
	, m_model(model)
	, m_shaderPair(nullptr)
	, m_texture(nullptr)
	, m_draw(true)
	, m_rotation({ 0.0f, 0.0f, 0.0f , 1.0f})
	, m_position({ 0.0f, 0.0f, 0.0f })
	, m_scale({ 1.0f, 1.0f, 1.0f })
{

}

GameObject::~GameObject()
{
}

void GameObject::Update()
{
	DirectX::XMFLOAT3 direction;
	DirectX::XMStoreFloat3(&direction, DirectX::XMVector3Rotate(DirectX::XMVectorSet(0, 0, 1, 1), DirectX::XMLoadFloat4(&m_rotation)));


	DirectX::XMStoreFloat4x4(&m_instanceData.worldMatrix, 
		DirectX::XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z) * DirectX::XMMatrixRotationQuaternion(DirectX::XMLoadFloat4(&m_rotation))*
							DirectX::XMMatrixTranslation(m_position.x, m_position.y, m_position.z));
}

void GameObject::Draw()
{
	if (m_draw)
		m_graphicsManager->InsertDrawData(m_shaderPair, m_model, m_texture, m_instanceData);
}

void GameObject::SetPosition(XMFLOAT3 newPos)
{
	m_position = newPos;
}

void GameObject::SetRotation(XMFLOAT4 newRotation)
{
	m_rotation = newRotation;
}

void GameObject::SetColor(const XMFLOAT4& color)
{
	m_instanceData.color = color;
}

void GameObject::SetModel(Model * model)
{
	m_model = model;
}

void  GameObject::SetTexture(Texture* texture)
{
	m_texture = texture;
}
