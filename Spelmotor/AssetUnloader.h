#pragma once
#include "ReplacementPolicies.h"
#include "Asset.h"
#include <deque>
#include "MemoryTracker.h"
class AssetUnloader
{
public:
	AssetUnloader(std::map<uint64_t, Asset*>* assetMapPtr, ReplacementPolicy policy = ReplacementPolicy::LRU);
	~AssetUnloader();

	void SetPolicy(ReplacementPolicy policy);
	//The minimum size needed to be freed
	bool FreeMemory(int size, MemoryTracker* memTracker);
	void ReportUnusedAsset(Asset* asset);
	bool FreeMem_LRU(int size, MemoryTracker* memTracker);
	bool FreeMem_MRU(int size, MemoryTracker* memTracker);

	std::deque<Asset*> unusedAssets;
private:
	ReplacementPolicy m_policy;
	std::map<uint64_t, Asset*>* assetMap;
};

