#include "Unpacker.h"
#include <zzip\zzip.h>

Unpacker::Unpacker()
{

}


Unpacker::~Unpacker()
{

}

void Unpacker::OutputEntryNames(std::string archivePath)
{
	struct archive *a;
	struct archive_entry *entry;
	int r;

	std::cout << "Archive: " << archivePath << " contains the entries: \n";

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);
	r = archive_read_open_filename(a, archivePath.c_str(), 10240);
	if(r != ARCHIVE_OK)
	{
		std::cout << "libarchive ERROR: could not open archive \n";
		return;
	}

	while(archive_read_next_header(a, &entry) == ARCHIVE_OK)
	{
		std::cout << archive_entry_pathname(entry) << "\n";
		archive_read_data_skip(a);
	}

	std::cout << "\n";

	r = archive_read_free(a);
	if(r != ARCHIVE_OK)
	{
		std::cout << "libarchive ERROR: could not free archive \n";
		return;
	}
}

void Unpacker::OutputArchiveData(std::string archivePath)
{
	int r;
	size_t size = 1;
	struct archive* a;
	struct archive_entry* entry;

	int bufferSize = 288;
	char* buff;
	buff = (char*)malloc(bufferSize);

	a = archive_read_new();
	//archive_read_support_filter_all(a);

	archive_read_support_compression_all(a);
	archive_read_support_format_zip(a);
	

	r = archive_read_open_filename(a, archivePath.c_str(), 288);
	if(r != ARCHIVE_OK)
	{
		std::cout << "libarchive ERROR: could not open archive \n";
		return;
	}

	while(size > 0)
	{
		// Move to next entry
		r = archive_read_next_header(a, &entry);
		if(r != ARCHIVE_OK)
		{
			std::cout << "Error: could not read next header in zip: " + archivePath;
			return;
		}

		std::cout << "Entry: " << archive_entry_pathname(entry) << " contains the following data: '";
		// Read whats in the entry
		size = archive_read_data(a, buff, bufferSize);

		// Outputs content as chars
		for(int i = 0; i < size; i++)
		{
			std::cout << buff[i];
		}
		std::cout << "'\n";
	}

	archive_read_free(a);
}

void Unpacker::ReadFromArchiveOffset(std::string archivePath, int offsetNumberOffFiles, int size, void*& output)
{
	std::string fileEnding = archivePath.substr(archivePath.find("."));

	if (fileEnding == ".zip")
		ReadFromArchiveOffsetZip(archivePath, offsetNumberOffFiles, size, output);
	else if (fileEnding == ".rip")
		ReadFromArchiveOffsetCustom(archivePath, offsetNumberOffFiles, size, output);
	else
		std::cout << "Cannot open archive '" << archivePath << "', file ending: " << fileEnding << " is not supported \n";
}

size_t Unpacker::LibArchiveReadFromArchiveOffset(std::string archivePath, int offsetNumberOffFiles, int size, void *& output)
{
	int r;
	struct archive* a;
	int sizet = 0;
	struct archive_entry* entry = nullptr;
	la_int64_t offset = 10;

	int bufferSize = size;
	void* buff;
	buff = malloc(bufferSize);

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);

	r = archive_read_open_filename(a, archivePath.c_str(), 10240); // TODO: wat is this block size?
	if(r != ARCHIVE_OK)
	{
		std::cout << "libarchive ERROR: could not open archive: " + archivePath + "\n";
		return -1;
	}

	int i = 0;
	while(i < offsetNumberOffFiles + 1)
	{
		//archive_read_data_skip(a);
		archive_read_next_header(a, &entry);
		i++;
	}

	// Read whats in the entry
	sizet = archive_read_data(a, buff, bufferSize);

	archive_read_free(a);

	output = buff;
	return sizet;
}

void Unpacker::ZipTest(std::string archiveName, int offsetNumberOffFiles)
{
	ZZIP_DIR* dir = zzip_dir_open("test.zip", 0);

	ZZIP_DIRENT dirent;

	int i = 0;
	while (i < offsetNumberOffFiles + 1)
	{
		//archive_read_data_skip(a);
		if (zzip_dir_read(dir, &dirent))
			std::cout << "Hej: " << dirent.d_name << ", " << dirent.d_csize << ", " << dirent.st_size << "\n";
		else
			std::cout << "ERROR!!";
		i++;
	}

	ZZIP_FILE* filepointer = zzip_file_open(dir, dirent.d_name, 0);

	char buff[10000];
	zzip_ssize_t size = zzip_file_read(filepointer, buff, dirent.st_size);
	// Outputs content as chars
	for (int i = 0; i < size; i++)
	{
		std::cout << buff[i];
	}
	std::cout << "'\n";

}

void Unpacker::ReadFromArchiveOffsetZip(std::string archivePath, int offsetNumberOffFiles, int size, void*& output)
{
	ZZIP_DIR* dir = zzip_dir_open(archivePath.c_str(), 0);

	if(dir == nullptr)
		std::cout << "Error: trying to open archive \"" << archivePath << "\": " << zzip_strerror_of(dir) << "\n";

	ZZIP_DIRENT dirent;

	int i = 0;
	while (i < offsetNumberOffFiles + 1)
	{
		if (!zzip_dir_read(dir, &dirent))
			std::cout << "Error could not read archive: " << archivePath << " \n";
		i++;
	}

	ZZIP_FILE* filepointer = zzip_file_open(dir, dirent.d_name, 0);

	if (filepointer == nullptr)
		std::cout << "Error trying to open file \"" << dirent.d_name << "\": " << zzip_strerror_of(dir) << "\n";

	//void* buff;
	//buff = malloc(size);

	// Reads zipped file and sets output to point on the data
	zzip_ssize_t readSize = zzip_file_read(filepointer, output, size);

	if (readSize != size)
	{
		std::cout << "Error: read size is not equal to size while trying to read from archive: " << archivePath << " and file: " << dirent.d_name << ". Something went wrong! \n";
	}
	
	//output = buff;

	zzip_dir_close(dir);
	zzip_file_close(filepointer);
	dir = nullptr;
	filepointer = nullptr;
	//buff = nullptr;
}

void Unpacker::ReadFromArchiveOffsetCustom(std::string archivePath, int offsetNumberOffFiles, int size, void*& output)
{
	const int fileHeaderSize = 20;
	char numberOfFilesBuffer[2];

	std::ifstream file("Assets/" + archivePath + ".rip", std::ios::binary);
	if (!file.is_open())
		std::cout << "Could not open archive: " << archivePath;

	file.read(numberOfFilesBuffer, sizeof(numberOfFilesBuffer));

	UINT16 numberOfFiles = *((UINT32*)numberOfFilesBuffer);
	int headerSize = numberOfFiles * fileHeaderSize + sizeof(numberOfFilesBuffer);


	file.seekg(sizeof(numberOfFilesBuffer) + offsetNumberOffFiles * fileHeaderSize);
	char headerBuffer[fileHeaderSize];
	file.read(headerBuffer, sizeof(headerBuffer));

	UINT32 fileSize = *((UINT32*)&headerBuffer[8]);
	UINT32 compressedSize = *((UINT32*)&headerBuffer[12]);
	UINT32 offset = *((UINT32*)&headerBuffer[16]);

	if (fileSize != size)
	{
		std::cout << "Error: file size is not equal to size while trying to read from archive: " << archivePath << " and file nr: " << offsetNumberOffFiles << ". Something went wrong! \n";
	}

	file.seekg(headerSize + offset);

	char* buffer = (char*)malloc(compressedSize);
	file.read(buffer, compressedSize);

	output = malloc(size);

	// inflate buffer into output
	// zlib struct
	z_stream infstream;
	infstream.zalloc = Z_NULL;
	infstream.zfree = Z_NULL;
	infstream.opaque = Z_NULL;
	// setup "buffer" as the compressed input and "output" as the output
	infstream.avail_in = (uInt)compressedSize; // size of input
	infstream.next_in = (Bytef *)buffer; // input char array
	infstream.avail_out = (uInt)fileSize; // size of output
	infstream.next_out = (Bytef *)output; // output char array

	// the actual DE-compression work.
	inflateInit(&infstream);
	inflate(&infstream, Z_NO_FLUSH);
	inflateEnd(&infstream);
}

std::vector<uint64_t> Unpacker::GetRipArchiveGuids(std::string archivePath)
{
	const int numberOfFilesBufferSize = 2;
	const int fileHeaderSize = 20;
	char numberOfFilesBuffer[numberOfFilesBufferSize];

	std::ifstream file("Assets/" + archivePath + ".rip", std::ios::binary);
	if (!file.is_open())
		std::cout << "Could not open archive: " << archivePath;

	file.read(numberOfFilesBuffer, numberOfFilesBufferSize);

	UINT16 numberOfFiles = *((UINT32*)numberOfFilesBuffer);
	int headerSize = numberOfFiles * fileHeaderSize;

	std::vector<uint64_t> returnVector;
	char headerBuffer[fileHeaderSize];
	returnVector.push_back(numberOfFilesBufferSize + headerSize);//HeaderOffset
	for (int headerOffset = numberOfFilesBufferSize; headerOffset < numberOfFilesBufferSize + headerSize; headerOffset += fileHeaderSize)
	{
		file.seekg(headerOffset);
		file.read(headerBuffer, fileHeaderSize);
		returnVector.push_back(*((uint64_t*)&headerBuffer[0]));//GUID
		returnVector.push_back(*((uint64_t*)&headerBuffer[12]));//Compressed Size and Offset
	}
	return returnVector;
}

void Unpacker::ReadFromArchiveOffsetRip(std::string archivePath, int offset, int compressedSize, int size, void*& output)
{
	std::ifstream file("Assets/" + archivePath + ".rip", std::ios::binary);
	if (!file.is_open())
		std::cout << "Could not open archive: " << archivePath;

	file.seekg(offset);

	char* buffer = (char*)malloc(compressedSize);
	file.read(buffer, compressedSize);

	output = malloc(size);

	// inflate buffer into output
	// zlib struct
	z_stream infstream;
	infstream.zalloc = Z_NULL;
	infstream.zfree = Z_NULL;
	infstream.opaque = Z_NULL;
	// setup "buffer" as the compressed input and "output" as the output
	infstream.avail_in = (uInt)compressedSize; // size of input
	infstream.next_in = (Bytef *)buffer; // input char array
	infstream.avail_out = (uInt)size; // size of output
	infstream.next_out = (Bytef *)output; // output char array

										  // the actual DE-compression work.
	inflateInit(&infstream);
	inflate(&infstream, Z_NO_FLUSH);
	inflateEnd(&infstream);
}
