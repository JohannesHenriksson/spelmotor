#ifndef TEXTURE_H
#define TEXTURE_H

#include "Asset.h"
#include <memory>

class MemoryTracker;

class Texture :
	public Asset
{
public:


	Texture();
	~Texture();

	ID3D11ShaderResourceView* GetTextureShaderResourceView() const;
	ID3D11Texture2D* GetTexture() const;

	static Texture* GetDefaultTexture();

	virtual size_t GetSize();

protected:
	std::unique_ptr<ID3D11ShaderResourceView, ComDeleter> m_TextureSRV;
	std::unique_ptr<ID3D11Texture2D, ComDeleter> m_Texture;

	//Size data
	unsigned int m_textureWidth;
	unsigned int m_textureHeight;
	unsigned int m_bitsPerPixel;

	unsigned int GetBitsPerPixel(DXGI_FORMAT format);
private:

	static ASSET_ERROR_CODES LoadDefaultTexture(ID3D11Device* device, MemoryTracker* memTracker);

	ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker);
	void Unload(MemoryTracker* memTracker);


	//TODO: Create default texture
	static Texture* defaultTexture;
	friend class AssetManager;
};

#endif