#ifndef CAMERA_H
#define CAMERA_H

#include <DirectXMath.h>
#include <d3d11.h>
#include "DxMath.h"

class Camera
{
public:


	Camera(float aspectRatioHbW = (1280.f / 720.f), float nearPlane = 0.1f, float farPlane = 3000.0f, float fieldOfView = DirectX::XM_PIDIV4);
	Camera(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 forward, DirectX::XMFLOAT3 up, DirectX::XMFLOAT3 right, float aspectRatioHbW = 1280.f / 720.f, float nearPlane = 0.1f, float farPlane = 300.0f, float fieldOfView = DirectX::XM_PIDIV4);
	~Camera();



	DirectX::XMFLOAT3 GetPosition() const;
	void SetPosition(DirectX::XMFLOAT3 position);
	//Look at a position from current camera position
	void LookAt(DirectX::XMFLOAT3 objectPos);
	//Look at a position from a given camera position
	void LookAt(DirectX::XMFLOAT3 objectPos, DirectX::XMFLOAT3 cameraPos);

	DirectX::XMFLOAT4X4 GetViewMatrix() const;
	DirectX::XMFLOAT4X4 GetProjectionMatrix() const;
	DirectX::XMFLOAT4X4 GetViewProj() const;

	void SetProjection(float nearZ, float farZ, float aspectRatio, float fieldOfView);
	void RotateVec3(DirectX::XMFLOAT3& vector, DirectX::XMMATRIX rotationMatrix);

	DirectX::XMFLOAT3 GetForward() { return m_forward; }

	virtual void Update(float time)
	{ };



protected:	
	DirectX::XMFLOAT3 m_position;
	DirectX::XMFLOAT3 m_forward;
	DirectX::XMFLOAT3 m_right;
	DirectX::XMFLOAT3 m_up;

	DirectX::XMFLOAT4X4	m_viewMatrix;
	DirectX::XMFLOAT4X4	m_projMatrix;



private:
	float	m_nearZ;
	float	m_farZ;
	float	m_aspectRatio;
	float	m_fieldOfView;
};

#endif //CAMERA_H