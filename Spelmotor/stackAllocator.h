#ifndef STACK_ALLOCATOR
#define STACK_ALLOCATOR

class StackAllocator
{
private:
	char* m_buffer;
	int m_bufferSize;
	int m_currentOffset;
	int m_stackOverFlow;

	void AllocateMemory(int bytes);
public:
	StackAllocator() {}
	//number of bytes to allocate from the OS
	StackAllocator(size_t bufferSize);
	~StackAllocator();

	//resets the offset back to zero
	void Release();
	int MemoryInUse();
	int GetFreeMemory();
	bool isStackOverFLow(int dataTypeSize);
	int CalcNewStackSize();

	//Allocates the amount of memory required for the type and returns a pointer of the type.
	template<typename T>
	inline T* Alloc() 
	{
		int dataTypeSize = sizeof(T);
	
		if (isStackOverFLow(dataTypeSize))
		{
			return new T();
		}

		void* allocatedMemory = m_buffer + m_currentOffset;
		m_currentOffset += dataTypeSize;
		return (T*)allocatedMemory;
	}
	inline void* Alloc(size_t size)
	{
		if(isStackOverFLow(size) || size == 0)
		{
			return nullptr;
		}

		void* allocatedMemory = m_buffer + m_currentOffset;
		m_currentOffset += (int)size;
		return allocatedMemory;
	}
};

#endif // STACK_ALLOCATOR