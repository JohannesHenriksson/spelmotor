#pragma once

#include "Camera.h"
#include "Input.h"
#include "Window.h"

struct Point
{
	Point()
	{ }
	Point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
	int x;
	int y;
};

class FreeFlightCamera : public Camera
{
public:
	FreeFlightCamera();
	~FreeFlightCamera();

	void Update(float time);
private:
	Input* input;
	Point prevMousePos;

	float pitch;
	float yaw;

	DirectX::XMFLOAT4 rotationQuaternion;
};

