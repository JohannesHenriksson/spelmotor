#ifndef Unpacker_h__
#define Unpacker_h__

#include <iostream>
#include <fstream>
#include <string>

#include <libarchive\archive.h>
#include <libarchive\archive_entry.h>
#include <zlib\zlib.h>
#include <vector>

class Unpacker
{
public:
	Unpacker();
	~Unpacker();

	void OutputEntryNames(std::string archivePath);
	void OutputArchiveData(std::string archivePath);

	void ReadFromArchiveOffset(std::string archivePath, int offsetNumberOffFiles, int size, void*& output);
	void ReadFromArchiveOffsetZip(std::string archivePath, int offsetNumberOffFiles, int size, void*& output);
	void ReadFromArchiveOffsetCustom(std::string archivePath, int offsetNumberOffFiles, int size, void*& output);

	std::vector<uint64_t> GetRipArchiveGuids(std::string archivePath);
	void ReadFromArchiveOffsetRip(std::string archivePath, int offset, int compressedSize, int size, void*& output);

	void ZipTest(std::string archiveName, int offsetNumberOffFiles);

	size_t LibArchiveReadFromArchiveOffset(std::string archivePath, int offsetNumberOffFiles, int size, void*& output);
};

#endif // Unpacker_h__