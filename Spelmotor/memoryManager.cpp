#include "memoryManager.h"

MemoryManager::MemoryManager()
{
	m_pool = std::map<int, PoolAllocator*>();
	m_stack = std::map<int, StackAllocator*>();
}


MemoryManager::~MemoryManager()
{
	for(auto sa : m_stack)
		delete sa.second;
	for(auto sa : m_pool)
		delete sa.second;
}

PoolAllocator* MemoryManager::CreatePool(int numberOfSlots, int slotSize, int poolID)
{
	memoryLock.lock();
	PoolAllocator* t_pool = new PoolAllocator(numberOfSlots, slotSize);
	auto it = m_pool.find(poolID);
	if (it == m_pool.end())
		m_pool[poolID] = t_pool;
	else
	{
		return m_pool[poolID];
	}
	memoryLock.unlock();
	return t_pool;
}

PoolAllocator* MemoryManager::GetPool(int poolID)
{
	return m_pool[poolID];
}

PoolAllocator* MemoryManager::GetPool_safe(int poolID, int numberOfSlots, int slotSize)
{
	auto it = m_pool.find(poolID);
	if(it == m_pool.end())
		return CreatePool(numberOfSlots, slotSize, poolID);
	return m_pool[poolID];
}

void MemoryManager::DeletePool(int poolID)
{
	delete m_pool[poolID];
	m_pool.erase(m_pool.find(poolID));
}

//int MemoryManager::AllocPoolInt(size_t p_size)
//{
//	m_poolAlloc_int = PoolAllocator<int>(p_size * sizeof(int));
//	return 0;
//}

StackAllocator* MemoryManager::CreateStack(size_t p_size, int stackID)
{
	memoryLock.lock();
	StackAllocator* t_stack = new StackAllocator(p_size);
	auto it = m_stack.find(stackID);
	if (it == m_stack.end())
		m_stack[stackID] = t_stack;
	else
	{
		return m_stack[stackID];
	}
	memoryLock.unlock();
	return t_stack;

}

StackAllocator * MemoryManager::GetStack(int stackID)
{
	return m_stack[stackID];
}

StackAllocator* MemoryManager::GetStack_safe(int stackID, int size)
{
	auto it = m_stack.find(stackID);
	if (it == m_stack.end())
		return CreateStack(stackID, size);
	return m_stack[stackID];
}

void MemoryManager::DeleteStack(int stackID)
{
	delete m_stack[stackID];
	m_stack.erase(m_stack.find(stackID));
}