#include "Camera.h"

Camera::Camera(float aspectRatioHbW /*= 1280.f / 720.f*/, float nearPlane /*= 0.1f*/, float farPlane /*= 300.0f*/, float fieldOfView /*= DirectX::XM_PIDIV4*/)
	: m_position({ 0.0f, 0.0f, -1.0f })
	, m_forward({ 0.0f, 0.0f, 1.0f })
	, m_right({ 1.0f, 0.0f, 0.0f })
	, m_up({ 0.0f, 1.0f, 0.0f })
	, m_nearZ(nearPlane)
	, m_farZ(farPlane)
	, m_aspectRatio(aspectRatioHbW)
	, m_fieldOfView(fieldOfView)
{
	LookAt({ 0.0f, 0.0f, 0.0f }, m_position);
	DirectX::XMStoreFloat4x4(&m_projMatrix, DirectX::XMMatrixPerspectiveFovLH(m_fieldOfView, m_aspectRatio, m_nearZ, m_farZ));
}

Camera::Camera(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 forward, DirectX::XMFLOAT3 up, DirectX::XMFLOAT3 right, float aspectRatioHbW /*= 1280.f / 720.f*/, float nearPlane /*= 0.1f*/, float farPlane /*= 300.0f*/, float fieldOfView /*= DirectX::XM_PIDIV4*/)
	: m_position(position)
	, m_forward(forward)
	, m_right(right)
	, m_up(up)
	, m_nearZ(nearPlane)
	, m_farZ(farPlane)
	, m_aspectRatio(aspectRatioHbW)
	, m_fieldOfView(fieldOfView)
{
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&(m_position + m_forward)), DirectX::XMLoadFloat3(&m_up)));
	DirectX::XMMatrixPerspectiveFovLH(fieldOfView, m_aspectRatio, m_nearZ, m_farZ);
}

Camera::~Camera()
{ }

DirectX::XMFLOAT3 Camera::GetPosition() const
{
	return m_position;
}

void Camera::SetPosition(DirectX::XMFLOAT3 position)
{
	m_position = position;
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&(m_position + m_forward)), DirectX::XMLoadFloat3(&m_up)));
}

void Camera::LookAt(DirectX::XMFLOAT3 objectPos)
{
	DirectX::XMStoreFloat3(&m_forward, DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&(objectPos - m_position))));
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&(m_position + m_forward)), DirectX::XMLoadFloat3(&m_up)));
}

void Camera::LookAt(DirectX::XMFLOAT3 objectPos, DirectX::XMFLOAT3 cameraPos)
{
	m_position = cameraPos;
	DirectX::XMStoreFloat3(&m_forward, DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&(objectPos - m_position))));
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&(m_position + m_forward)), DirectX::XMLoadFloat3(&m_up)));
}

DirectX::XMFLOAT4X4 Camera::GetViewMatrix() const
{
	return m_viewMatrix;
}

DirectX::XMFLOAT4X4 Camera::GetProjectionMatrix() const
{
	return m_projMatrix;
}

DirectX::XMFLOAT4X4 Camera::GetViewProj() const
{
	return (m_viewMatrix * m_projMatrix);
}

void Camera::SetProjection(float nearZ, float farZ, float aspectRatio, float fieldOfView)
{
	DirectX::XMStoreFloat4x4(&m_projMatrix, DirectX::XMMatrixPerspectiveFovLH(fieldOfView, aspectRatio, nearZ, farZ));
}


void Camera::RotateVec3(DirectX::XMFLOAT3& vectorToRotate, DirectX::XMMATRIX rotationMatrix)
{
	DirectX::XMVECTOR vectorXM;
	vectorXM = DirectX::XMLoadFloat3(&vectorToRotate);

	DirectX::XMVECTOR newVector = DirectX::XMVector3TransformCoord(vectorXM, rotationMatrix);

	DirectX::XMStoreFloat3(&vectorToRotate, newVector);
}

