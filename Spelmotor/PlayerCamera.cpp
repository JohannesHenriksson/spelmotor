#include "PlayerCamera.h"



PlayerCamera::PlayerCamera(Input* input)
{
	m_input = input;
	m_cameraSpeed = .7;
}


PlayerCamera::~PlayerCamera()
{
}

void PlayerCamera::Update(float dt)
{
	float speed = 0.02f;

	// Look at new direction
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&(m_position + m_forward)), DirectX::XMLoadFloat3(&m_up)));

	DirectX::XMFLOAT3 moveDirection = m_targetPosition - m_position;

	moveDirection = DirectX::XMStoreFloat3(DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&moveDirection)));
	//
	m_position += moveDirection * m_cameraSpeed;
}


void PlayerCamera::UpdateTargetPosition(DirectX::XMFLOAT3 targetPos)
{
	m_targetPosition = targetPos;
}

void PlayerCamera::UpdateLookAtTarget(DirectX::XMFLOAT3 targetPos)
{
	DirectX::XMStoreFloat4x4(&m_viewMatrix, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&m_position), DirectX::XMLoadFloat3(&targetPos), DirectX::XMLoadFloat3(&m_up)));
}

