

struct VSInput
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoords : TEX_COORDS;

	float4x4 WorldMatrix : WORLD;
	float4 Color : COLOR;
	uint InstanceID : SV_InstanceID;
};

struct VSOutput
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 TexCoords : TEX_COORDS;
	float4 Color : COLOR;
	float3 lightPos1 : LIGHTPOS1;
	float3 lightPos2 : LIGHTPOS2;
};

cbuffer ViewProjBuffer : register(b0)
{
	float4x4 ViewProjectionMatrix;
};

cbuffer LightPositions : register(b1)
{
	float4 lightPosition[2];
}

VSOutput main( VSInput input )
{
	VSOutput output;

	output.Position = mul(input.WorldMatrix, float4(input.Position, 1.0f));

	output.lightPos1.xyz = lightPosition[0].xyz - output.Position.xyz;
	output.lightPos2.xyz = lightPosition[1].xyz - output.Position.xyz;
	//output.lightPos1 = normalize(output.lightPos1);
	//output.lightPos2 = normalize(output.lightPos2);

	output.Position = mul(ViewProjectionMatrix, output.Position);
	output.Normal = mul((float3x3)input.WorldMatrix, input.Normal);
	//output.Normal = input.Normal;
	output.TexCoords = input.TexCoords;
	output.Color = input.Color;
	return output;
}