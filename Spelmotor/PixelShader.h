#ifndef PIXEL_SHADER_H
#define PIXEL_SHADER_H
#include "Shader.h"
class PixelShader :
	public Shader
{
public:
	PixelShader();
	~PixelShader();

	ID3D11PixelShader* GetShader() const;

	virtual size_t GetVRAMSize();

private:
	ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker);
	void Unload(MemoryTracker* memTracker);


	std::unique_ptr<ID3D11PixelShader, ComDeleter> m_pixelShader;

	friend class AssetManager;
};

#endif