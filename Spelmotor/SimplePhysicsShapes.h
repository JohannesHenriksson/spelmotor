#pragma once

enum class SimpleShapes
{
	BOX,
	PLANE,
	SPHERE
};