#pragma once

enum class AssetType
{
	TEXTURE,
	MODEL,
	AUDIO,
	SHADER,
	PHYSICS,
	UNKNOWN
};