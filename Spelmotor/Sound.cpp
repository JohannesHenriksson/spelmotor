#include "Sound.h"


Sound::Sound(Audio* audioAsset)
{
	this->audioAsset = audioAsset;
	loadCompleted = true;
	volume = 1.f;
}

Sound::Sound()
{

}

void Sound::Init(Audio* audioAsset)
{
	this->audioAsset = audioAsset;
	this->loadCompleted = true;
	this->volume = 1.f;
}

void Sound::Play(DWORD playFlags)
{
	if(loadCompleted)
	{
		if(AudioOpt::LOOP_SOUND & playFlags)
		{
			SoundManager::GetInstance()->PlayLooped(this);
		}
		else
		{
			SoundManager::GetInstance()->PlayOnce(this);
		}
	}
	else
	{
		if(AudioOpt::LOOP_SOUND & playFlags)
		{
			boost::function<void()> my_proc = boost::bind(&Sound::Play, this, playFlags);
			functionCalls.push_back(my_proc);
		}
		else
			std::cout << "Cannot play sound! Audio has not been loaded yet \n";
	}
}

void Sound::Pause()
{
	if(loadCompleted)
	{
		SoundManager::GetInstance()->Pause(this);
		std::cout << "Pause \n";
	}
	else
	{
		boost::function<void()> my_proc = boost::bind(&Sound::Pause, this);
		functionCalls.push_back(my_proc);
	}
}

void Sound::ChangeVolume(float value)
{
	if(loadCompleted)
	{
		SoundManager::GetInstance()->ChangeVolume(this, value);
		std::cout << "Change volume \n";
	}
	else
	{
		boost::function<void()> my_proc = boost::bind(&Sound::ChangeVolume, this, value);
		functionCalls.push_back(my_proc);
	}
}

float Sound::GetVolume()
{
	if(loadCompleted)
	{
		float volume;
		channel->getVolume(&volume);
		return volume;
	}
	else
	{
		std::cout << "Warning: Sound::GetVolume called before audio finished loading \n";
		return 0;
	}
}

Audio* Sound::GetAudioAsset()
{
	if(loadCompleted)
	{
		return this->audioAsset;
	}
	else
	{
		std::cout << "Error: Sound::GetAudioAsset called before audio finished loading \n";
		return nullptr;
	}
}

FMOD::Channel* Sound::GetChannel()
{
	if(loadCompleted)
	{
		return this->channel;
	}
	else
	{
		std::cout << "Error: Sound::GetChannel called before audio finished loading \n";
		return nullptr;
	}
}

void Sound::SetChannel(FMOD::Channel* channel)
{
	this->channel = channel;
}

FMOD::Sound* Sound::GetAudioSource()
{
	if(loadCompleted)
	{
		return audioAsset->GetAudioSource();
	}
	else
	{
		std::cout << "Error: Sound::GetAudioSource called before audio finished loading \n";
		return nullptr;
	}
}
