#pragma once
#include "GameObject.h"
#include "PhysicsManager.h"
class StaticPhysicsGameObject :
	public GameObject
{
public:
	StaticPhysicsGameObject(GraphicsManager* gm, AssetManager* assetManager, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 size, DirectX::XMFLOAT4 color);
	~StaticPhysicsGameObject();

private:
	physx::PxRigidStatic* m_physObj;
};

