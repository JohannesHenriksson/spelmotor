#ifndef PlayerCamera_h__
#define PlayerCamera_h__

#include "Camera.h"
#include "Input.h"

class PlayerCamera 
	: public Camera
{
public:
	PlayerCamera(Input* input);
	~PlayerCamera();

	void Update(float dt);


	void UpdateTargetPosition(DirectX::XMFLOAT3 targetPos);
	void UpdateLookAtTarget(DirectX::XMFLOAT3 targetPos);
	void UpdateForward(DirectX::XMFLOAT3 forward) { m_forward = forward; }

private: 
	Input* m_input;
	float m_pitch, m_yaw;

	DirectX::XMFLOAT3 m_targetPosition;
	float m_cameraSpeed;
};
#endif // PlayerCamera_h__

