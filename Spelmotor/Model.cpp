#include "Model.h"
#include "DxMath.h"

Model* Model::defaultModel;

Model::Model()
	: m_vertexStride(sizeof(Vertex))
	, m_verterxOffset(0)
{

}


Model::~Model()
{
}


void Model::Shutdown()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	return;
}

size_t Model::GetSize()
{
	if(isLoaded)
		return m_vertexCount * sizeof(Vertex) + m_indexCount * sizeof(UINT);
	return 0;
}

ASSET_ERROR_CODES Model::Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker)
{
	if (InitializeBuffers(device, data, dataSize, name))
	{
		isLoaded = true;
		std::wstring s = L"Size difference for " + std::to_wstring(GetSize() / static_cast<float>(dataSize));
		OutputDebugStringW(s.c_str());
		memTracker->Submit_RAM(AssetType::MODEL, GetSize(), dataSize*2.125f);
		return ASSET_ERROR_CODES::SUCCESS;
	}

	else return ASSET_ERROR_CODES::ERROR_COULD_NOT_READ_FILE;

}

void Model::Unload(MemoryTracker* memTracker)
{
	memTracker->Remove_RAM(AssetType::MODEL, GetSize());
	if(m_vertexBuffer)
		m_vertexBuffer->Release();
	if(m_indexBuffer)
		m_indexBuffer->Release();
	m_indexCount = 0;
	m_vertexCount = 0;
	isLoaded = false;
}

int Model::GetIndexCount()
{
	return m_indexCount;
}

int Model::GetVertexCount() const
{
	return m_vertexCount;
}

Model* Model::GetDefaultModel()
{
	return defaultModel;
}

ID3D11Buffer* Model::GetVertexBuffer() const
{
	return m_vertexBuffer;
}

ID3D11Buffer* Model::GetIndexBuffer() const
{
	return m_indexBuffer;
}



unsigned int Model::GetStride() const
{
	return m_vertexStride;
}

unsigned int Model::GetOffset() const
{
	return m_verterxOffset;
}

ASSET_ERROR_CODES Model::LoadDefaultModel(ID3D11Device* device, MemoryTracker* memTracker)
{
	Model::defaultModel = new Model();
	std::vector<Vertex> vertices;
	//Front
	vertices.push_back(Vertex({ -.5f, -.5f, .5f }, { 0.f,0.f,1.f }, { 0.f,1.f }));
	vertices.push_back(Vertex({ .5f, -.5f, .5f }, { 0.f,0.f,1.f }, { 1.f,1.f }));
	vertices.push_back(Vertex({ .5f, .5f, .5f }, { 0.f,0.f,1.f }, { 1.f,0.f }));
	vertices.push_back(Vertex({ -.5f, .5f, .5f }, { 0.f,0.f,1.f }, { 0.f,0.f }));

	//right
	vertices.push_back(Vertex({ .5f, .5f, .5f }, { 1.f,0.f,0.f }, { 1.f,0.f }));
	vertices.push_back(Vertex({ .5f, .5f, -.5f }, { 1.f,0.f,0.f }, { 0.f,0.f }));
	vertices.push_back(Vertex({ .5f, -.5f, -.5f }, { 1.f,0.f,0.f }, { 0.f,1.f }));
	vertices.push_back(Vertex({ .5f, -.5f, .5f }, { 1.f,0.f,0.f }, { 1.f,1.f }));

	//back -- correct texcoords
	vertices.push_back(Vertex({ -.5f, -.5f, -.5f }, { 0.f,0.f,-1.f }, { 0.f,1.f }));
	vertices.push_back(Vertex({  .5f, -.5f, -.5f }, { 0.f,0.f,-1.f }, { 1.f,1.f }));
	vertices.push_back(Vertex({  .5f, .5f, -.5f }, { 0.f,0.f,-1.f }, { 1.f,0.f }));
	vertices.push_back(Vertex({ -.5f, .5f, -.5f }, { 0.f,0.f,-1.f }, { 0.f,0.f }));
								  
	//left						  
	vertices.push_back(Vertex({ -.5f, -.5f, -.5f }, { -1.f,0.f,0.f }, { 0.f,1.f }));
	vertices.push_back(Vertex({ -.5f, -.5f, .5f }, { -1.f,0.f,0.f }, { 0.f,0.f }));
	vertices.push_back(Vertex({ -.5f, .5f, .5f }, { -1.f,0.f,0.f }, { 1.f,0.f }));
	vertices.push_back(Vertex({ -.5f, .5f, -.5f }, { -1.f,0.f,0.f }, { 1.f,1.f }));
								  
	//Up						  
	vertices.push_back(Vertex({  .5f, .5f, .5f }, { 0.f,1.f,0.f }, { 1.f,1.f }));
	vertices.push_back(Vertex({ -.5f, .5f, .5f }, { 0.f,1.f,0.f }, { 0.f,1.f }));
	vertices.push_back(Vertex({ -.5f, .5f, -.5f }, { 0.f,1.f,0.f }, { 0.f,0.f }));
	vertices.push_back(Vertex({  .5f, .5f, -.5f }, { 0.f,1.f,0.f }, { 1.f,0.f }));
								  
	//Down						  
	vertices.push_back(Vertex({ -.5f, -.5f, -.5f }, { 0.f,-1.f,0.f }, { 0.f,0.f }));
	vertices.push_back(Vertex({  .5f, -.5f, -.5f }, { 0.f,-1.f,0.f }, { 1.f,0.f }));
	vertices.push_back(Vertex({  .5f, -.5f, .5f }, { 0.f,-1.f,0.f }, { 1.f,1.f }));
	vertices.push_back(Vertex({ -.5f, -.5f, .5f }, { 0.f,-1.f,0.f }, { 0.f,1.f }));

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * vertices.size();
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_FLAG(0);
	vertexBufferDesc.StructureByteStride = sizeof(Vertex);
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = &vertices[0];
	Model::defaultModel->m_vertexCount = vertices.size();

	HRESULT hr = device->CreateBuffer(&vertexBufferDesc, &initData, &Model::defaultModel->m_vertexBuffer);
	if (FAILED(hr))
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_CREATE_DIRECTXOBJECT;
		
	SetDebugObjectName(Model::defaultModel->m_vertexBuffer, "Default_model_VB");

	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3, //front
		4, 5, 6, 4, 6, 7, //right
		8, 9, 10, 8, 10, 11, //back
		12, 13, 14, 12, 14, 15, //left
		16, 17, 18, 16, 18, 19, //upper
		20, 21, 22, 20, 22, 23 }; //bottom

	Model::defaultModel->m_indexCount = indices.size();

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.ByteWidth = sizeof(unsigned int) * indices.size();
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_FLAG(0);
	indexBufferDesc.StructureByteStride = sizeof(unsigned int);
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA initIndexData;
	initIndexData.pSysMem = &indices[0];

	hr = device->CreateBuffer(&indexBufferDesc, &initIndexData, &Model::defaultModel->m_indexBuffer);
	if (FAILED(hr))
		return ASSET_ERROR_CODES::ERROR_COULD_NOT_CREATE_DIRECTXOBJECT;


	SetDebugObjectName(Model::defaultModel->m_indexBuffer, "Default_model_IB");

	Model::defaultModel->isLoaded = true;
	memTracker->Submit_RAM(AssetType::MODEL, Model::defaultModel->GetSize(), 0);
	return ASSET_ERROR_CODES::SUCCESS;
}

bool Model::InitializeBuffers(ID3D11Device* device, void* data, int dataSize, const std::string& name)
{
	std::vector<Vertex> vertices;
	std::vector<int> indices;

	int nrPos, nrTex, nrNorm, nrFaces;

	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	
	int offset = 0;
	//position data is always in face info

	memcpy(&nrPos, (char*)data + offset, sizeof(int));
	offset += sizeof(int);
	memcpy(&nrNorm, (char*)data + offset, sizeof(int));
	offset += sizeof(int);
	memcpy(&nrTex, (char*)data + offset, sizeof(int));
	offset += sizeof(int);
	memcpy(&nrFaces, (char*)data + offset, sizeof(int));
	offset += sizeof(int);

	vertices.resize(nrFaces * 3);

	int positionStart = 4 * sizeof(int);
	int normalsStart = positionStart + nrPos * sizeof(float) * 3;
	int textureStart = normalsStart + nrNorm * sizeof(float) * 3;
	int facesStart = textureStart + nrTex * sizeof(float) * 2;
	
	offset = facesStart;

	for (int c = 0; c < nrFaces; c++)
	{
		for (int k = 0; k < 3; k++)
		{
			int index;
			memcpy(&index, (char*)data + offset, sizeof(int));
			offset += sizeof(int);
			memcpy(&vertices[c * 3 + k].Position, (char*)data + positionStart + index * sizeof(float) * 3 , sizeof(float) * 3);
			indices.push_back(c * 3 + k);

			if (nrTex > 0)
			{
				memcpy(&index, (char*)data + offset, sizeof(int));
				offset += sizeof(int);
				memcpy(&vertices[c * 3 + k].TexCoords, (char*)data + textureStart + index * sizeof(float) * 2, sizeof(float) * 2);
			}
			else
				vertices[c * 3 + k].TexCoords = DirectX::XMFLOAT2(0, 1);

			if (nrNorm > 0)
			{
				memcpy(&index, (char*)data + offset, sizeof(int));
				offset += sizeof(int);
				memcpy(&vertices[c * 3 + k].Normal, (char*)data + normalsStart + index * sizeof(float) * 3, sizeof(float) * 3);
			}
			else
				vertices[c * 3 + k].Normal = DirectX::XMFLOAT3(0, 1, 0);
			
		}
		if (nrNorm <= 0)
		{
			DirectX::XMFLOAT3 vertexNormal;
			DirectX::XMFLOAT3 u;
			DirectX::XMFLOAT3 v;
			//if (c % 2 == 0)
			//{
				u = vertices[c * 3 + 1].Position - vertices[c * 3].Position;
				v = vertices[c * 3 + 2].Position - vertices[c * 3].Position;
			//}
			//else
			//{
			//	v = vertices[c * 3 + 1].Position - vertices[c * 3].Position;
			//	u = vertices[c * 3 + 2].Position - vertices[c * 3].Position;
			//}

				vertexNormal.x = u.y * v.z - u.z * v.y;
				vertexNormal.y = u.z * v.x - u.x * v.z;
				vertexNormal.z = u.x * v.y - u.y * v.x;
			
			DirectX::XMStoreFloat3(&vertexNormal, DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&vertexNormal)));

			for (int i = 0; i < 3; i++)
				vertices[c * 3 + i].Normal = vertexNormal;
		}
	}

	m_vertexCount = nrFaces * 3;
	m_indexCount = indices.size();

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(Vertex) * vertices.size());
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = &vertices[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(result))
	{
		return false;
	}
	std::string str = name + "VB";
	SetDebugObjectName(m_vertexBuffer, str);

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(int) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = &indices[0];
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(result))
	{
		return false;
	}
	std::string s = name + "IB";
	SetDebugObjectName(m_indexBuffer, s);

	//// Release the arrays now that the vertex and index buffers have been created and loaded.
	//delete[] indices;
	//indices = 0;

	return true;
}

void Model::ShutdownBuffers()
{
	// Release the index buffer.
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}
