#ifndef GUID_h__
#define GUID_h__

#include <stdint.h>
/*
1 byte package number
1 byte file offset
2 byte file type
4 byte file size
*/
enum GUIdentifier: uint64_t
{
	apa_txt = 0x00000000000000bf,
	apaLjud_txt = 0x0001000000000011,
	apaTextur_txt = 0x0002000000000018,
	kanin_txt = 0x0100000000000000,
	kaninText_txt = 0x0101000000000000,
	strutsText_txt = 0x0200000000000005,
	bunny_qwe = 0x030000000013228c,
	DefaultInstancedPS_cso = 0x0400000000003060,
	DefaultInstancedVS_cso = 0x0401000000004528,
};

static const char* GetGUIDString(GUIdentifier guid)
{
	switch (guid)
	{
	case apa_txt:
		return "apa_txt";
	case apaLjud_txt:
		return "apaLjud_txt";
	case apaTextur_txt:
		return "apaTextur_txt";
	case kanin_txt:
		return "kanin_txt";
	case kaninText_txt:
		return "kaninText_txt";
	case strutsText_txt:
		return "strutsText_txt";
	case bunny_qwe:
		return "bunny_qwe";
	case DefaultInstancedPS_cso:
		return "DefaultInstancedPS_cso";
	case DefaultInstancedVS_cso:
		return "DefaultInstancedVS_cso";
	default:
		break;
	}
}

#endif // GUID_h__