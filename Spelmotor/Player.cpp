#include "Player.h"
#include "Input.h"
#include "DDSTexture.h"

Player::Player(GraphicsManager* gm, AssetManager* assetManager, float radius)
	: PhysicsGameObject(gm, assetManager, SimpleShapes::SPHERE, { radius, 0.f, 0.f})
	, m_accelerationForce(250.f * 0.0125f)
	, m_maxSpeedZ(11.5f)
	, m_maxSpeedX(10.0f)
{
	m_model = assetManager->Load<Model>(0xD73721FC7B29B236);
	m_texture = assetManager->Load<DDSTexture>(0x385835F86DE15759);
	float sphereRadius = 1.0f;
	m_scale = { sphereRadius * radius, sphereRadius * radius , sphereRadius * radius };

	//Default bindings
	m_keyBinds[PLAYER_ACTION::MOVE_LEFT] = static_cast<int>('A');
	m_keyBinds[PLAYER_ACTION::MOVE_RIGHT] = static_cast<int>('D');
	m_keyBinds[PLAYER_ACTION::MOVE_FORWARD] = static_cast<int>('W');
	m_keyBinds[PLAYER_ACTION::MOVE_BACKWARD] = static_cast<int>('S');
	m_keyBinds[PLAYER_ACTION::JUMP] = static_cast<int>(VK_SPACE);
	m_physicsObject->GetRigidDynamic()->setAngularDamping(0.5f);
	m_physicsObject->GetRigidDynamic()->setMass(0.1f);
}

Player::~Player()
{
}

void Player::Update()
{
	
	HandleInput();
	auto currentVelocity = m_physicsObject->GetRigidDynamic()->getLinearVelocity();
	if (currentVelocity.z > m_maxSpeedZ)
		currentVelocity.z = m_maxSpeedZ;
	else if (currentVelocity.z < -m_maxSpeedZ)
		currentVelocity.z = -m_maxSpeedZ;
	if (currentVelocity.x > m_maxSpeedX)
		currentVelocity.x = m_maxSpeedX;
	else if (currentVelocity.x < -m_maxSpeedX)
		currentVelocity.x = -m_maxSpeedX;

	m_physicsObject->GetRigidDynamic()->setLinearVelocity(currentVelocity);
	PhysicsGameObject::Update();
}

void Player::HandleInput()
{
	if (Input::GetAsyncKeyState(m_keyBinds[PLAYER_ACTION::MOVE_LEFT]))
		m_physicsObject->AddForce({ -1.0f, 0.f, 0.f }, m_accelerationForce);
	if (Input::GetAsyncKeyState(m_keyBinds[PLAYER_ACTION::MOVE_RIGHT]))
		m_physicsObject->AddForce({ 1.0f, 0.f, 0.f }, m_accelerationForce);
	if (Input::GetAsyncKeyState(m_keyBinds[PLAYER_ACTION::MOVE_FORWARD]))
		m_physicsObject->AddForce({ 0.0f, 0.f, 1.f }, m_accelerationForce);
	if (Input::GetAsyncKeyState(m_keyBinds[PLAYER_ACTION::MOVE_BACKWARD]))
		m_physicsObject->AddForce({ 0.0f, 0.f, -1.f }, m_accelerationForce);
//	if (Input::GetAsyncKeyState(m_keyBinds[PLAYER_ACTION::JUMP]) && m_physicsObject->GetRigidDynamic()->)
//		m_physicsObject->AddForce({ 0.0f, 1.f, 0.f }, m_accelerationForce);
}

void Player::SetBind(PLAYER_ACTION action, int newKeybinding)
{
	m_keyBinds[action] = newKeybinding;
}

float Player::GetPlayerZVelocity()
{
	return m_physicsObject->GetRigidDynamic()->getLinearVelocity().z;
}

void Player::IncreaseMinSpeeds(float speedIncrease)
{
	m_maxSpeedZ += speedIncrease;
}