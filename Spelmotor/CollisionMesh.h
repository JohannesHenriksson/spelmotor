#pragma once
#include "Asset.h"
#include "PxPhysicsAPI.h"

class CollisionMesh : 
	public Asset
{
public:
	CollisionMesh();
	~CollisionMesh();
	ASSET_ERROR_CODES Load(ID3D11Device* device, void* data, int dataSize, const std::string& name, MemoryTracker* memTracker);
	void Unload(MemoryTracker* memTracker) = 0;
private:
	
};

