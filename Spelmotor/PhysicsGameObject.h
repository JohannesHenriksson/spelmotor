#pragma once
#include "GameObject.h"
#include "PhysicsObject.h"
class PhysicsGameObject :
	public GameObject
{
public:
	//HalfExtents for Box Shape, If sphere shape put radius in halfExtents.x
	PhysicsGameObject(GraphicsManager* gm, AssetManager* assetManager, SimpleShapes shape, DirectX::XMFLOAT3 halfExtents);
	PhysicsGameObject(GraphicsManager* gm, AssetManager* assetManager, SimpleShapes shape, DirectX::XMFLOAT3 halfExtents, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 scale = {1.f, 1.f, 1.f});
	~PhysicsGameObject();

	virtual void Update() override;
	PhysicsObject* GetPhysicsObject();

protected:
	PhysicsObject* m_physicsObject;
};

