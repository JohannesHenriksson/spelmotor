#include "PhysicsGameObject.h"




PhysicsGameObject::PhysicsGameObject(GraphicsManager* gm, AssetManager* assetManager, SimpleShapes shape, DirectX::XMFLOAT3 halfExtents)
	: GameObject(gm, assetManager)
{
	m_physicsObject = new PhysicsObject(shape, &this->m_position, &this->m_rotation, halfExtents);
}

PhysicsGameObject::PhysicsGameObject(GraphicsManager* gm, AssetManager* assetManager, SimpleShapes shape, DirectX::XMFLOAT3 halfExtents, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 scale)
	: GameObject(gm, assetManager)
{
	m_position = position;

	m_physicsObject = new PhysicsObject(shape, &this->m_position, &this->m_rotation, halfExtents);
	m_scale = scale;
}

PhysicsGameObject::~PhysicsGameObject()
{
	delete m_physicsObject;
}

void PhysicsGameObject::Update()
{
	if (m_physicsObject)
		m_physicsObject->Update();
	/*
		if (Input::GetAsyncKeyState('T'))
			m_physicsObject->AddForce(direction, 250.f);
		if (Input::GetAsyncKeyState('G'))
			m_physicsObject->AddForce(direction * (-1.0f), 250.f);*/


	GameObject::Update();
}

PhysicsObject* PhysicsGameObject::GetPhysicsObject()
{
	return m_physicsObject;
}
