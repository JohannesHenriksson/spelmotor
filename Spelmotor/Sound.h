#pragma once

#include "Audio.h"
#include "SoundManager.h"
#include "SoundOptions.h"
#include <vector>
#include "boost/function.hpp"
#include "boost/bind.hpp"

class Sound {
public:
	Sound();
	Sound(Audio* audioAsset);
	void Init(Audio* audioAsset);
	void Play(DWORD playFlags = 0);
	void Pause();
	//positive value to increase, negative to decrease. Default value is 1.0
	void ChangeVolume(float value);
	float GetVolume();

	bool loadCompleted = false;
	float volume;
	std::vector<boost::function<void()>> functionCalls;

	Audio* GetAudioAsset();
private:
	Audio* audioAsset;
	FMOD::Channel* channel;
protected:
	FMOD::Channel* GetChannel();
	FMOD::Sound* GetAudioSource();
	void SetChannel(FMOD::Channel* channel);

	friend SoundManager;
};