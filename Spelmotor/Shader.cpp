#include "Shader.h"

#include <d3dcompiler.h>
#include <comdef.h>
#include <iostream>

Shader::Shader() 
	: m_shaderSource(nullptr)
{ }


Shader::~Shader()
{ }

size_t Shader::GetSize()
{
	if(m_shaderSource)
		return m_shaderSource->GetBufferSize();
	return 0;
}

void Shader::LoadShaderSource(void* data, int dataSize)
{
	ID3DBlob* tempBlob;

	auto hr = D3DCreateBlob(dataSize, &tempBlob);
	if(FAILED(hr))
	{
		_com_error err(hr);
		LPCTSTR errMsg = err.ErrorMessage();
		std::cout << "Failed to create shader source blob: " << errMsg << std::endl;
		return;
	}

	memcpy(tempBlob->GetBufferPointer(), data, dataSize);
	
	m_shaderSource.reset(tempBlob);
}
