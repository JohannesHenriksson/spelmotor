#include "input.h"
#include "Window.h"

HWND Input::listenWindow;

static int keyCallbackID = 0;
static int mouseButtonCallbackID = 0;
static int scrollCallbackID = 0;
static int resizeCallbackID = 0;
static int charCallbackID = 0;

std::vector<std::pair<int, std::function<void(const KeyState&)>>> Input::keyCallbacks;
std::vector<std::pair<int, std::function<void(const KeyState&)>>> Input::mouseButtonCallbacks;
std::vector<std::pair<int, std::function<void(int)>>>			 Input::charCallbacks;
std::vector<std::pair<int, std::function<void(UINT)>>>			 Input::scrollCallbacks;
std::vector<std::pair<int, std::function<void(int, int)>>>		 Input::resizeCallbacks;

DirectX::XMFLOAT2 Input::mouseDownPos[3];
DirectX::XMFLOAT2 Input::mouseUpPos[3];

DirectX::XMFLOAT2 Input::mousePosition;
DirectX::XMFLOAT2 Input::oldMousePosition;

MOUSE_BUTTON Input::lastMouseButton;
Timer Input::lastClickTimer;
std::chrono::milliseconds Input::doubleClickDelay;

int Input::xLock;
int Input::yLock;

Input::Input()
{

}

Input::~Input()
{

}

void Input::Init(HWND hWnd)
{
	xLock = -1;
	yLock = -1;

	listenWindow = hWnd;

	for(int i = 0; i < 3; ++i)
	{
		mouseDownPos[i] = DirectX::XMFLOAT2(0.0f, 0.0f);
		mouseUpPos[i] = DirectX::XMFLOAT2(0.0f, 0.0f);
	}

	mousePosition.x = 0;
	mousePosition.y = 0;
	oldMousePosition.x = 0;
	oldMousePosition.y = 0;

	SetDoubleClickDelay(500);

	lastClickTimer.Start();

	LockCursor(Window::GetWindowWidth() / 2, Window::GetWindowHeight() / 2);
	HideCursor();
}

void Input::Update()
{
	oldMousePosition = mousePosition;

	POINT mousePoint;

	GetCursorPos(&mousePoint);
	ScreenToClient(listenWindow, &mousePoint);

	mousePosition.x = static_cast<float>(mousePoint.x);
	mousePosition.y = static_cast<float>(mousePoint.y);

	if(xLock != -1)
	{
		POINT lockPoint{ xLock, yLock };

		ClientToScreen(listenWindow, &lockPoint);
		SetCursorPos(lockPoint.x, lockPoint.y);
	}
}

//////////////////////////////////////////////////////////////////////////
//CALLBACK REGISTER
//////////////////////////////////////////////////////////////////////////
int Input::RegisterKeyCallback(std::function<void(const KeyState&)> callback)
{
	keyCallbackID++;
	keyCallbacks.push_back(std::pair<int, std::function<void(const KeyState&)>>(keyCallbackID, callback));
	return keyCallbackID;
}

int Input::RegisterCharCallback(std::function<void(int)> callback)
{
	charCallbackID++;
	charCallbacks.push_back(std::pair<int, std::function<void(int)>>(charCallbackID, callback));
	return charCallbackID;
}

int Input::RegisterScrollCallback(std::function<void(UINT)> callback)
{
	scrollCallbackID++;
	scrollCallbacks.push_back(std::pair<int, std::function<void(UINT)>>(scrollCallbackID, callback));
	return scrollCallbackID;
}

int Input::RegisterResizeCallback(std::function<void(int, int)> callback)
{
	resizeCallbackID++;
	resizeCallbacks.push_back(std::pair<int, std::function<void(int, int)>>(resizeCallbackID, callback));
	return resizeCallbackID;
}

int Input::RegisterMouseButtonCallback(std::function<void(const KeyState&)> callback)
{
	mouseButtonCallbackID++;
	mouseButtonCallbacks.push_back(std::pair<int, std::function<void(const KeyState&)>>(mouseButtonCallbackID, callback));
	return mouseButtonCallbackID;
}

//////////////////////////////////////////////////////////////////////////
//CALLBACK UNREGISTER
//////////////////////////////////////////////////////////////////////////

// Template for the various function that unregisters callbacks
template<class T> void UnregisterCallback(std::vector<T>& callbacks, int keyCallbackID)
{
	unsigned int i;
	for(i = 0; i < callbacks.size(); i++)
	{
		if(callbacks[i].first == keyCallbackID)
			break;
	}

	if(i == callbacks.size())
	{
		std::cout << "Tried to unregister a callback that didn't exist. \n";
		return;
	}

	callbacks[i] = callbacks[callbacks.size() - 1];
	callbacks.pop_back();
}

void Input::UnregisterKeyCallback(int keyCallbackID)
{
	UnregisterCallback(keyCallbacks, keyCallbackID);
}

void Input::UnregisterMouseButtonCallback(int mouseButtonCallbackID)
{
	UnregisterCallback(mouseButtonCallbacks, mouseButtonCallbackID);
}

void Input::UnregisterCharCallback(int charCallbackID)
{
	UnregisterCallback(charCallbacks, charCallbackID);
}

void Input::UnregisterScrollCallback(int scrollCallbackID)
{
	UnregisterCallback(scrollCallbacks, scrollCallbackID);
}

void Input::UnregisterResizeCallback(int resizeCallbackID)
{
	UnregisterCallback(resizeCallbacks, resizeCallbackID);
}

void Input::LockCursor(int xPosition, int yPosition)
{
	xLock = xPosition;
	yLock = yPosition;

	if(xLock != -1)
	{
		POINT lockPoint{ xLock, yLock };

		ClientToScreen(listenWindow, &lockPoint);
		SetCursorPos(lockPoint.x, lockPoint.y);
	}
}

void Input::HideCursor()
{
	int i = 0;
	do
	{
		i = ::ShowCursor(false);
	} while(i >= 0);
	//glfwSetInputMode(listenWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Input::ShowCursor()
{
	int i = 0;
	do
	{
		i = ::ShowCursor(true);
	} while(i < 1);
	//::ShowCursor(true);
	//glfwSetInputMode(listenWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

//////////////////////////////////////////////////////////////////////////
//SETTERS
//////////////////////////////////////////////////////////////////////////
void Input::SetDoubleClickDelay(unsigned int ms)
{
	doubleClickDelay = std::chrono::milliseconds(ms);
}

//////////////////////////////////////////////////////////////////////////
//GETTERS
//////////////////////////////////////////////////////////////////////////
DirectX::XMFLOAT2 Input::GetMouseDelta()
{
	DirectX::XMFLOAT2 returnPosition;

	if(xLock == -1)
	{
		returnPosition.x = mousePosition.x - oldMousePosition.x;
		returnPosition.y = mousePosition.y - oldMousePosition.y;
	}
	else
	{
		returnPosition.x = mousePosition.x - xLock;
		returnPosition.y = mousePosition.y - yLock;
	}

	return returnPosition;
}

DirectX::XMFLOAT2 Input::GetMousePosition()
{
	return mousePosition;
}

bool Input::MouseMoved()
{
	return mousePosition.x != oldMousePosition.x || mousePosition.y != oldMousePosition.y;
}

DirectX::XMFLOAT2 Input::GetMouseDownPos(MOUSE_BUTTON button)
{
	return mouseDownPos[static_cast<int>(button) - 1];
}

DirectX::XMFLOAT2 Input::GetMouseUpPos(MOUSE_BUTTON button)
{
	return mouseUpPos[static_cast<int>(button) - 1];
}

HWND Input::GetListenWindow()
{
	return listenWindow;
}

bool Input::GetAsyncKeyState(int key)
{
	return ((::GetAsyncKeyState(key) >> 15) & 1) == 1;
}

bool Input::GetAsyncKeyModifierState(KEY_MODIFIERS modifier)
{
	if(modifier == KEY_MODIFIERS::NONE || modifier == KEY_MODIFIERS::UNKNOWN)
		return false;

	std::vector<KEY_MODIFIERS> modifiers;
	modifiers.reserve(6);

	for(int i = 0; i < 6; ++i)
	{
		if(static_cast<int>(modifier) & (1 << i))
			modifiers.push_back(static_cast<KEY_MODIFIERS>(1 << i));
	}

	for(const auto& currentModifier : modifiers)
	{
		switch(currentModifier)
		{
			case KEY_MODIFIERS::L_SHIFT:
				if(!GetAsyncKeyState(VK_LSHIFT))
					return false;
				break;
			case KEY_MODIFIERS::R_SHIFT:
				if(!GetAsyncKeyState(VK_RSHIFT))
					return false;
				break;
			case KEY_MODIFIERS::L_CONTROL:
				if(!GetAsyncKeyState(VK_LCONTROL))
					return false;
				break;
			case KEY_MODIFIERS::R_CONTROL:
				if(!GetAsyncKeyState(VK_RCONTROL))
					return false;
				break;
			case KEY_MODIFIERS::L_ALT:
				if(!GetAsyncKeyState(VK_LMENU))
					return false;
				break;
			case KEY_MODIFIERS::R_ALT:
				if(!GetAsyncKeyState(VK_RMENU))
					return false;
				break;
			default:
				return false;
		}
	}

	return true;
}

void Input::KeyEvent(UINT msg, WPARAM wParam, LPARAM lParam)
{
	if(keyCallbacks.size() == 0)
		return;

	KeyState keyState;

	if(msg == WM_KEYDOWN)
	{
		if(((lParam >> 30) & 1) == 0)
			keyState.action = KEY_ACTION::DOWN;
		else
			keyState.action = KEY_ACTION::REPEAT;
	}
	else
		keyState.action = KEY_ACTION::UP;

	keyState.mods = GetCurrentModifierKeys();

	keyState.key = static_cast<int>(wParam);

	for(unsigned int i = 0; i < keyCallbacks.size(); i++)
	{
		keyCallbacks[i].second(keyState);
	}
}

void Input::CharEvent(unsigned int key)
{
	if(key < 32 || key > 126)
		return;

	for(unsigned int i = 0; i < charCallbacks.size(); i++)
	{
		charCallbacks[i].second(key);
	}
}

void Input::MouseButtonEvent(UINT msg, WPARAM wParam)
{
	if(mouseButtonCallbacks.size() == 0)
		return;

	KeyState keyState;

	keyState.mods = GetCurrentModifierKeys();

	MOUSE_BUTTON currentMouseButton = MOUSE_BUTTON::UNKNOWN;

	switch(msg)
	{
		case WM_LBUTTONDOWN:
			SetCapture(listenWindow);
			keyState.action = KEY_ACTION::DOWN;
			keyState.key = static_cast<int>(MOUSE_BUTTON::LEFT);
			currentMouseButton = MOUSE_BUTTON::LEFT;
			break;
		case WM_LBUTTONUP:
			SetCapture(0);
			keyState.action = KEY_ACTION::UP;
			keyState.key = static_cast<int>(MOUSE_BUTTON::LEFT);
			currentMouseButton = MOUSE_BUTTON::LEFT;
			break;
		case WM_MBUTTONDOWN:
			keyState.action = KEY_ACTION::DOWN;
			keyState.key = static_cast<int>(MOUSE_BUTTON::MIDDLE);
			currentMouseButton = MOUSE_BUTTON::MIDDLE;
			break;
		case WM_MBUTTONUP:
			keyState.action = KEY_ACTION::UP;
			keyState.key = static_cast<int>(MOUSE_BUTTON::MIDDLE);
			currentMouseButton = MOUSE_BUTTON::MIDDLE;
			break;
		case WM_RBUTTONDOWN:
			keyState.action = KEY_ACTION::DOWN;
			keyState.key = static_cast<int>(MOUSE_BUTTON::RIGHT);
			currentMouseButton = MOUSE_BUTTON::RIGHT;
			break;
		case WM_RBUTTONUP:
			keyState.action = KEY_ACTION::UP;
			keyState.key = static_cast<int>(MOUSE_BUTTON::RIGHT);
			currentMouseButton = MOUSE_BUTTON::RIGHT;
			break;
		default:
			break;
	}

	DirectX::XMFLOAT2 mousePoint = GetMousePosition();

	if(keyState.action == KEY_ACTION::DOWN)
	{
		DirectX::XMFLOAT2 newMousePos(mousePoint.x, mousePoint.y);
		DirectX::XMFLOAT2 lastMouseDownPos(mouseDownPos[static_cast<int>(lastMouseButton) - 1].x, mouseDownPos[static_cast<int>(lastMouseButton) - 1].y);

		DirectX::XMVECTOR xmNewMousePos = DirectX::XMLoadFloat2(&newMousePos);
		DirectX::XMVECTOR xmLastMouseDownPos = DirectX::XMLoadFloat2(&lastMouseDownPos);

		if(currentMouseButton == lastMouseButton
		   && lastClickTimer.GetTimeMilliseconds() <= doubleClickDelay.count()
		   && DirectX::XMVectorGetX(DirectX::XMVector2Length(DirectX::XMVectorSubtract(xmNewMousePos, xmLastMouseDownPos))) <= maxDoubleClickDistance)
		{
			keyState.action = KEY_ACTION::REPEAT;
		}

		lastClickTimer.Reset();

		mouseDownPos[keyState.key - 1] = mousePoint;
	}
	else
	{
		mouseUpPos[keyState.key - 1] = mousePoint;
	}

	lastMouseButton = currentMouseButton;
	for(unsigned int i = 0; i < mouseButtonCallbacks.size(); i++)
	{
		mouseButtonCallbacks[i].second(keyState);
	}
}

void Input::ScrollEvent(UINT distance)
{
	for(unsigned int i = 0; i < scrollCallbacks.size(); i++)
	{
		scrollCallbacks[i].second(distance);
	}
}

void Input::ResizeEvent(int xResolution, int yResolution)
{
	for(unsigned int i = 0; i < resizeCallbacks.size(); i++)
	{
		resizeCallbacks[i].second(xResolution, yResolution);
	}
}

DirectX::XMFLOAT2 Input::GetWindowSize()
{
	RECT rect;

	GetClientRect(listenWindow, &rect);

	return DirectX::XMFLOAT2(static_cast<float>(rect.right), static_cast<float>(rect.bottom));
}

KEY_MODIFIERS Input::GetCurrentModifierKeys()
{
	int modifiers = 0;

	if(GetKeyState(VK_LSHIFT) & 0x8000)
		modifiers |= static_cast<int>(KEY_MODIFIERS::L_SHIFT);
	if(GetKeyState(VK_RSHIFT) & 0x8000)
		modifiers |= static_cast<int>(KEY_MODIFIERS::R_SHIFT);
	if(GetKeyState(VK_LCONTROL) & 0x8000)
		modifiers |= static_cast<int>(KEY_MODIFIERS::L_CONTROL);
	if(GetKeyState(VK_RCONTROL) & 0x8000)
		modifiers |= static_cast<int>(KEY_MODIFIERS::R_CONTROL);
	if(GetKeyState(VK_LMENU) & 0x8000)
		modifiers |= static_cast<int>(KEY_MODIFIERS::L_ALT);
	if(GetKeyState(VK_RMENU) & 0x8000)
		modifiers |= static_cast<int>(KEY_MODIFIERS::R_ALT);

	return static_cast<KEY_MODIFIERS>(modifiers);
}