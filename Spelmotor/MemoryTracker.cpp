#include "MemoryTracker.h"
#include "AssetManager.h"
#include <iostream>


MemoryTracker::MemoryTracker(int max_RAM_Size, int max_VRAM_Size)
	: m_totalRAM(0)
	, m_totalVRAM(0)
	, m_maxRAM(max_RAM_Size)
	, m_maxVram(max_VRAM_Size)
	, m_reservedMemory(0)
{
}


MemoryTracker::~MemoryTracker()
{
}

void MemoryTracker::Submit_RAM(AssetType type, int memoryInBytes, int reservedMemory)
{
	AssetManager::freeMemLock.lock();
	m_totalRAM += memoryInBytes;
	m_reservedMemory -= reservedMemory;
	AssetManager::freeMemLock.unlock();

	auto it = m_assetMemory.find(type);
	if (it != m_assetMemory.end())
	{
		m_assetMemory[type].first += memoryInBytes;
	}
	else
	{
		m_assetMemory[type] = { memoryInBytes, 0 };
	}
}

void MemoryTracker::Remove_RAM(AssetType type, int memoryInBytes)
{
	auto it = m_assetMemory.find(type);
	if (it != m_assetMemory.end())
	{
		m_assetMemory[type].first -= memoryInBytes;
		m_totalRAM -= memoryInBytes;
	}
	else
	{
		std::cout << "No asset with this assettype has been submitted" << std::endl;
	}
}

void MemoryTracker::Submit_VRAM(AssetType type, int memoryInBytes)
{
	auto it = m_assetMemory.find(type);
	if (it != m_assetMemory.end())
	{
		m_assetMemory[type].second += memoryInBytes;
	}
	else
	{
		m_assetMemory[type] = { 0, memoryInBytes };
	}
	m_totalVRAM += memoryInBytes;
}

void MemoryTracker::Remove_VRAM(AssetType type, int memoryInBytes)
{
	auto it = m_assetMemory.find(type);
	if (it != m_assetMemory.end())
	{
		m_assetMemory[type].second -= memoryInBytes;
		m_totalVRAM -= memoryInBytes;
	}
	else
	{
		std::cout << "No asset with this asset type has been submitted" << std::endl;
	}
}

int MemoryTracker::Get_RAM_For(AssetType type)
{
	auto it = m_assetMemory.find(type);
	if (it != m_assetMemory.end())
	{
		return m_assetMemory[type].first;
	}
	else
	{
		std::cout << "No assets with this type was found!";
		return -1;
	}
}

int MemoryTracker::GetTotal_RAM()
{
	return m_totalRAM + m_reservedMemory;
}

int MemoryTracker::Get_VRAM_For(AssetType type)
{
	auto it = m_assetMemory.find(type);
	if (it != m_assetMemory.end())
	{
		return m_assetMemory[type].second;
	}
	else
	{
		std::cout << "No assets with this type was found!";
		return -1;
	}
}

int MemoryTracker::GetTotal_VRAM()
{
	return m_totalVRAM;
}

void MemoryTracker::SetMax_RAM_Usage(int maxRam)
{
	m_maxRAM = maxRam;
}

void MemoryTracker::SetMax_VRAM_Usage(int maxVram)
{
	m_maxVram = maxVram;
}

int MemoryTracker::GetMax_RAM()
{
	return m_maxRAM;
}

int MemoryTracker::GetMax_Vram()
{
	return m_maxVram;
}

int MemoryTracker::Get_RAM_left()
{
	return (m_maxRAM - m_totalRAM - m_reservedMemory);
}

int MemoryTracker::Get_VRAM_left()
{
	return (m_maxVram - m_totalVRAM);
}

void MemoryTracker::PrintMemoryUsePerType()
{
	system("cls");
	for (auto it : m_assetMemory)
	{
		std::cout << GetEnumString(it.first).c_str() << "\t" << it.second.first << "\t\t bytes RAM" << std::endl;
	}
	std::cout << "Total: " << m_totalRAM / 1000.f << "kb RAM" << std::endl;
	std::cout << std::endl;
}

void MemoryTracker::ReserveMemory(int memory)
{
	m_reservedMemory += memory;
}

std::string MemoryTracker::GetEnumString(AssetType type)
{
	switch (type)
	{
	case AssetType::TEXTURE:
		return "Texture";
	case AssetType::SHADER:
		return "Shader";
	case AssetType::PHYSICS:
		return "physics";
	case AssetType::MODEL:
		return "Model";
	case AssetType::AUDIO:
		return "Audio";
	}
}
