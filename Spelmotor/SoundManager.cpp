#include "SoundManager.h"
#include "Sound.h"
#include <memory>

SoundManager* SoundManager::instance;
const int AUDIO_CHANNELS = 256;

SoundManager::SoundManager()
{
	// Create FMOD interface object
	FMOD_RESULT result = FMOD::System_Create(&system);
	FMODErrorCheck(result);
	result = system->init(AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	FMODErrorCheck(result);
}

SoundManager::~SoundManager()
{
	system->release();
}

SoundManager* SoundManager::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new SoundManager();
	}
	return instance;
}

void SoundManager::PlayLooped(Sound* sound)
{
	FMOD::Channel* channel = nullptr;
	channel = Play(sound->GetAudioSource());
	channel->setMode(FMOD_LOOP_NORMAL);
	channel->setPaused(false);
	channel->setVolume(sound->volume);
	sound->SetChannel(channel);
}

void SoundManager::PlayLooped(Sound* sound, DirectX::XMFLOAT3* position, DirectX::XMFLOAT3* velocity)
{
	FMOD::Channel* channel;
	channel = Play(sound->GetAudioSource());
	channel->set3DAttributes(reinterpret_cast<FMOD_VECTOR*>(position), reinterpret_cast<FMOD_VECTOR*>(velocity));
	channel->setMode(FMOD_LOOP_NORMAL);
	channel->setPaused(false);
	channel->setVolume(sound->volume);
	sound->SetChannel(channel);
}

void SoundManager::PlayOnce(Sound* sound, DirectX::XMFLOAT3* position)
{
	FMOD::Channel* channel;
	channel = Play(sound->GetAudioSource());
	channel->set3DAttributes(reinterpret_cast<FMOD_VECTOR*>(position), nullptr);
	channel->setPaused(false);
	channel->setVolume(sound->volume);
	sound->SetChannel(channel);
}

void SoundManager::PlayOnce(Sound* sound)
{
	FMOD::Channel* channel;
	channel = Play(sound->GetAudioSource());
	channel->setPaused(false);
	channel->setVolume(sound->volume);
	sound->SetChannel(channel);
}


ASSET_ERROR_CODES SoundManager::CreateSound(void* data, int size, Audio* audio)
{
	FMOD_CREATESOUNDEXINFO info = { 0 };
	info.cbsize = sizeof(info);
	info.length = size;
	//info.format = FMOD_SOUND_FORMAT_BITSTREAM;
	FMOD::Sound* soundSource;
	result = system->createSound(reinterpret_cast<char*>(data), FMOD_OPENMEMORY, &info, &soundSource);
	audio->SetAudioSource(soundSource);
	return FMODErrorCheck(result);
}


FMOD::Channel* SoundManager::Play(FMOD::Sound* sound)
{
	FMOD::Channel* channel;
	result = system->playSound(sound, 0, true, &channel);
	FMODErrorCheck(result);
	return channel;
}

void SoundManager::Update()
{
	system->update();
}

void SoundManager::ChangeVolume(Sound* sound, float value)
{
	FMOD::Channel* channel = sound->GetChannel();
	float currentVolume = sound->volume;
	channel->setVolume(currentVolume + value);
	sound->volume = currentVolume + value;
}

void SoundManager::Pause(Sound* sound)
{
	sound->GetChannel()->setPaused(true);
}

ASSET_ERROR_CODES SoundManager::FMODErrorCheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		if (result == FMOD_ERR_FILE_NOTFOUND)
		{
			return ASSET_ERROR_CODES::ERROR_COULD_NOT_READ_FILE;
		}
		
		return ASSET_ERROR_CODES::ERROR_UNKOWN;
	}
}

