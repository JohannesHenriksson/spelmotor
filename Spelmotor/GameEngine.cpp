#include <memory>

#include "GameEngine.h"
#include "SoundManager.h"
#include "memoryManager.h"
#include "Window.h"
#include "AssetManager.h"
#include "Camera.h"
#include "Input.h"
#include "Sound.h"
#include "FreeFlightCamera.h"
#include "PhysicsManager.h"
#include "PlayerCamera.h"

GameEngine* GameEngine::m_instance;

GameEngine::GameEngine()
	: m_isRunning(true)
{

}

void GameEngine::Init()
{
	m_window = new Window();
	m_memoryManager = new MemoryManager();
	m_assetManager = new AssetManager(m_memoryManager, m_window->GetDevice());
	m_graphicsManager = new GraphicsManager(m_window, m_assetManager);
	m_physicsManager = PhysicsManager::GetInstance();
	Input::Init(m_window->GetHWND());

	m_defaultCamera = new FreeFlightCamera();
	m_camera = m_defaultCamera;
	//initializes SoundManager if non has been made.
	//SoundManager::GetInstance();
}

GameEngine::~GameEngine()
{
	delete m_defaultCamera;
	delete m_graphicsManager;
	delete m_assetManager;
	delete m_memoryManager;
	delete m_window;
}

GameEngine* GameEngine::GetInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new GameEngine();
		m_instance->Init();
	}
	return m_instance;
}

void GameEngine::Draw()
{	
	m_graphicsManager->DrawAllObjects();
}

void GameEngine::Update(float dt)
{
	//read input
	Input::Update();
	//Handle windows messages
	if (!m_window->PeekMessages() || Input::GetAsyncKeyState(VK_ESCAPE) == true)
	{
		m_isRunning = false;
		return;
	}

	//Needs seconds
	m_physicsManager->Update(dt * 0.001f);

	m_graphicsManager->UpdateConstantBuffer(m_camera->GetViewProj(), m_light);
	//SoundManager::GetInstance()->Update();	
}

void GameEngine::UpdateLightPosition(DirectX::XMFLOAT4 pos1, DirectX::XMFLOAT4 pos2)
{
	m_light.lightPos[0] = pos1;
	m_light.lightPos[1] = pos2;
}

GraphicsManager* GameEngine::GetGraphicsManager() const
{
	return m_graphicsManager;
}

AssetManager* GameEngine::GetAssetManager() const
{
	return m_assetManager;
}

MemoryManager* GameEngine::GetMemoryManager() const
{
	return m_memoryManager;
}

Camera* GameEngine::GetCamera() const
{
	return m_camera;
}

Window* GameEngine::GetWindow() const
{
	return m_window;
}

bool GameEngine::IsRunning() const
{
	return m_isRunning;
}

void GameEngine::SetCamera(Camera* camera)
{
	if (!camera)
		m_camera = m_defaultCamera;
	else
		m_camera = camera;
}
