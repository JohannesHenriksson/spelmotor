#pragma once


#include <windows.h>
#include <d3d11.h>
#include <string>

#ifndef SafeRelease
	#define SafeRelease(x) if(x != nullptr) {x->Release(); x = nullptr;}
#endif
#define SafeDelete(x) if(x!=nullptr) delete x

class Window
{
public:
	Window();
	~Window();

	ID3D11Device* GetDevice() const;
	ID3D11DeviceContext* GetDeviceContext() const;
	ID3D11DepthStencilView* GetDepthStencilView() { return m_depthStencilView; }
	HWND GetHWND() { return m_hWnd; }
	void SetAndClearRenderTargets();
	void Present();
	HINSTANCE getHinstance() { return GetModuleHandle(NULL); }

	bool PeekMessages();

	static bool GetWindowIsFocused();
	static UINT GetWindowWidth();
	static UINT GetWindowHeight();

private:

	HINSTANCE m_hInstance;
	HWND m_hWnd;

	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	IDXGISwapChain* m_swapChain;

	ID3D11RenderTargetView* m_backbufferRTV;
	ID3D11UnorderedAccessView* m_backbufferUAV;

	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilView* m_depthStencilView;


	void InitWindow();
	void InitDevice();


	static UINT m_windowWidth;
	static UINT m_windowHeight;

	static bool windowIsFocused;

	static LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	inline void SetDebugObjectName(ID3D11DeviceChild* resource, const std::string& name)
	{
#if defined(_DEBUG) || defined(PROFILE)
		resource->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.c_str());
#endif
	}
};
