#pragma once

#include "StackAllocator.h"
#include "PoolAllocator.h"

#include <DirectXMath.h>
#include <map>
#include <string>

class MemoryManager
{
private:
	std::map<int, StackAllocator*> m_stack;
	std::map<int, PoolAllocator*> m_pool;

	std::mutex memoryLock;

	//PoolAllocator<int> m_poolAlloc_int;
	

	//int AllocPoolInt(size_t p_size);	//creates a new memory pool for ints and returns ID

public:
	MemoryManager();
	~MemoryManager();

	StackAllocator* CreateStack(size_t p_size, int stackID);	//Creates a new memory stack and returns ID
	PoolAllocator* CreatePool(int numberOfSlots, int slotSize, int poolID);	//creates a new memory pool and returns pointer
	//Returns pointer to memory pool
	StackAllocator* GetStack(int stackID);
	//Return pointer to memory pool, creates new if there is no existing one
	StackAllocator* GetStack_safe(int stacklID, int size = 1024);
	//Returns pointer to memory stack
	PoolAllocator* GetPool(int poolID);
	//Return pointer to memory stack, creates new if there is no existing one
	PoolAllocator* GetPool_safe(int poolID, int numberOfSlots = 1024, int slotSize = 1024);
	void DeletePool(int poolID);
	void DeleteStack(int stackID);

	//template<typename T>
	//inline T* StackAlloc()
	//{
	//	char* test = m_stack[0]->Alloc<char>();	//TODO handle which stack to use
	//	return test;
	//}
};

