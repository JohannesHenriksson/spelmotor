#ifndef CollisionBox_h__
#define CollisionBox_h__

#include "Asset.h"
#include <d3d11.h>
#include <directxmath.h>

class CollisionBox : 
	public Asset
{
public:
	CollisionBox();
	~CollisionBox();

	ASSET_ERROR_CODES Load(ID3D11Device * device, void * data, int dataSize, const std::string& name, MemoryTracker* memTracker);
	void Unload(MemoryTracker* memTracker);

	virtual size_t GetSize();

	DirectX::XMFLOAT3 middlePoint;
	DirectX::XMFLOAT3 halfExtents;
};

#endif // CollisionBox_h__